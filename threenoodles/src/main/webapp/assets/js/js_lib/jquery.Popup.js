﻿(function($){
	$.fn.popup = function(settings){
		var config = {
				popup_title:'提示',
				popup_message:'',
				popup_style:'warning',
				popup_ok:false,
				popup_ok_text:'',
				popup_cancel:false,
				popup_cancel_text:'',
				popup_spare:false,
				popup_spare_text:'',
				//eventType:'click',
				height:0,
				width:0,
				modal:0,
				windowPadding:0,
				overlayOpacity:70,
				functionCallOnOKDown:null,
				functionCallOnCancleDown:null,
				functionCallOnSpareAction:null
			};
		
		if(settings){
			config = $.extend({},config,settings || {});
		}
		$.fn.popup.initPopup(config);
		
	}
	$.fn.popup.init = function(config){
		var popupHtml = '';
		popupHtml += '<div id="popup_alert" style="display:none;overflow: hidden;">';
		popupHtml += '<div id="popup_container">';
		popupHtml += '<h1 id="popup_title">提示</h1>';
		popupHtml += '<div id="popup_content" class="warning">';
		popupHtml += '<div id="popup_message" style="overflow:visible;"></div>';
		popupHtml += '<div id="popup_panel">';
		popupHtml += '<input type="button" value="&nbsp;OK&nbsp;" id="popup_ok" style="display: none;">';
		popupHtml += '<input type="button" value="SPARE" id="popup_spare" style="display: none;">';
		popupHtml += '<input type="button" value="&nbsp;CANCEL&nbsp;" id="popup_cancel" style="display: none;"></div>';
		popupHtml += '</div></div></div>';
		$("body").append(popupHtml);
		
		
		$("#popup_title").html(config.popup_title);
		$("#popup_content").attr("class",config.popup_style);
		$("#popup_message").html(config.popup_message);

		if(config.popup_ok){
			$("#popup_ok").val(config.popup_ok_text);
			$("#popup_ok").show();
		}
		if(config.popup_cancel){
			$("#popup_cancel").val(config.popup_cancel_text);
			$("#popup_cancel").show();
		}
		if(config.popup_spare){
			$("#popup_spare").val(config.popup_spare_text);
			$("#popup_spare").show();
		}
		if(!config.height){
			$.each($(config.popup_message),function(index,element){
				
				config.height += $(element).outerHeight(true);
			});
			config.height+=108;
		}
		$(this).openDOMWindow(config);
	}
})(jQuery);