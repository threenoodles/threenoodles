var loginObj = {
		initListener : function(loginFlag,restAlias,buildingAlias){
			$("#username").focus(function(){
				if("inputTips" == $(this).attr("class")){
					$(this).val("");
					$(this).attr("class","");
				}
			});
			
			$("#password").get(0).onkeyup = function(e){
				var ev=window.event||e; 
				switch(ev.keyCode){
					case 13: 
						loginObj.login(loginFlag,restAlias,buildingAlias);
						return;
						break;
				}
			};
			$("#submit").click(function(){
				loginObj.login(loginFlag,restAlias,buildingAlias);
			});
		},
		
		login : function(loginFlag,restAlias,buildingAlias){
			var username = $.trim($("#username").val());
			var password = $.trim($("#password").val());
			username =encodeURIComponent(username);
			$.ajax({
				url : "http://demo.cookwaimai.com/login/testLogin",
				type : "POST",
				dataType: 'json',
				contentType: "application/x-www-form-urlencoded;charset=utf-8",
				cache: false,
				data : {
					username : username,
					password : password
				},
				success : function(response) {
					
					if(response && "success"== response.success){
						if("to_restaurant" == loginFlag && restAlias){
							window.location.href = "http://demo.cookwaimai.com/restaurant/toRestaurantDetail/"+restAlias;
						}else if("to_restaurants" == loginFlag && buildingAlias){
							window.location.href = "http://demo.cookwaimai.com/restaurant/toRestaurants/"+buildingAlias;
						}else if("toGift" == loginFlag){
							window.location.href = "http://demo.cookwaimai.com/gift/toGifts";
						}else if("toMycenter_comment" == loginFlag){
							window.location.href = "http://demo.cookwaimai.com/mycenter/comment?flag=comment";
						}else if("toMycenter_today" == loginFlag){
							window.location.href = "http://demo.cookwaimai.com/mycenter/order?flag=today";
						}else {
							window.location.href = "http://demo.cookwaimai.com";
						}
					}else {
						loginObj.loginFail("用户名或密码错误!");
					}
				},
				error : function() {
					loginObj.loginFail("系统繁忙，请稍候重试!");
				}
			});
		
		},
		loginFail : function(msg){
			$("table .login_failed td").show();
			$("table .login_failed td[name='login_failed_text']").html(msg);
		}
};