var addressObj = {
	init : function() {
		var regionList = $("div[name='region_building']");
		for ( var i = 0; i < regionList.length; i++) {
			var buildingList = $(regionList[i]).find(".t3").children();
			if (buildingList.length <= 0) {
				$(regionList[i]).find(".t3").html(
						"对不起，此区域暂时无外卖送达，不妨换个区域试试...~o(∩_∩)o~");
			}
		}
	},
	initListener : function() {
		$("#container .t4 a.t4_l.active").bind("click", function(){
			var regionId = $(this).attr("val");
			var regionName = $(this).text();
			if (undefined == regionId || "" == regionId || null == regionId) {
				return;
			}
			var regionList = $("div[name='region_building']");
			for ( var i = 0; i < regionList.length; i++) {
				var region_building = $(regionList[i]);
				if (regionId == region_building.attr("val")) {
					addressObj.hideAllAreas();
					region_building.find("a[name='region_name']").text(regionName);
					region_building.show();
				}
			}
		});
		$("#allAreasTitle").bind("click", function(e) {
			addressObj.hideAllAreas();
			$("#hotAreas").show();
		});
	},
	hideAllAreas : function() {
		var regionList = $("div[name='region_building']");
		for ( var i = 0; i < regionList.length; i++) {
			$(regionList[i]).hide();
		}
		$("#hotAreas").hide();
	}
};

var searchObj = {
		init : function(){
			//初始化地址信息:[{href='',name=''},{}...]
			searchObj.addressArr = [];
			var regionsArr = $($("#container .hotArea").find("div[name='region_building']"));
			for(var i = 0;i<regionsArr.length;i++){
				var addresses = $(regionsArr[i]).find("div[class='t3']").find("a");
				for(var j = 0;j<addresses.length;j++){
					var address = {};
					address.href = $.trim(addresses[j].href);
					address.name = $.trim(addresses[j].innerHTML);
					searchObj.addressArr.push(address);
				}
			}
			//初始化输入框的提示信息
			$("#container .searchArea .input").val($("#container .searchArea .input").attr("tips"));
			
			$("#container .searchArea .input").click(function(){
				//用户第一次点击
				if("input inputTips" == $(this).attr("class")){
					$(this).val("");
					$(this).attr("class","input");
				}
			});
		},
		initListener : function(){
			//发生按键事件以后
			var addressInput = $("#container .searchArea .input").get(0);
			addressInput.onkeyup = function(e){
				var e=arguments[arguments.length-1]; 
				var ev=window.event||e; 
				switch(ev.keyCode){ 
					case 13:
						var addressName = searchObj.complete_enter_getAddressName();
						searchObj.complete_connectTo(addressName);
						return;
						break;
						case 40://DOWN 
						var index_active = searchObj.getActiveLiIndex_next();
						searchObj.complete_activeLi(index_active);
						return;
						break; 
						case 38://UP 
							var index_active = searchObj.getActiveLiIndex_prev();
							searchObj.complete_activeLi(index_active);
							return;
							break;
					default: 
					  var addressName = $("#container .searchArea .input").val();
					  if(undefined != addressName && "" != $.trim(addressName)){
							
							searchObj.completeAddress(addressName);
						}else {
							
							$("#autoComplete").hide();
						}
				} 
			};
			
			$("#container .searchArea .input").focus(function(){
				var addressName = $(this).val();
				if(undefined !=addressName && "" != $.trim(addressName)){
					searchObj.completeAddress(addressName);
				}
			});
			$("#autoComplete li").live("mouseover",function(){
				searchObj.complete_active_clear();
				$(this).attr("class","active");
			});
			$("#autoComplete li").live("click",function(){
				var addressName = $(this).html();
				searchObj.complete_connectTo(addressName);
			});
		},
		completeAddress : function(addressName){
			searchObj.addressArr_comp = [];
			for(var i = 0;i<searchObj.addressArr.length;i++){
				var pattern = new RegExp(addressName); 
				var flag = pattern.test(searchObj.addressArr[i].name);
				if(true == flag){
					
					searchObj.addressArr_comp.push(searchObj.addressArr[i]);
				}
			}
			
			//先初始化一下搜索框
			$("#autoComplete li").remove("li");
			if(searchObj.addressArr_comp.length>0){
				for(var i = 0 ;i<searchObj.addressArr_comp.length;i++){
					
					$("#autoComplete").append("<li class=''>"+searchObj.addressArr_comp[i].name+"</li>");
				}
				$("#autoComplete").show();
			}
		},
		//清除激活的li 样式
		complete_active_clear : function(){
			
			var address_comp_arr = $("#autoComplete li");
			for(var i = 0;i<address_comp_arr.length;i++){
				$(address_comp_arr[i]).attr("class","");
			}
		},
		
		//获取下一个需要激活的li 下标
		getActiveLiIndex_next : function(){
			var address_comp_arr = $("#autoComplete li");
			for(var i = 0;i<address_comp_arr.length;i++){
				if("active" == $(address_comp_arr[i]).attr("class")){
					//如果游标到了最后一个元素，就返回第一个游标的index
					if(i == (address_comp_arr.length -1)){
						
						return 0;
					}else {
						
						return i + 1;
					}
				//如果最后一个元素还是没有被激活，那么返回0
				}else if(i == (address_comp_arr.length -1)){
					return 0;
				}
			}
		},
		//获取上一个需要激活的li 下标
		getActiveLiIndex_prev : function(){
			var address_comp_arr = $("#autoComplete li");
			for(var i = 0;i<address_comp_arr.length;i++){
				if("active" == $(address_comp_arr[i]).attr("class")){
					//如果游标到了第一个元素，就返回最后一个游标的index
					if(i == 0){
						return address_comp_arr.length-1;
					}else {
						return i-1;
					}
				//如果最后一个元素还是没有被激活，那么返回0
				}else if(i == (address_comp_arr.length -1)){
					return 0;
				}
			}
		},
		//根据 index 激活一个元素
		complete_activeLi : function(index_active){
			searchObj.complete_active_clear();
			$($("#autoComplete li")[index_active]).attr("class","active");
		},
		//用户敲击了回车，返回地址名
		complete_enter_getAddressName : function(){
			var address_comp_arr = $("#autoComplete li");
			for(var i = 0;i<address_comp_arr.length;i++){
				if("active" == $(address_comp_arr[i]).attr("class")){
					return $(address_comp_arr[i]).html();
				}
			}
		},
		//根据地址名，连接到指定目标
		complete_connectTo : function(addressName){
			if(undefined == addressName || "" == addressName){
				return;
			}
			for(var i = 0;i<searchObj.addressArr.length;i++){
				if(addressName == searchObj.addressArr[i].name){
					
					$("#container .searchArea .input").val(addressName);
					var href = searchObj.addressArr[i].href;
					window.location.href = href;
				}
			}
		}
};