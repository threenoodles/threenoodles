var restaurant = {
	init : function(buildingId) {
		restaurant.restList=[];
		restaurant.restList_html="";
		restaurant.restoreRestList();
		restaurant.getRestCateList();
		//restaurant.updateRestList(buildingId);
	},
	initListener : function() {

		$(".resList .res .body").live("click",
			function() {
				var restAlias = $(this).attr("alias");
				window.location.href = "http://demo.cookwaimai.com/restaurant/toRestaurantDetail/"+restAlias;
		});

		$(".filterBox .body .cb").live("click",function() {
			if ("checked cb" == $(this).attr("class") || !restaurant.restList) {
				return;
			}
			$(".filterBox .checked.cb").each(function(i,element){
				$(element).attr("class","cb");
			});
			$(this).attr("class","checked cb");
			restaurant.setRestVisible($(this).attr("val"));
		});

		$(".filterBox .checked.cb").live("click", function() {

			$(this).attr("class", "cb");
			$("#restList").html(restaurant.restList_html);
		});
	},
	restoreRestList : function() {
		$("#restList").find(".section2").each(function(i){
			restaurant.restList.push(this);
		});
		restaurant.restList_html = $("#restList").html();
	},
	getRestCateList : function() {
		$.ajax({
			type : "POST",
			url : "http://demo.cookwaimai.com/restaurant/listRestCategory",
			success : function(restCateList) {
				restaurant.restCateList = restCateList;
				restaurant.setRestCateList(restCateList);
			}
		});
	},
	setRestCateList : function(restCateList) {
		var restList = $("#restList").children();
		for ( var i = 0; i < restList.length; i++) {
			var rest = $(restList[i]).find(".body");
			var restId = rest.attr("id");
			for ( var j = 0; j < restCateList.length; j++) {
				if (restId == restCateList[j].id) {
					var cateList = restCateList[j].cateList;
					$(restList[i]).attr("val", cateList.toString());
					break;
				}
			}
		}
	},
	// 设置哪些店铺显示
	setRestVisible : function(cateId) {
		if(parseInt($.trim(cateId+""))>0){
			$("#restList").find(".section2").each(function(){
				$(this).remove();
			});
			var i = 1;
			$.each(restaurant.restList,function(){
				var cates = $(this).attr("val");
				if(cates && (cates.indexOf(cateId)>=0)){
					var html = restaurant.createHtml(i,$(this));
					$("#restList").append(html);
					i++;
				}
			});
		}else {
			$("#restList").find(".section2").each(function(){
				$(this).remove();
			});
			var i = 1;
			$.each(restaurant.restList,function(){
				if(1 == $(this).find(".notify").attr("val")){
					var html = restaurant.createHtml(i,$(this));
					$("#restList").append(html);
					i++;
				}
			});
		}
	},
	//创建html
	createHtml : function(index,element){
		var html="";
		if(index%3){
			html+="<div class='section2 res' id="+element.find('.body').attr('id')+">";
			html+=element.html();
			html+="</div>";
		}else {
			html+="<div class='section2 res rlast' id="+element.find('.body').attr('id')+">";
			html+=element.html();
			html+="</div>";
		}
		return html;
	}
};

var notice = {
	orderHtml : "",
	commentHtml : "",
	noticeList : "",
	init : function() {
		$.ajax({
			type : "post",
			url : "http://demo.cookwaimai.com/notice/listNotice",
			success : function(result) {
				orderObjList = result.orderList;
				var orderHtml = '';
				for ( var i = 0; i < orderObjList.length; i++) {
					var time = notice.formatTime(orderObjList[i].ordered_time);
					orderHtml +='<li>';
					orderHtml +='<span class="g">[订单]'+orderObjList[i].rest_name+'</span>';
					orderHtml +=' 来自 '+orderObjList[i].address+' 的用户  '+time;
					orderHtml +='</li>';
				}
				notice.orderHtml = orderHtml;
				$("#orderList").html(orderHtml);
				notice.setTimer();
			}
		});
	},

	initListener : function() {
		$("#ord_rec_Title").bind("mousedown", function(e) {
			var pageX = document.body.clientWidth;
			var posX = e.clientX;
			if (posX / pageX >= 0.7709) {
				window.location.href="http://demo.cookwaimai.com/comment/toBuildingComments";
			}
		});

	},
	
	setTimer : function(){
		var handler = function(){
			$("#orderList").animate({"top":"10px"},1000);
			var order = $("#orderList li:last").remove();
			order.css({"opacity":0});
			$("#orderList").animate({"top":"0px"},500);
			$("#orderList").prepend(order);
			$("#orderList li:first").delay(800).animate({"opacity":1});
		};
		setInterval(handler , 5000);
	},
	formatTime : function(time) {
		return new Date(time).toLocaleTimeString();
	}
};


////紧急通知
//var urgentNotice = {
//		initListener : function(buildingId){
//			$.ajax({
//				url:"/restaurant/getUrgentNotice.action",
//				data: {
//					buildingId : buildingId
//				},
//				cache: !1,
//				async: !0,
//				type: "POST",
//				dataType: "json",
//				success: function(msg) {
//					
//					if(undefined == msg || "none" == msg){
//						return;
//					}else if(undefined != msg.title && "" != msg.title){
//						$("#urgentNotice").show();
//						$("#urgentNotice_title").html(msg.title);							
//						$("#urgentNotice_content").html(msg.content);
//						$("#urgentNotice_content").click(function(){
//						window.open(msg.target);
//						});
//					}
//				},
//				error: function() {
//					
//				}
//			});
//			//点击关闭按钮，关闭通知
//			$("#urgentNotice_close").click(function(){
//				
//				$("#urgentNotice").hide();
//			});
//		}
//};
