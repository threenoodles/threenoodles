var rest = {
		init : function(params_){
			rest.params = {};
			rest.params = $.extend({},rest.params,params_);
		},
		initListener : function(){
			$("#addToFav").click(function(){
				topNavi.testLogin(function(){
					
					rest.addFavourite();
				},function(){
					var popup = Boxy.ask("对不起，登录后才能收藏餐厅哦，是否现在登录？",["登录","取消"],function(val){
	            		 if("登录" == val){
	            			 window.location.href="http://demo.cookwaimai.com/login/toLogin?login_flag=to_restaurant&rest_alias="+rest.params.restAlias;
	            		 }else {
	            			 popup.hide();
	            		 }
	            	 },{title:"温馨提示"});
				},true);
			});
		},
		addFavourite : function(){
			$.ajax({
				 type : "post",
				 dataType: 'json',
				 url : "/restaurant/addFavourite",
				 contentType: "application/x-www-form-urlencoded;charset=utf-8",
				 cache: false, 
				data : {
					restId : rest.params.restId
				},
				success : function(response){
					if(response && response.success && "success" == response.success){
						
						Boxy.alert("<span style='color:#00CC00;'>恭喜您，添加收藏成功！</span>",function(){
							
							$("#favorite")[0].click();
						});
					}else {
						
						Boxy.alert("<span style='color:#FF0000'>添加收藏失败，系统正忙，请稍后重试！");
					}
				},
				error : function(){
					Boxy.alert("<span style='color:#FF0000'>系统正忙，请稍候重试！");
				}
			});
	}
};

var cart = {
		init : function(params_){
			cart.params = {
					totalCount:0,
					totalPrice:0,
					carriage:0,
					restName:"",
					menuList : [],
					menuList_order:[]
			};
			cart.params = $.extend({},cart.params,params_);
			cart.params.carriage = parseFloat(cart.params.carriage);
			
			cart.initCartData(cart.params.menuId);
			cart.initListener();
		},
		// 初始化购物车显示的数据
		initCartData : function(menuId){
			$("#chart .delivery_cost td[class='count']").html(cart.params.carriage+"元");
			$(".chart .total .line div[val='totalPrice']").html(cart.params.carriage+"元");
			$("#cartName").html(rest.params.restName);
			cart.initMenuList(cart.params.menuId);
		},
		initMenuList : function(menuId){
			var menuObjList = $(".menuArea .item");
			for(var i = 0;i<menuObjList.length;i++){
				var menuObj = $(menuObjList[i]);
				var id = menuObj.find("input[name='menu_id']").val();
				var name = menuObj.find("input[name='menu_name']").val();
				var price = menuObj.find("input[name='menu_price']").val();
				var menu = {
						id : id,
						name : name,
						price : price,
						num : 0
				};
				cart.params.menuList.push(menu);
			}
			
			if(menuId){
				var menu = cart.getMenu(menuId);
				if(menu){
					cart.addMenu(menu);
				}
			}
		},
		initListener : function(){
			$(".menuArea .item,.menuArea .rec_t").live("click",function(){
				var menuId = $(this).find("input[name='menu_id']").val();
				var menu = cart.getMenu(menuId);
				if(!menu){
					return;
				}
				var a = menu.name;
				b = $(this).offset(),
				f = $("#chart").offset();
				f.top += $("#chart").height() / 2;
				a = $('<div class="aniObj">' + a + "</div>");
				a.offset(b);
				a.appendTo("body").animate({
					left: f.left + "px",
					top: f.top + "px",
					opacity: 1
				},800,function() {
					$(this).animate({
						opacity: 0
					},
					500,
					function() {
						$(this).remove();
					});
				});
				cart.addMenu(menu);
			});
			
			$("#chart .del").live("click",function(){
				var menuId = $(this).attr("val");
				cart.deleteMenu(menuId);
			});
			
			$("#chart .amount .more").live("click",function(){
				var menuId = $(this).attr("val");
				var menu = cart.getMenu(menuId);
				cart.addMenu(menu);
			});
			$("#chart .amount .less").live("click",function(){
				var menuId = $(this).attr("val");
				cart.cutMenu(menuId);
			});
			
			$(".chart .action a").click(function(){
				cart.params.menuList_order = [];
				var menuHtml = cart.getCartMenuHtml();
				$("#chart").find("tbody").html(menuHtml);
				cart.calculateTotalCount();
			});
			
			$("#pay").bind("click",function(){
				var menuList = cart.initMenuListForSubmit();
				if(!$.trim(menuList)){
					return;
				}
				var carriage = cart.params.carriage;
				//查看店铺是否打样
				var mask = null;
				 $.ajax({
					 url: "http://demo.cookwaimai.com/restaurant/checkStatus", 
					 type: 'GET', 
					 dataType: 'json',
					 contentType: "application/x-www-form-urlencoded;charset=utf-8",
					 cache: false, 
					 data : {
						 restId : rest.params.restId,
						 menuList : menuList,
						 totalPrice : cart.params.totalPrice,
						 carriage : carriage
					 },
					 beforeSend:function(){
						 mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
					 },
					 success: function(response) {
						 if(mask){
							 mask.hide();
						 }
						 var result = (response && response.data) ? response.data : "failed";
						 //可以马上订餐
			             if("success" == result){
			            	 
			            	 $.cookie("orderRestore",null,{path:'/',expires:-1});
			            	 window.location.href="http://demo.cookwaimai.com/order/toConfirmOrder";
			             //可以预定
			             }else if("success_reservation" == result){
			            	 var popup = Boxy.ask("店铺打烊，但是您可以 <span style='color:red'>预定订单</span>，是否预定？",["预定","取消"],function(val){
			            		 if("预定" == val){
			            			 $.cookie("orderRestore",null,{path:'/',expires:-1});
			            			 window.location.href="http://demo.cookwaimai.com/order/toConfirmOrder?flag=true";
			            		 }else {
			            			 popup.hide();
			            		 }
			            	},{title:"温馨提示"});
			            	 
			             }else if("rest_closed" == result){
			            	 
			            	 Boxy.alert("对不起，店铺已打样，您不妨去别家看看?");
			            	 
			             }else if("no_login" == result){
			            	 //保存cookie
			            	 var cookieValue = "{restId:"+rest.params.restId+",menuList:'"+cart.initMenuListForSubmit()+"',carriage:"+cart.params.carriage+",buildingId:"+rest.params.buildingId+"}";
			 				 $.cookie("orderRestore",cookieValue,{path:'/',expires:1});
			 				 
			            	 var popup = Boxy.ask("对不起，您还没有登录，登录后才能订餐哦",["登录","注册","取消"],function(val){
			            		 if("登录" == val){
			            			 window.location.href="http://demo.cookwaimai.com/login/toLogin?login_flag=to_restaurant&rest_alias="+rest.params.restAlias;
			            		 }else if("注册" == val){
			            			 window.location.href="http://demo.cookwaimai.com/regist/toRegist";
			            		 }else {
			            			 popup.hide();
			            		 }
			            	 },{title:"温馨提示"});
			             }else if("less_delivery" == result){
			            	 Boxy.alert("实在不好意思，您的订单没有达到店铺的启送金额！");
			             }
			         },
			         error:function(){
			        	 if(mask){
							 mask.hide();
						 }
			        	 //TODO
			         }
				    });
			});
			
			$("#viewPhone").live("click",function(){
				
				window.scroll(0, 0);
			});
		},
		addMenu : function(menu){
			if(!menu){
				return;
			}
			// menu添加到已购列表中
			if(0 == cart.params.menuList_order.length){
				cart.params.menuList_order.push(menu);
			}
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				if(menu.id == cart.params.menuList_order[i].id){
					cart.params.menuList_order[i].num += 1;
					break;
				}
				if(i == (cart.params.menuList_order.length -1)){
					cart.params.menuList_order.push(menu);
				}
			}
			
			cart.calculateTotalCount();
		},
		cutMenu : function(menuId){
			var menu = cart.getMenu(menuId);
			if(!menu){
				return;
			}
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				var menu = cart.params.menuList_order[i];
				if(menuId == menu.id){
					if(menu.num <=1){
						cart.deleteMenu(menuId);
						break;
					}else {
						menu.num -= 1;
					}
				}
			}
			
			cart.calculateTotalCount();
		},
		deleteMenu : function(menuId){
			if(!menuId){
				return;
			}
			var menuList = [];
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				if(menuId != cart.params.menuList_order[i].id){
					menuList.push(cart.params.menuList_order[i]);
				}
			}
			cart.params.menuList_order = menuList;
			cart.calculateTotalCount();
			
		},
		calculateTotalCount : function(){
			cart.params.totalPrice = 0;
			cart.params.totalCount = 0;
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				var menu = cart.params.menuList_order[i];
				cart.params.totalCount += menu.num;
				cart.params.totalPrice += (menu.price*menu.num);
			}
			$(".chart .total .line div[val='totalPrice']").html((cart.params.totalPrice+cart.params.carriage)+"元");
			$(".chart .total .line div[val='totalCount']").html(cart.params.totalCount+"份");
			$(".chart .total .line div[val='totalPoint']").html((cart.params.totalPrice*5)+"分");

			var menuHtml = cart.getCartMenuHtml();
			$("#chart").find("tbody").html(menuHtml);
		},
		getCartMenuHtml :function(){
			var cartMenuHtml = '';
			cartMenuHtml +='<tr>';
			cartMenuHtml +='<td width="113">菜名</td>';
			cartMenuHtml +='<td width="40" align="center">数量</td>';
			cartMenuHtml +='<td class="count">小计</td>';
			cartMenuHtml +='</tr>';
			cartMenuHtml +='<tr>';
			cartMenuHtml +='<td colspan="3"><div class="divide"></div></td>';
			cartMenuHtml +='</tr>';
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				var menu = cart.params.menuList_order[i];
				cartMenuHtml +='<tr>';
				cartMenuHtml +='<td>'+menu.name+'</td>';
				cartMenuHtml +='<td class="amount"><em><div class="more" val='+menu.id+'></div>';
				cartMenuHtml +='<div class="less" val='+menu.id+'></div></em><input type="text" class="orderNumInput" name="f_1" maxlength="2" readonly="true" value="'+menu.num+'"></td>';
				cartMenuHtml +='<td class="count"><em><div class="del" val='+menu.id+'></div></em>'+(menu.price*menu.num)+'</td>';
				cartMenuHtml +='</tr>';
				cartMenuHtml +='<tr>';
				cartMenuHtml +='<td colspan="3"><div class="divide"></div></td>';
				cartMenuHtml +='</tr>';
			}
			
			cartMenuHtml +='<tr class="delivery_cost">';
			cartMenuHtml +='<td colspan="2">送餐费</td>';
			cartMenuHtml +='<td class="count">'+cart.params.carriage+'元</td>';
			cartMenuHtml +='</tr>';
			
			return cartMenuHtml;
		},
		getMenu : function(menuId){
			var menu = null;
			if(cart.params.menuList){
				for(var i = 0;i<cart.params.menuList.length;i++){
					if(menuId && menuId == cart.params.menuList[i].id){
						menu = cart.params.menuList[i];
					}
				}
			}
			return menu;
		},
		initMenuListForSubmit : function(){
			var menuList_submit = [];
			for(var i = 0;i<cart.params.menuList_order.length;i++){
				var menu = cart.params.menuList_order[i];
				menuList_submit.push("id:"+menu.id+";name:"+menu.name+";num:"+menu.num+";price:"+menu.price);
			}
			return menuList_submit.join(",");
		}
};


var chart = {
    p1: null,
    h: null,
    init: function() {
        this.p1 = $(".partr").offset().top + 35;
        this.h = $(".chart").height();
        $(window).scroll(chart.scrolling);
        $(".chart .submit_tel").click(function() {
            $(".chart .tel").show();
            $(".tips [name=step_1]").hide();
            $(".tips [name=step_2]").show();
            pointsPop.gen(20, ".chart .submit_tel");
        });
    },
    scrolling: function() {
        var b = $(window).scrollTop();
        chart.h = $(".chart").height();
        var a = Math.max($(".partl").height(), $(".partr").height()),
        c = chart.p1 + a - chart.h - 40;
        b <= chart.p1 ? $(".chart").removeClass("chart_sticky chart_pinned_b").addClass("chart_pinned_t") : b < c ? $(".chart").removeClass("chart_pinned_t chart_pinned_b").addClass("chart_sticky") : ($(".partr").css("height", a + "px"), $(".chart").removeClass("chart_pinned_t chart_sticky").addClass("chart_pinned_b"));
    },
    invalidDishes: function() {
        var b = $(document).scrollTop() + $(window).height() / 2 - 85;
        $("#invalidDishes_pop .c").css("top", b + "px");
        $("#invalidDishes_pop").show();
    }
};

var hotComments = {
		restId : null,
		reply_comment_id : null,
		replyingObj : {},
		init : function(restId){
			hotComments.restId = restId;
			hotComments.getComments(restId,0);
		},
		initListener : function(){
			$("#moreComments").click(function(){
				 var commentCount = $(this).attr("val");
				 if(commentCount){
					 commentCount = parseInt(commentCount) + 5;
					 hotComments.getComments(hotComments.restId,commentCount);
				 }
			});
			
			$(".comments .actions .reply").live("click",function(){
				// 要回复的评论的id
				hotComments.reply_comment_id = $(this).parent().attr("id");
				$(".comments .title").find("span").html("&nbsp&nbsp对（"+$(this).parent().attr("val")+"的回复）");
				hotComments.replyingObj = $(this).parent();
			});
			
			$("#change_captha_img").click(function(){
				$(".comments .post .captcha").find("img").attr("src","http://demo.cookwaimai.com/admin/imagecode/getImageCode?r="+Math.random());
			});
			
			$(".comments .post .submit").click(function(){
				hotComments.replyingContent = $(".comments .post textarea").val();
				//var imageCode = $(".comments .post .e input").val();
				if(!hotComments.reply_comment_id){
					Boxy.alert("<span style='color:red'>请您选择要回复的评论！</span>");
					return;
				}else if(!hotComments.replyingContent || hotComments.replyingContent.length<1 || hotComments.replyingContent.length>200){
					Boxy.alert("<span style='color:red'>评论内容为 1-200 字符之间！</span>");
					return;
				}
				$.ajax({
					type : "POST",
					url : "/comment/submitReply",
					data : {
						commentId : hotComments.reply_comment_id,
						content : hotComments.replyingContent
					},
					success : function(response){
						if(response && response.data && "success" == response.data){
							alert("评论成功~");
							var replyHtml = "<div class='quote'><div class='nt'><b>我 在 </b>"+hotComments.formatTime() +"回复</div><div>&nbsp&nbsp&nbsp&nbsp"+hotComments.replyingContent+"</div></div>";
							hotComments.replyingObj.before(replyHtml);
							$(".comments .title").find("span").html("");
							$(".comments .post textarea").val("");
							$("#change_captha_img").get(0).click();
							$(".comments .post .e input").val("");
							return;
						}else {
							var popup = Boxy.ask("登录以后您才可以评论，现在去登录吗？",["登录","取消"],function(val){
			            		 if("登录" == val){
			            			 window.location.href="http://demo.cookwaimai.com/login/toLogin?login_flag=to_restaurant&rest_alias="+rest.params.restAlias;
			            		 }else {
			            			 popup.hide();
			            		 }
			            	},{title:"温馨提示"});
						}
					}
					
				});
			});
		},
		//获取评论和回复信息
		getComments : function(restId,start){
			$.ajax({
				type : "POST",
				url : "/comment/listRestComments",
				data : {
					restId : restId,
					start : start
				},
				success : function(response){
					if(response && response.dataList.length){
						$("#moreComments").attr("val",start);
						
						var commentList = response.dataList;
						var commentsHtml = hotComments.getAllCommentAndReplyHtml(commentList);
						$("#comments").append(commentsHtml);
					}else {
						$("#moreComments").html("没有更多评论了~");
						 return;
					}
				}
			});
		},
		// 根据后台的评论的json 信息获取评论和回复的html
		getAllCommentAndReplyHtml : function(commentList){
			var commentHtml = "";
			for(var i = 0;i<commentList.length;i++){
				commentHtml += hotComments.getCommentAndReplyHtml(commentList[i]);
			}
			return commentHtml;
		},
		getCommentAndReplyHtml : function(comment){
			var comment_id = comment.id;
			var user_name = comment.user_name;
			var comment_time = hotComments.formatTime(comment.comment_time);
			var score = comment.score_avg;
			var content = comment.content;
			var replyList = comment.replyList;
			
			var commentHtml = "";
			commentHtml += "<div class='t'></div>";
			commentHtml += "<div class='body'>";
			commentHtml += "<div class='name_n_time'>"+user_name+"在 "+comment_time+" 说</div>";
			commentHtml += "<div class='star star_"+score+"'></div>";
			commentHtml += "<div class='c2'>";
			commentHtml += "<b>点评：</b>"+content;
			commentHtml += "</div>";
			commentHtml += "<div class='actions' val='"+user_name+"' id='"+comment_id+"'>";
			if(replyList){
				for(var i = 0 ;i<replyList.length;i++){
					commentHtml += hotComments.getReplyHtml(replyList[i]);
				}
			}
			commentHtml += "<a href='#reply' class='reply'>回复</a>";
			commentHtml += "</div>";
			commentHtml += "</div>";
			commentHtml += "<div class='b'></div>";
			
			return commentHtml;
		},
		getReplyHtml : function(reply){
			var replyHtml = "";
			if(reply){
				replyHtml += "<div class='quote'><div class='nt'><b>"+reply.replier_name+" </b>"+hotComments.formatTime(reply.reply_time)+"回复</div><div>&nbsp;&nbsp;&nbsp;&nbsp;"+reply.content+"</div></div>";
			}
			return replyHtml;
		},
		formatTime : function(ms){
			var date = new Date();
			if(ms){
				date = new Date(ms);
			}
			return date.toLocaleString();
		}
};


// 点击下面三个导航条以后的操作
var subNav = {
		initListener : function(){
			$(".subnav a").click(function(){
					$("#menuArea").hide();
					$("#hotComments").hide();
					$("#favorable").hide();
					$(".subnav a").attr("class","");
					var target = "#"+$(this).attr("name");
					$(this).attr("class","active");
					$(target).show();
			});
		}
};

var foodListNav = {
	    p1: null,
	    h: null,
	    init: function() {
	        0 != $(".floatnav").length && (this.p1 = $(".menuArea").offset().top - 42, this.h = $(".floatnav").height(), $(window).scroll(foodListNav.scrolling), $(".floatnav a").click(function() {
	            $(".floatnav li").removeClass("active");
	            $(this).parent().addClass("active");
	            var a = $('a[name="' + $(this).attr("to") + '"]').offset().top,
	            a = a - ($(this).offset().top - $(".floatnav").offset().top + 43);
	            $("html, body").animate({
	                scrollTop: a
	            },
	            500);
	        }), $(".floatnav li:first").addClass("active"))
	    },
	    scrolling: function() {
	        var a = $(window).scrollTop(),
	        b = foodListNav.p1 + $(".menuArea").height() - foodListNav.h - 8;
	        a <= foodListNav.p1 ? $(".floatnav").removeClass("floatnav_sticky floatnav_pinned_b").addClass("floatnav_pinned_t") : a < b ? $(".floatnav").removeClass("floatnav_pinned_t floatnav_pinned_b").addClass("floatnav_sticky") : $(".floatnav").removeClass("floatnav_pinned_t floatnav_sticky").addClass("floatnav_pinned_b")
	    }
	};
	

var menuSearchObj = {
		initListener : function(){
			$(".subnav .qsearch input").val($(".subnav .qsearch input").attr("tips"));
			$(".subnav .qsearch input").focus(function(){
				if("inputTips" == $(this).attr("class")){
					$(this).attr("class","");
					$(this).val("");
				}
			});
			$(".subnav .qsearch input").keyup(function(){
				menuSearchObj.showItems();
				var menuName = $(this).val();
				if(menuName){
					menuSearchObj.selectItem(menuName); 
				}else {
					menuSearchObj.showItems();
				}
			});
		},
		selectItem : function(menuName){
			var g_menuList = $(".menuArea .item");
			var pattern = new RegExp(menuName); 
			for(var i = 0;i < g_menuList.length;i++){
				var menu = $(g_menuList[i]);
				var flag = pattern.test(menu.find("input[name='menu_name']").val());
				if(false == flag){
					menu.hide();
				}
			}
		},
		showItems : function(){
			var g_menuList = $(".menuArea .item");
			for(var i = 0;i < g_menuList.length;i++){
				var menu = $(g_menuList[i]);
				menu.show();
			}
		}
};


var cookie = {
		init : function(){
			var order = eval("("+$.cookie("orderRestore")+")");
			if(!order){
				return;
			}else {
				var buildingId_restore = order.buildingId;
				if(rest.params.buildingId && rest.params.buildingId == buildingId_restore && order.restId == rest.params.restId){
					var menusArr = order.menuList.split(",");
					for(var i = 0;i<menusArr.length;i++){
						var menuArr =  menusArr[i].split(",");
						for(var j = 0 ;j<menuArr.length;j++){
							var menu_cookie = cookie.parseMenu(menuArr[j]);
							var menuId = menu_cookie.menuId;
							var menuNum = menu_cookie.menuNum;
							
							for(var j = 0;j<menuNum;j++){
								var menu_comp = cart.getMenu(menuId);
								cart.addMenu(menu_comp);
							}
							
						}
					}
				}
			}
		},
		parseMenu : function(menuStr){
			var menuArr = menuStr.split(";");
			var menuId = eval("({"+menuArr[0]+"})").id;
			var menuNum = eval("({"+menuArr[2]+"})").num;
			return {menuId : menuId,menuNum : menuNum};
		}
		
};
