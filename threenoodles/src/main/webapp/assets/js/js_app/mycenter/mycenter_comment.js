var commentObj = {
	initListener : function() {
		$("a[name='view_detail']").live("click",
				function() {
					if ("查看详情" == $(this).html()) {
						$(this).html("收起");
						$(this).parent().parent().find("div[class='detailArea']").show();
					} else {
						$(this).html("查看详情");
						$(this).parent().parent().find("div[class='detailArea']").hide();
					}
				});
		// 用户点击了查看更多评论
		$("#moreComments").click(function() {
				$(this).html("正在努力的为您加载。。。");
				var replyCount = $(this).attr("val");
				
				$.ajax({
					type : "POST",
					url : "/mycenter/comment/listComments",
					dataType: 'json',
					contentType: "application/x-www-form-urlencoded;charset=utf-8",
					cache: false,
					data : {
						start : replyCount
					},
					success : function(response) {
						
						$("#moreComments").attr("val", parseInt(replyCount) + 5);
						if (response && response.dataList && response.dataList.length >0) {
							
							var commentHtml = commentObj.getCommentHtml(response.dataList);
							
							$("#commentList").append(commentHtml);
							
							$("#moreComments").html("显示更多我的评论");
						} else {
							$("#moreComments").html("没有更多评论了");
						}
					}
				});
			});

		// 点击关闭按钮
		$(".pop .close").click(function() {
			$("#feedback_pop").hide();
		});
		// 点击输入框以后
		$("#feedback_pop .content").focus(function() {
			if ("" == $.trim($(this).val()) || "请输入您的回复信息" == $(this).val()) {
				$(this).val("");
			}

		});
		$("#feedback_pop .content").focusout(function() {
			if ("" == $.trim($(this).val())) {
				$(this).val("请输入您的回复信息");
			}
		});

		$(".reply_other .reply_content a").live("click",function() {
				// 初始化反馈组件
				$("#feedback_pop .content").val("请输入您的回复信息")
				$("#feedback_pop [name^=step_]").hide();
				$("#feedback_pop [name=step_1]").show();

				commentObj.commentId = $(this).attr("val");
				var replier_name = $(this).parent().parent().find(".reply_name").find("b").html();
				$("#feedback_pop .t").html("回复 " + replier_name);
				$("#feedback_pop").show();
		});

		// 用户点击了提交按钮以后
		$("#feedback_pop .submit").click(function() {
			var content = $.trim($("#feedback_pop .content").val());
			if (!commentObj.commentId || !content || "请输入您的回复信息" == content) {
				$(this).val("请输入您的回复信息");
				return;
			}
			commentObj.sendReply(commentObj.commentId, content);
		});
	},
	sendReply : function(comment_id, content) {
		$.ajax({
			type : "POST",
			url : "/mycenter/comment/submitComment",
			type : "POST",
			dataType: 'json',
			contentType: "application/x-www-form-urlencoded;charset=utf-8",
			cache: false,
			data : {
				comment_id : comment_id,
				content : content
			},
			beforeSend : function() {
				$("#feedback_pop [name^=step_]").hide();
				$("#feedback_pop [name=step_3]").show();
			},
			success : function(response) {
				if (response && response.success) {
					$("#feedback_pop [name^=step_]").hide();
					$("#feedback_pop [name='step_4']").show();
				} else {
					
					$("#feedback_pop").hide();
					Boxy.alert("<span style='color:red'>实在不好意思，系统正忙，请稍后重试！</span>");
				}
			}
		});
	},

	getCommentHtml : function(commentList) {
		var commentHtml = "";
		for (var i = 0; i < commentList.length; i++) {
			var comment = commentList[i];
			commentHtml += '<li>';
			commentHtml += '<div class="listItem li_0"></div>';
			commentHtml += '<div class="listItem li_1">'
					+ comment.content + '...</div>';
			commentHtml += '<div class="listItem li_2">' + comment.rest_name
					+ '</div>';
			commentHtml += '<div class="listItem li_3">';
			if (comment.replyList && comment.replyList.length>0) {
				for (var m = 0; m < comment.replyList.length; m++) {
					commentHtml += comment.replyList[m].nick_name + " ";
				}
			} else {
				commentHtml += "无回复";
			}
			commentHtml += '</div>';
			commentHtml += '<div class="listItem li_4">' + new Date(comment.comment_time).toLocaleString()+ '</div>';
			commentHtml += '<div class="listItem li_5">已读</div>';
			commentHtml += '<div style="float: right;" class="listItem li_5"><a name="view_detail" href="javascript:void(0);">查看详情</a></div>';
			commentHtml += '<div class="clear"></div>';

			commentHtml += '<div class="detailArea" style="display: none;">';
			commentHtml += '<div class="reply_my">';
			commentHtml += '<div class="reply_my_name">';
			commentHtml += '<b>我的评论详情：</b>';
			commentHtml += '</div>';
			commentHtml += '<div>';
			commentHtml += comment.content;
			commentHtml += '</div>';
			commentHtml += '</div>';

			if (comment.replyList_s && comment.replyList_s.length > 0) {
				for ( var j = 0; j < comment.replyList_s.length; j++) {
					commentHtml += '<div class="reply_other">';
					commentHtml += '<div class="reply_name">';
					commentHtml += '<b>' + comment.replyList_s[j].replier_name
							+ '的回复：</b>';
					commentHtml += '</div>';
					commentHtml += '<div class="reply_content">';
					commentHtml += comment.replyList_s[j].reply_content;
					commentHtml += '<a href="#" val="' + comment.id
							+ '">回复</a>';
					commentHtml += '</div>';
					commentHtml += '</div>';
				}
			}
			commentHtml += '<div class="clear"></div>';
			commentHtml += '</div>';

			commentHtml += '</li>';
		}
		return commentHtml;
	},
	// 要回复的评论的id
	commentId : 0
};