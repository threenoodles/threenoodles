//监听评论按钮的对象
var comment = {
	initListener : function() {
		// 点击按钮输入
		$(".rateArea .comment_quick a").live("click",function() {
			var textArea = $(this).parent().prev();
			// 用户点击了清除按钮
			if (6 == $(this).attr("val")) {
				textArea.val("");
			} else {
				textArea.get(0).click();
				var content = $(this).html();
				var contentHtml = textArea.val() + content;
				textArea.val(contentHtml);
			}
		});
		// 用户手动输入
		$(".rateArea .comment").live("click",function() {
			if ("您也可以手动填写" == $(this).val()) {
				$(this).val("");
			}
		});
	}
};

// 订单对象
var orderObj = {
	text : {
		0 : '&nbsp;',
		1 : '很差',
		2 : '差',
		3 : '还行',
		4 : '好',
		5 : '很好'
	},
	initListener : function() {

		$('div[name=r_star] .star div').live("mouseenter",orderObj.enter).live("mouseleave",orderObj.leave).live("click",orderObj.set);

		$("div[name='submit']").live("click",
				function() {
					var order_id = $(this).attr("val");
					// 质量和口味
					var taste = $(this).parent().find("div[value_='taste']").attr("val");
					// 态度
					var attitude = $(this).parent().find("div[value_='attitude']").attr("val");
					// 速度
					var speed = $(this).parent().find("div[value_='speed']").attr("val");
					// 评论内容
					var content = $(this).parent().find(".comment").val();
					if (!content || "您也可以手动填写" == content) {
						content = "味道不错，赞一个！";
					}
					
					if ("0" == taste || "0" == attitude || "0" == speed) {
						Boxy.alert("<span style='color:red'>请点击小星星，为本次订单打分~</span>");
					} else {
						orderObj.submit(order_id, taste, attitude, speed,content);
					}
				});

	},
	enter : function() {
		var i = $(this).attr('val');
		$(this).parent().removeClass().addClass('star star_' + i);
		$(this).parents('.rateBlock').find('.dytext').html(orderObj.text[i]);
		$(this).parents('.rateBlock').find('.tips').hide();
	},
	leave : function() {
		var i = $(this).parents('.rateBlock').attr('val');
		$(this).parents('.rateBlock').find('.star').removeClass().addClass(
				'star star_' + i);
		$(this).parents('.rateBlock').find('.dytext').html(orderObj.text[i]);
		if (i == 0)
			$(this).parents('.rateBlock').find('.tips').show();
	},
	set : function(e) {
		var i = $(this).attr('val');
		$(this).parents('.rateBlock').attr('val', i);

	},
	submit : function(order_id, taste, attitude, speed, content) {
		$.ajax({
				type : "POST",
				url : "/comment/submitComment",
				type : "POST",
				dataType: 'json',
				contentType: "application/x-www-form-urlencoded;charset=utf-8",
				cache: false,
				data : {
					order_id : order_id,
					taste : taste,
					attitude : attitude,
					speed : speed,
					content : content
				},
				success : function(response) {
					if (response && response.success) {
						Boxy.alert("恭喜您，评论成功，您已获得20积分！");
						$("div[name='submit'][val="+order_id+"]").hide();
					}else {
						Boxy.alert("<span style='color:red'>对不起，系统正忙，提交评论失败!</span>");
					}
				},
				error : function(){
					Boxy.alert("<span style='color:red'>对不起，系统正忙，提交评论失败!</span>");
				}
			});
	}
};
