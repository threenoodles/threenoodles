var feedbackObj = {
	initListener : function() {
		$("a[name='view_detail']").live(
				"click",
				function() {
					if ("查看详情" == $(this).html()) {
						$(this).html("收起");
						$(this).parent().parent().find(
								"div[class='detailArea']").show();
					} else {
						$(this).html("查看详情");
						$(this).parent().parent().find(
								"div[class='detailArea']").hide();
					}
				});
		// 用户点击了查看更多评论
		$("#moreFeedback").click(
				function() {
					$(this).html("正在努力的为您加载...");
					var replyCount = $(this).attr("val");
					$.ajax({
						type : "POST",
						url : "/mycenter/feedback/listFeedbacks",
						dataType: 'json',
						contentType: "application/x-www-form-urlencoded;charset=utf-8",
						cache: false,
						data : {
							start : replyCount
						},
						success : function(response) {
							$("#moreFeedback").attr("val", parseInt(replyCount) + 5);
							if (response && response.dataList && response.dataList.length >0) {
								
								var feedbackHtml = feedbackObj.getFeedbackHtml(response.dataList);
								
								$("#feedbackList").append(feedbackHtml);
								
								$("#moreFeedback").html("显示更多我的反馈信息");
							} else {
								$("#moreFeedback").html("没有更多反馈信息了");
							}
						}
					});
				});
		
		// 点击关闭按钮
		$(".pop .close").click(function() {
			$("#feedback_pop").hide();
		});
		// 点击输入框以后
		$("#feedback_pop .content").focus(function() {
			if ("" == $.trim($(this).val()) || "请输入您的回复信息" == $(this).val()) {
				$(this).val("");
			}

		});
		$("#feedback_pop .content").focusout(function() {
			if ("" == $.trim($(this).val())) {
				$(this).val("请输入您的回复信息");
			}
		});

		$(".reply_other .reply_content a").live("click", function() {
			// 初始化反馈组件
			$("#feedback_pop .content").val("请输入您的回复信息")
			$("#feedback_pop [name^=step_]").hide();
			$("#feedback_pop [name=step_1]").show();

			feedbackObj.feedbackId = $(this).attr("val");
			var replier_name = $(this).parent().parent().find(".reply_name").find("b").html();
			$("#feedback_pop .t").html("回复 " + replier_name);
			$("#feedback_pop").show();
		});
		//提交
		$("#feedback_pop .submit").live("click",function(){
			
			var content = $.trim($("#feedback_pop .content").val());
			if (!feedbackObj.feedbackId || !content || "请输入您的回复信息" == content) {
				$(this).val("请输入您的回复信息");
				return;
			}
			feedbackObj.sendReply(feedbackObj.feedbackId,content);
		});
	},
	getFeedbackHtml : function(feedbackList) {
		var feedbackHtml = "";
		for ( var i = 0; i < feedbackList.length; i++) {
			var feedback = feedbackList[i];
			feedbackHtml += '<li>';
			feedbackHtml += '<div class="listItem li_0"></div>';
			feedbackHtml += '<div class="listItem li_1">'
					+ feedback.content + '</div>';
			feedbackHtml += '<div class="listItem li_2">问题反馈</div>';
			feedbackHtml += '<div class="listItem li_3">';
			if (feedback.replyList_s && feedback.replyList_s.length>0) {
				for(var j = 0;j<feedback.replyList_s.length;j++){
					feedbackHtml += feedback.replyList_s[j].replier_name;
				}
			} else {
				feedbackHtml += "无回复";
			}
			feedbackHtml += '</div>';
			feedbackHtml += '<div class="listItem li_4">' + new Date(feedback.time).toLocaleString()+'</div>';
			feedbackHtml += '<div class="listItem li_5">已读</div>';
			feedbackHtml += '<div style="float: right;" class="listItem li_5"><a name="view_detail" href="javascript:void(0);">查看详情</a></div>';
			feedbackHtml += '<div class="clear"></div>';

			feedbackHtml += '<div class="detailArea" style="display: none;">';
			feedbackHtml += '<div class="reply_my">';
			feedbackHtml += '<div class="reply_my_name">';
			feedbackHtml += '<b>我的评论详情：</b>';
			feedbackHtml += '</div>';
			feedbackHtml += '<div>';
			feedbackHtml += feedback.content;
			feedbackHtml += '</div>';
			feedbackHtml += '</div>';

			for(var r = 0;r<feedback.replyList_s.length;r++){
				var reply = feedback.replyList_s[r];
				feedbackHtml += '<div class="reply_other">';
				feedbackHtml += '<div class="reply_name">';
				feedbackHtml += '<b>' + reply.replier_name + '的回复：</b>';
				feedbackHtml += '</div>';
				feedbackHtml += '<div class="reply_content">';
				feedbackHtml += reply.content;
				feedbackHtml += '<a href="#" val="'+feedback.id+'">回复</a>';
				feedbackHtml += '</div>';
				feedbackHtml += '</div>';
			}
			feedbackHtml += '<div class="clear"></div>';
			feedbackHtml += '</div>';
			feedbackHtml += '</li>';
		}

		return feedbackHtml;
	},
	sendReply : function(feedback_id, content) {
		$.ajax({
			type : "POST",
			url : "/mycenter/feedback/submitFeedbackReply",
			type : "POST",
			dataType: 'json',
			contentType: "application/x-www-form-urlencoded;charset=utf-8",
			cache: false,
			data : {
				feedback_id : feedback_id,
				content : content
			},
			beforeSend : function() {
				$("#feedback_pop [name^=step_]").hide();
				$("#feedback_pop [name=step_3]").show();
			},
			success : function(response) {
				if (response && response.success) {
					$("#feedback_pop [name^=step_]").hide();
					$("#feedback_pop [name='step_4']").show();
				} else {
					
					$("#feedback_pop").hide();
					Boxy.alert("<span style='color:red'>实在不好意思，系统正忙，请稍后重试！</span>");
				}
			}
		});
	}
};