<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp" %>
<link rel="stylesheet" type="text/css" href="/assets/css/restaurant/restaurants.css" media="all">

<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_lib/jquery.easing.1.3.min.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/restaurant/restaurants_broadcast.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/restaurant/restaurants.js"></script>

<!--[if IE 6]>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_lib/DD.belatedPNG.0.0.8a.min.js"></script>
<script type="text/javascript">
	DD_belatedPNG.fix('div,a,li,img');
	$("#restCategory").hide();
</script>
<![endif]-->

<script type="text/javascript">
	
	$(function() {

		var buildingId = '${sessionScope.building.id}';
		var userId = "${sessionScope.user.id}";
		restaurant.init(buildingId);
		restaurant.initListener();
		topNavi.init();
		notice.init();
		notice.initListener();
		
		feedback.init();
		
		//urgentNotice.initListener(buildingId);
		
		
		$(".weibo .iphone").click(function(){
			
			var popup = Boxy.ask("iPhone客户端需要跳转到一个客户的客户端演示页面，是否继续？",["确定","取消"],function(val){
       		 if("确定" == val){
       			 window.location.href="http://itunes.apple.com/cn/app/fei-fan-wang-shang-ding-can/id769725884?mt=8";
       		 }else {
       			 popup.hide();
       		 }
       	 },{title:"温馨提示"});
		});
	});
</script>
</HEAD>
<BODY>

<%@include file="/WEB-INF/jsp/common/common_top.jsp" %>

<!-- 反馈信息 -->
<%@include file="/WEB-INF/jsp/common/common_feedback.jsp"%>
	
	<div id="pushCustomerMsg"></div>
	<div id="container">
		<div class="progress">
			<div class="text">
				<div class="text_1">
					<a href="javascript:;">选餐馆</a>
				</div>
				<div class="text_2">选美食</div>
				<div class="text_3">确认送餐地址</div>
				<div class="text_4">餐到付款</div>
			</div>
			<div class="pbar">
				<div class="pbaron pbaron_1"></div>
			</div>
		</div>

		<div class="partl">
			<div class="section1 rollerBox">
				<div class="t"></div>
				<div class="body">
					<div class="roller_c">
						<div class="roller_b">
						
							<c:forEach items="${broadCastList}" var="broadcast">
								<a class="roller_item" href="${broadcast.target}"><img style="width: 719px;" alt="${broadcast.alt}" src="http://demo.cookwaimai.com/assets/images/broadcast/${broadcast.name}" /></a>
							</c:forEach>
						</div>
						<div class="roller_nav">
							<div class="roller_active" name="nav_0"></div>
						</div>
					</div>
				</div>
				<div class="b"></div>
			</div>

			<div class="section1 filterBox">
				<div class="t"></div>
				<div class="body">
					<div val="0" class="cb">营业中</div>
					<div val="1" class="cb">中式快餐</div>
					<div val="2" class="cb">西式快餐</div>
					<div val="3" class="cb">日韩料理</div>
					<div val="4" class="cb">蛋糕甜点</div>
					<div val="5" class="cb">奶茶饮料</div>
					<div val="6" class="cb">特色小吃</div>
					<div class="clear"></div>
				</div>
				<div class="b"></div>
			</div>

<!--店铺列表开始-->
			<div class="resList" id="restList">
					<c:forEach items="${restaurants}" var="restaurant" varStatus="status">
							<c:choose>
								<c:when test="${0 == (status.index+1)%3}">
									<div class="section2 res rlast" val="">
							</c:when>
							
							<c:otherwise>
									<div class="section2 res" val="">
							</c:otherwise>
						</c:choose>
						<div class="t"></div>
							<div class="body" id="${restaurant.id}" alias="${restaurant.alias}">
									<div><img src="http://demo.cookwaimai.com/assets/images/restaurant/${restaurant.id}/${restaurant.pic}" id="${restaurant.id}"></div>
									
								<c:if test="${ 1 eq restaurant.status }">
									<div val="1" class="notify">速度:${restaurant.speed} 口味:${restaurant.taste} 服务:${restaurant.attitude}
										<div style="color: green;font-weight:bolder;float: right;">营业中</div>
									</div>
								</c:if>
									
								<c:if test="${ 2 eq restaurant.status }">
									<div val="2" class="notify">速度:${restaurant.speed} 口味:${restaurant.taste}服务:${restaurant.attitude}
										<div style="color: grey;font-weight:bolder;float: right;">已打烊</div>
									</div>
								</c:if>
									
								<c:if test="${ 3 eq restaurant.status }">
									<div val="1" class="notify">速度:${restaurant.speed} 口味:${restaurant.taste} 服务:${restaurant.attitude}
										<div style="color: green;font-weight:bolder;float: right;">可预订</div>
									</div>
								</c:if>
									
								<div class="status"><a href="http://demo.cookwaimai.com/restaurant/toRestaurantDetail/${restaurant.alias}">${restaurant.name}</a></div>
							</div>
						<div class="b"></div>
					</div>
					</c:forEach>
			</div>

		</div>
<!--店铺列表结束-->
		<div class="partr">
			<div class="weibo">
				<a class="android" href="http://demo.cookwaimai.com/assets/android/cookwaimai.apk"><img src="/assets/images/restaurants/mobile.jpg"></a>
				
				<a class="iphone" href="#"><img src="/assets/images/restaurants/iPhone.png"></a>
			</div>
			
			<div class="section2 flying">
				<div class="t"></div>
				<div class="body">
					<div id="ord_rec_Title" class="title">
						<img id="ordersNotice" class="t1" alt="酷客最新订餐信息"
							src="http://demo.cookwaimai.com/assets/images/restaurants/dcxx.jpg" />
					</div>
					<UL id="orderList" class="list">
						<LI><span class="g">最新订单信息加载中...</span></LI>
					</UL>
				</div>
				<div class="b"></div>
			</div>
			
			<div class="section2 hotNews">
				<div class="t"></div>
				<div class="body">
				
				<c:forEach items ="${newsList}" var="news_" varStatus="sta">
					<c:if test="${0 == sta.index}">
						<a class="title" href="http://demo.cookwaimai.com/news/listNewsDetail/${news_.id}">活动公告专区</a>
					</c:if>
				</c:forEach>
				
					<ul id="notice_list" class="list">
						<c:forEach items ="${newsList}" var="news">
							<li>
								<a class="blink" href="http://demo.cookwaimai.com/news/listNewsDetail/${news.id}">${news.title}</a>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="b"></div>
			</div>

			<div class="section2 brandList">
				<div class="t"></div>
				<div class="body">
					<div class="title">品牌餐馆</div>
					<div class="list">
						<a href="http://www.4008823823.com.cn/" target="_blank"><IMG
							src="http://demo.cookwaimai.com/assets/images/restaurants/kfc.jpg"><BR> 肯德基</a><a
							href="http://www.pizzahut.com.cn/" target="_blank"><IMG
							src="http://demo.cookwaimai.com/assets/images/restaurants/pizzahut.jpg"><BR> 必胜客</a><a
							href="http://demo.cookwaimai.com/about/toAboutUs.action" target="_blank"><IMG
							src="http://demo.cookwaimai.com/assets/images/restaurants/mcdonalds.jpg"><BR> 广告位</a>
					</div>
				</div>
				<div class="b"></div>
			</div>
		</div>
		<div class="clear"></div>

	<div id="urgentNotice">
		<div style="text-align: center;"><h2 id="urgentNotice_title">外卖员也有春天</h2></div>
		<img id="urgentNotice_close" src="http://demo.cookwaimai.com/assets/images/common/cross.png"/>
		<div id="urgentNotice_content" style="cursor: pointer;">
		&nbsp&nbsp&nbsp&nbsp尊敬的酷友,来年再见,感谢2012世界还在,更感谢你们,见证了酷客的成长,2013我们与您同在.<br/>----小酷o(∩_∩)o
		</div>
	</div>
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp" %>
</BODY>
</HTML>
