<%@page language="java" contentType="text/html; charset=utf-8"%>
<div class="partl">
	<div class="title">个人中心</div>
	<div class="leftnav">
		<div class="t"></div>
		<div class="body">
			<ul name="mycenter_leftNavi">
				<li class="active" val="today"><a href="/mycenter/order?flag=today">今日订单</a></li>
				<li val="history"><a href="/mycenter/order?flag=history">历史订单</a></li>
				<li val="comment"><a href="/mycenter/comment?flag=comment">我的评论</a></li>
				<li val="feedback"><a href="/mycenter/feedback?flag=feedback">我的反馈</a></li>
				<li val="gift"><a href="/mycenter/gift?flag=gift">我的兑换礼品</a></li>
			</ul>
		</div>
		<div class="b"></div>
	</div>
	<div class="title">账户管理</div>
	<div class="leftnav">
		<div class="t"></div>
		<div class="body">
			<ul name="mycenter_leftNavi">
				<li val="password"><a href="/mycenter/password?flag=password">修改密码</a></li>
			</ul>
		</div>
		<div class="b"></div>
	</div>
</div>