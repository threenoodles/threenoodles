<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>

<LINK rel="stylesheet" type="text/css" href="http://demo.cookwaimai.com/assets/css/gift/gift.css" media="all">
<script type="text/javascript">
$(function(){
	gift.initListener();
});

//礼物对象
var gift = {
    initListener: function() {
        $(".block .item").click(function() {
            var gift_id = $(this).attr("id");
            var gift_points = $(this).attr("val");
            var gift_descr = $(this).find("input").val();
            var mask = null;
             Boxy.ask(gift_descr,["兑换","取消"],function(val){
      		 if("兑换" == val){
      			var confirm = Boxy.ask("兑换此礼品，将扣除您 " + gift_points + " 积分，确定要继续吗？",["确定","取消"],function(val){
      				if("确定" == val){
     					$.ajax({
     		            	type : "POST",
     						contentType: "application/x-www-form-urlencoded;charset=utf-8",
     						url : "/gift/exchangeGift",
     						type : "POST",
     						dataType: 'json',
     						cache: false,
     		                data: {
     		                	gift_id: gift_id
     		                },
     		               beforeSend:function(){
     							 mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
     						 },
     		                success: function(response) {
     		                	if(mask){
     								 mask.hide();
     							 }
     		                	if(response && response.success && "success" == response.success){
																		   
     		                		Boxy.alert("<span style='color:green'>恭喜您，礼品兑换成功，我们会在24小时内与您联系。</br>如果有任何疑问，欢迎拨打客服热线-${service_tel}！</span>",function(){
     		                			
     		                			window.location.href="/gift/toGifts";
     		                		});
     		                	}else if(response && response.success && "no_login" == response.success){
     		                		Boxy.ask("登录以后才能继续操作，要登录吗？",["确定","取消"],function(val){
     		                			if("确定" == val){
     		                				window.location.href="/login/toLogin?login_flag=toGift";
     		                			}
     		                		},{title : "登录提示"});
     		                	}else {
     		                		Boxy.alert("<span style='color:red'>对不起，您的积分不足，无法兑换。</br>如果有任何疑问，欢迎拨打客服热线-${service_tel}！</span>");
     		                	}
     		                },
     		               error : function() {
     		            	  if(mask){
  								 mask.hide();
  							 }
     		                		Boxy.alert("<span style='color:red'>对不起，系统正忙，请稍后重试。</br>如果有任何疑问，欢迎拨打客服热线-${service_tel}！</span>");
     						}
     		            });
      				}
      			},{title:"确认提示"});
      		 }
       	},{title:"温馨提示"});
            
        });
    },
    giftRegulation: function() {
        Boxy.alert("正常订餐，1元=5 积分；<br/>评论您的订单 1次=20 积分；<br/>参加酷客的活动，更有更多积分等你拿，谢谢您对酷客的支持");
    }
};
</script>
</head>
<body>

<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="tel" style="margin-top: -20px">酷客礼品中心电话：${service_tel}</div>
		<div class="gift_title">礼品中心 
		
		<c:if test="${null == sessionScope.user}">
			<a href="http://demo.cookwaimai.com/login/toLogin?flag=toGift">登录以后才能显示您的积分哦</a>
		</c:if>
		
		<c:if test="${null != sessionScope.user}">
			<a href="#">您现在拥有 ${sessionScope.user.gold} 积分</a>
		</c:if>
		 
		
		<a style="float: right ;margin-top: -5px;" href="#" onclick="gift.giftRegulation()">点击查看积分规则</a></div>		
		<c:forEach items="${order_by}" var="point">
		
				<div class="block">
				<div class="title">${point}积分（点击查看详情或兑换）</div>
				<div class="itemArea">
					
					<c:forEach items="${data[point]}" var="gift">
						<a href="#" class="item" id="${gift.id}" val="${point}">
							<img src="http://demo.cookwaimai.com/assets/images/gift/${gift.pic}"/>${gift.name}
							<input value="${gift.descr}" type="hidden"/>
						</a>
					</c:forEach>
					<div class="clear"></div>
				</div>
			</div>
		</c:forEach>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>