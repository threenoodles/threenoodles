<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>

<%@include file="/assets/jsp/common.jsp"%>
<link href="http://demo.cookwaimai.com/assets/css/order/order_success.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
	
<div id="backtotop"><a href="#"></a></div>
<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

<div id="pushCustomerMsg"></div>
	<div id="container">
		<a href="http://demo.cookwaimai.com/restaurant/toRestaurants/${building.id}" class="backLink blink">返回首页</a>
		<div class="progress">
			<div class="text">
				<div class="text_1"><a href="#">选餐馆</a></div>
				<div class="text_2"><a href="#">选美食</a></div>
				<div class="text_3"><a href="#">订单提交成功</a></div>
				<div class="text_4">餐到付款</div>
			</div>
			<div class="pbar"><div class="pbaron pbaron_3"></div></div>
		</div>
		<div class="partm">
			<div class="title"><div class="icon"></div>
				您的订单提交成功，您已经获得${sessionScope.order_score}积分！<a href="http://demo.cookwaimai.com/gift/toGifts"><font size="2px">查看礼品中心</font></a></div>
		    <div class="successtips">您的订单已发送至“<a href="#">${sessionScope.order_rest_name}</a>”，
		    <c:choose>
		    	<c:when test="${null != sessionScope.order_reserveTime}">
		    		订单的下发时间为&nbsp&nbsp<em>${sessionScope.order_reserveTime}</em>。
		    	</c:when>
		    	<c:otherwise>
			    	距离订单的配送时间约为&nbsp&nbsp<em>30分钟</em>，请您耐心等待。
		    	</c:otherwise>
		    </c:choose>	
		    <br>请保持您的电话畅通，方便配送员与您联系。
		    </div>	
			<div class="action">
			<a href="http://demo.cookwaimai.com/mycenter/order?naviFlag=mycenter&flag=today" class="btn_106_30 check_status">订单查询</a>
			<a href="http://demo.cookwaimai.com/restaurant/toRestaurants/${sessionScope.building.alias}" class="back"><span class="icon"></span>回到选餐馆页面</a>
			</div>
            <div class="hiddenDiv">
            
            
			</div>
		</div>
	</div>
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>	
</body>
</html>