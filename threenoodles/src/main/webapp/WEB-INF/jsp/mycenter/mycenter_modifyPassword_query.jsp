<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all"/>
<LINK rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/mycenter/changePassword.css" media=all />

<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		resetObj.initListener();
		mycenterObj.initListener(flag);
	});
	//查询对象
	var resetObj = {
			initListener : function(){
				//点击了查询了按钮以后
				$("#queryAccount").click(function(){
					var data = $("#account").val();
					$.ajax({
						type : "POST",
						url : "/mycenter/password/queryUserInfo",
						data : {
							data : data
						},
						success : function(response){
							 
							if(response && response.data){
								window.location.href="/mycenter/password/toModifyPassword?username="+response.data.nick_name;
							}else {
								Boxy.alert("未找到此用户");	
							}
						},
						error : function(){
							Boxy.alert("系统正忙，请稍后重试！");	
						}
					});
				});
			}
	};
</script>
</head>
<body>

	<div id="backtotop">
		<a href="#"></a>
	</div>
	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>

	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>
	
		<div class="partr">
			<div class="title">查询用户</div>
			<div class="tips">您可以填写您的用户名/手机号 查找您的账户名</div>
			<form id="form_chgPassword" action="#" method="post">
			<table>
				<tbody><tr>
						<td class="text">帐号</td>
						<td class="input">
							<div class="l"></div>
							<div class="m"><input id="account" name="oldpassword" type="text"></div>
							<div class="r"></div>
							<div class="checker_notice" id="account_notice">
							<div class="icon"></div><span>输入用户名/手机号</span>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><div id="queryAccount" class="btn_116_36 submit">查找用户</div></td>
				</tr>
				</tbody>
			</table>
			<input style="display: none;">
			</form>
		</div>
	</div>

	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
