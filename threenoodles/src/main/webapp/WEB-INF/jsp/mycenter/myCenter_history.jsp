<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css"
	rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript"
	src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>
<script type="text/javascript"
	src="http://demo.cookwaimai.com/assets/js/js_app/mycenter/mycenter_order.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var flag = "${flag}";
    comment.initListener();
    mycenterObj.initListener(flag);
    orderObj.initListener();
    topNavi.init();
    var order_history = {
        initListener: function() {
            $(".orderArea .li_6 a[name='view_detail']").live("click",function() {
                var orderId = $(this).attr("val");
                if($(".detailArea[val=" + orderId + "]").is(":visible")){
                	$(this).text("查看");
                	$(".detailArea[val=" + orderId + "]").hide();
                }else {
                	$(this).text("隐藏");
                	$(".detailArea[val=" + orderId + "]").show();
                }
            });
            $("#moreOrders").click(function() {
                var start = $(this).attr("val");
                if (!start) {
                    start = 5;
                }
                $.ajax({
                    type: "POST",
                    url: "/mycenter/order/list_historyOrder",
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    cache: false,
                    beforeSend:function(){
                    	$("#moreOrders").text("正在努力为您加载中。。。");
					 },
                    data: {
                        start: start,
                        flag: 'history'
                    },
                    success: function(response) {
                        if (response && response.dataList && response.dataList.length>0) {
                        	$.each(response.dataList,function(index,order){
	                            var html = order_history.getOrderHtml(order);
	                            $(".orderArea .body").append(html);
                        	});
                        	$("#moreOrders").attr("val",parseInt($("#moreOrders").attr("val"))+5);
                        	$("#moreOrders").text("显示更多我的订单");
                        }else {
                        	$("#moreOrders").text("没有更多订单了");
                        }
                    },
                    error: function() {
                        Boxy.alert("<span style='color:red'>对不起，系统正忙，提交评论失败!</span>")
                    }
                })
            })
        },
        getOrderHtml : function(order){

            var html = "";
            html += '<li>';
            html += '<div class="listItem li_0  "></div>';
            html += '<div class="listItem li_1">';
            html += '	<a href="javascript:void(0);">'+order.id+'</a>';
            html += '</div>';
            html += '<div class="listItem li_2">';
            html += order.rest_name;
            html += '</div>';
            html += '<div class="listItem li_3">';
            html += order.phone;
            html += '</div>';
            html += '<div class="listItem li_4">';
            html += new Date(parseFloat(order.ordered_time)).toLocaleString();
            html += '</div>';
            html += '<div class="listItem li_5">';
            html += "￥"+(parseFloat(order.totalPrice)+parseFloat(order.carriage));
            html += '</div>';
            html += '<div class="listItem li_6">';
            html += '<a name="view_detail" val="'+order.id+'" href="javascript:void(0);">查看</a> | <a href="javascript:void(0);"> '+(5 == order.status?"已评价":"未评价")+'</a>';
            html += '</div>';
            html += '<div class="clear"></div>';
            html += '<div val="'+order.id+'" class="detailArea" style="display: none;">';
            html += '<div class="infoArea">';
            html += '<div class="restName">';
            html += '<b>'+order.rest_name+'</b>';
            html += '</div>';
            html += '<div class="status">';
            html += '<b>订单状态：</b>';
            
            if(5 == order.status){
	            html += '<em class="tooltips">已评价<span></span></em>';
            }else if(2 == order.status){
	            html += '<em class="tooltips">烹饪中<span></span></em>';
            }else if(3 == order.status){
	            html += '<em class="tooltips">配送中<span></span></em>';
            }else if(4 == order.status){
	            html += '<em class="tooltips">配送完成<span></span></em>';
            }else if(7 == order.status){
            	
	            html += '<em class="tooltips">下单时间未到<span></span></em>';
            }
            
            html += '</div>';
            html += '<div class="clear"></div>';
            html += '<div class="orderId">';
            html += '<b>订单号：</b>';
            html += order.id;
            html += '</div>';
            html += '<div class="orderTime">';
            html += '<b>下单时间：</b>';
            html += new Date(parseFloat(order.ordered_time)).toLocaleString();
            html += '</div>';
            html += '<div class="clear"></div>';
            html += '<table class="detail">';
            html += '<colgroup>';
            html += '<col width="250">';
            html += '<col width="110">';
            html += '</colgroup>';
            html += '<tbody>';
            
            $.each(order.menus,function(index,menu){
	            html += '<tr>';
	            html += '<td>'+menu.name+'</td>';
	            html += '<td>'+menu.count+' × '+menu.price+' = ￥'+parseFloat(menu.price)*parseInt(menu.count)+'</td>';
	            html += '</tr>';
            });
            
            html += '<tr>';
            html += '<td>送餐费</td>';
            html += '<td>1 × '+order.carriage+' = ￥'+order.carriage+'</td>';
            html += '</tr>';
            html += '</tbody>';
            html += '</table>';
            html += '<div class="infoBlock price">';
            html += '<b>总计：</b><em>'+order.totalPrice+'+'+order.carriage+'='+(parseFloat(order.totalPrice)+parseFloat(order.carriage))+'元</em>';
            html += '</div>';
            html += '<div class="infoBlock">';
            html += '<b>您的手机：</b>';
            html +=  order.phone;
            html += '</div>';
            html += '<div class="clear"></div>';
            html += '<div class="remark">';
            html += '<b>备注：</b>';
            html += order.remark;
            html += '</div>';
            html += '<div class="address">';
            html += '<b>地址：</b>';
            html += order.address;
            html += '</div>';
            html += '<br>';
            html += '<div class="restTel">';
            html += '<b>催餐电话：</b> '+order.rest_telephone;
            html += '</div>';
            html += '<div class="clear"></div>';
            html += '</div>';
            html += '<div class="rateArea">';
            
			//已经评论
            if(order.status && 5 == order.status){
            	html += '<div>订单已评价，20积分已到账~</div>';
                html += '<div value_="taste" class="rateBlock">';
                html += '<b>菜品口味：</b>';
                html += '<div class="star star_'+order.comment.taste+'">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<div value_="attitude" class="rateBlock">';
                html += '<b>服务态度：</b>';
                html += '<div class="star star_'+order.comment.attitude+'">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<div value_="speed" class="rateBlock">';
                html += '<b>送餐速度：</b>';
                html += '<div class="star star_'+order.comment.speed+'">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<textarea name="r_comment" class="comment" tips="发表评论">'+order.comment.content;
                html += '</textarea>';
                html += '<div class="comment_quick">';
                html += '<a href="javascript:void(0);">赞一个！</a> <a href="javascript:void(0);" val="1">味道不错</a> <a href="javascript:void(0);" val="2">速度挺快</a> <a href="javascript:void(0);" val="3">服务不错</a> <a href="javascript:void(0);" val="4">一般</a> <a href="javascript:void(0);" val="5">有点失望哦</a> <a href="javascript:void(0);" val="6">点击清除</a>';
                html += '</div>';
                
                html += '<div class="btn_91_28_g submit">已评价</div>';
                
                html += '</div>';
                
            }else {
            	
            	html += '<div>评价订单，再送您 20 积分哦 亲 ~</div>';
                html += '<div value_="taste" class="rateBlock" name="r_star" val="0">';
                html += '<b>菜品口味：</b>';
                html += '<div class="star star_0">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div class="dytext">未打分</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<div value_="attitude" class="rateBlock" name="r_star" val="0">';
                html += '<b>服务态度：</b>';
                html += '<div class="star star_0">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div class="dytext">未打分</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<div value_="speed" class="rateBlock" name="r_star" val="0">';
                html += '<b>送餐速度：</b>';
                html += '<div class="star star_0">';
                html += '<div val="1"></div>';
                html += '<div val="2"></div>';
                html += '<div val="3"></div>';
                html += '<div val="4"></div>';
                html += '<div val="5"></div>';
                html += '</div>';
                html += '<div class="dytext">未打分</div>';
                html += '<div style="display: none;" class="tips">';
                html += '<em>点击小星星为商家打分</em>';
                html += '</div>';
                html += '</div>';
                html += '<textarea name="r_comment" class="comment" tips="发表评论">您也可以手动填写';
                html += '</textarea>';
                html += '<div class="comment_quick">';
                html += '<a href="javascript:void(0);">赞一个！</a> <a href="javascript:void(0);" val="1">味道不错</a> <a href="javascript:void(0);" val="2">速度挺快</a> <a href="javascript:void(0);" val="3">服务不错</a> <a href="javascript:void(0);" val="4">一般</a> <a href="javascript:void(0);" val="5">有点失望哦</a> <a href="javascript:void(0);" val="6">点击清除</a>';
                html += '</div>';
                
                if(order.status || 2 == order.status || 3 == order.status || 4 == order.status){
                	
	                html += '<div class="btn_91_28 submit" name="submit" val="'+order.id+'">提交评价</div>';
                }else if(7 == order.status){
                    html += '<div class="btn_91_28_g submit">配送时间未到</div>';
                }
                
                html += '</div>';
            }
            
            
            html += '<div class="clear"></div>';
            html += '</div>';
            html += '</li>';
            
            return html;
        }
    };
    order_history.initListener();
});
</script>
</head>
<body>
	<div id="backtotop">
		<a href="#"></a>
	</div>
	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

	<div id="pushCustomerMsg"></div>
	<div id="container">
		<div class="top_space"></div>

		<!-- 个人中心左侧导航代码块 -->
		<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>

		<div class="partr">
			<div class="section11 tips">
				<div class="t"></div>
				<div class="body">
					欢迎您，亲爱的 ${sessionScope.user.nick_name}, 最近您在酷客网完成了 <em>${sessionScope.user_detail.order_count}</em>
					个订单，已经有了 <em>${sessionScope.user_detail.gold}</em> 点积分，<a href="#">礼品中心&nbsp</a>已经上线,更多礼品等你拿~
				</div>
				<div class="b"></div>
			</div>

			<div class="section11 orderArea">
				<div class="t"></div>
				<ul class="body">
					<li class="th">
						<div class="listItem li_0"></div>
						<div class="listItem li_1">订单号</div>
						<div class="listItem li_2">餐厅名称</div>
						<div class="listItem li_3">您的手机号</div>
						<div class="listItem li_4">下单时间</div>
						<div class="listItem li_5">订单金额</div>
						<div class="listItem li_6">操作</div>
						<div class="clear"></div>
					</li>


					<c:forEach items="${dataList}" var="order">
						<!-- 未评价 -->
						<li>
							<div class="listItem li_0  "></div>
							<div class="listItem li_1">
								<a href="javascript:void(0);">${order.id}</a>
							</div>
							<div class="listItem li_2">${order.rest_name}</div>
							<div class="listItem li_3">${order.phone}</div>
							<div class="listItem li_4">
								<fmt:formatDate value="${order.ordered_time}"
									pattern="yyyy-MM-dd HH:mm:ss" />
							</div>
							<div class="listItem li_5">
								￥${order.totalPrice+order.carriage}</div>
							<div class="listItem li_6">
								<a name="view_detail" val="${order.id}"
									href="javascript:void(0);">查看</a> | <a
									href="javascript:void(0);"> <c:if
										test="${5 != order.status && null == order.comment }">未评价</c:if>
									<c:if test="${5 == order.status && null != order.comment }">已评价</c:if></a>
							</div>
							<div class="clear"></div>

							<div val="${order.id }" class="detailArea" style="display: none;">
								<div class="infoArea">
									<div class="restName">
										<b>${order.rest_name}</b>
									</div>

									<div class="status">
										<b>订单状态：</b>
										<c:if test="${2 == order.status}">
											<em class="tooltips">烹饪中<span></span></em>
										</c:if>
										<c:if test="${3 == order.status}">
											<em class="tooltips">配送中<span></span></em>
										</c:if>
										<c:if test="${4 == order.status}">
											<em class="tooltips">配送完成<span></span></em>
										</c:if>
										<c:if test="${5 == order.status}">
											<em class="tooltips">已评价<span></span></em>
										</c:if>
										<c:if test="${7 == order.status}">
											<em class="tooltips">配送时间未到<span></span></em>
										</c:if>

									</div>

									<div class="clear"></div>
									<div class="orderId">
										<b>订单号：</b> ${order.id }
									</div>
									<div class="orderTime">
										<b>下单时间：</b>
										<fmt:formatDate value="${order.ordered_time}"
											pattern="yyyy-MM-dd HH:mm:ss" />
									</div>
									<div class="clear"></div>
									<table class="detail">
										<colgroup>
											<col width="250">
											<col width="110">
										</colgroup>
										<tbody>
											<c:forEach items="${order.menus}" var="menu">
												<tr>
													<td>${menu.name }</td>
													<td>${menu.count } × ${menu.price } =
														￥${menu.count*menu.price }</td>
												</tr>
											</c:forEach>

											<tr>
												<td>送餐费</td>
												<td>1 × ${order.carriage } = ￥${order.carriage }</td>
											</tr>
										</tbody>
									</table>
									<div class="infoBlock price">
										<b>总计：</b><em>${order.totalPrice }+${order.carriage}=${order.totalPrice+order.carriage }元</em>
									</div>
									<div class="infoBlock">
										<b>您的手机：</b> ${order.phone }
									</div>
									<div class="clear"></div>
									<div class="remark">
										<b>备注：</b> ${order.remark }
									</div>
									<div class="address">
										<b>地址：</b> ${order.address }
									</div>
									<br>
									<div class="restTel">
										<b>催餐电话：</b> ${order.rest_telephone }
									</div>
									<div class="clear"></div>

								</div>


								<!-- 未评价订单 -->
								<c:if test="${5 != order.status && null == order.comment }">
									<div class="rateArea">
										<div style="margin: 0px auto">评价订单，再送您 20 积分哦 亲 ~</div>

										<div value_="taste" class="rateBlock" name="r_star" val="0">
											<b>菜品口味：</b>
											<div class="star star_0">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div class="dytext">未打分</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<div value_="attitude" class="rateBlock" name="r_star" val="0">
											<b>服务态度：</b>
											<div class="star star_0">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div class="dytext">未打分</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<div value_="speed" class="rateBlock" name="r_star" val="0">
											<b>送餐速度：</b>
											<div class="star star_0">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div class="dytext">未打分</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<textarea name="r_comment" class="comment" tips="发表评论">您也可以手动填写</textarea>
										<div class="comment_quick">
											<a href="javascript:void(0);">赞一个！</a> <a
												href="javascript:void(0);" val="1">味道不错</a> <a
												href="javascript:void(0);" val="2">速度挺快</a> <a
												href="javascript:void(0);" val="3">服务不错</a> <a
												href="javascript:void(0);" val="4">一般</a> <a
												href="javascript:void(0);" val="5">有点失望哦</a> <a
												href="javascript:void(0);" val="6">点击清除</a>
										</div>

										<div class="btn_91_28 submit" name="submit" val="${order.id}">提交评价</div>

									</div>
								</c:if>

								<!-- 已评价订单 -->
								<c:if test="${5 == order.status && null != order.comment }">

									<div class="rateArea">
										<div>订单已评价，20积分已到账~</div>

										<div id="taste" class="rateBlock">
											<b>菜品口味：</b>
											<div class="star star_${order.comment.taste }">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<div id="attitude" class="rateBlock">
											<b>服务态度：</b>
											<div class="star star_${order.comment.attitude }">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<div id="speed" class="rateBlock">
											<b>送餐速度：</b>
											<div class="star star_${order.comment.speed }">
												<div val="1"></div>
												<div val="2"></div>
												<div val="3"></div>
												<div val="4"></div>
												<div val="5"></div>
											</div>
											<div style="display: none;" class="tips">
												<em>点击小星星为商家打分</em>
											</div>
										</div>


										<textarea name="r_comment" class="comment" tips="发表评论">${order.comment.content }
										</textarea>
										<div class="comment_quick">
											<a href="javascript:void(0);">赞一个！</a> <a
												href="javascript:void(0);" val="1">味道不错</a> <a
												href="javascript:void(0);" val="2">速度挺快</a> <a
												href="javascript:void(0);" val="3">服务不错</a> <a
												href="javascript:void(0);" val="4">一般</a> <a
												href="javascript:void(0);" val="5">有点失望哦</a> <a
												href="javascript:void(0);" val="6">点击清除</a>
										</div>

									</div>
								</c:if>

								<div class="clear"></div>
							</div>

						</li>

						<!-- 已经评价的订单 -->
					</c:forEach>

				</ul>
				<div class="b"></div>
			</div>

			<div id="moreOrders" class="more" val="6">显示更多“我的订单”</div>
		</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
