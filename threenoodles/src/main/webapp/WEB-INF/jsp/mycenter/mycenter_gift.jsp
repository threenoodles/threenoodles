<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/mygift.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		mycenterObj.initListener(flag);
		//topNav.initListener();
		//topNavi.init();
	});
</script>
</head>
<body>
	<div id="backtotop">
		<a href="#"></a>
	</div>
	
	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>


	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>
		
		<div class="partr">
			<div class="section11 tips">
				<div class="t"></div>
				<div class="body">
					欢迎您，亲爱的
					${sessionScope.user.nick_name}, 最近您在酷客网完成了 <em>${sessionScope.user_detail.order_count}</em> 个订单，已经有了 <em>${sessionScope.user_detail.gold}</em> 点积分，<a href="#">礼品中心&nbsp</a>已经上线,更多礼品等你拿~
				</div>
				<div class="b"></div>
			</div>

		<div class="partr">
		<div class="section11 listArea">
			<div class="t"></div>
			<ul class="body">
				<li class="th">
					<div class="listItem li_1">订单号</div>
					<div class="listItem li_2">礼品名称</div>
					<div class="listItem li_3">数量</div>
					<div class="listItem li_4">消耗积分</div>
					<div class="listItem li_5">兑换时间</div>
					<div class="listItem li_6">联系方式</div>
					<div class="listItem li_7">兑换状态</div>
					<div class="clear"></div>
				</li>
				
				<c:forEach items="${dataList}" var="gift">
				<li>
					<div class="listItem li_0  "></div>
					<div class="listItem li_1"><a>1010${gift.id}</a></div>
					<div class="listItem li_2">${gift.gift_name}</div>
					<div class="listItem li_3">1</div>
					<div class="listItem li_4">${gift.points}</div>
					<div class="listItem li_5"><fmt:formatDate value="${gift.time_exchange}" pattern="yyyy-MM-dd HH:mm:ss" />
					</div>
					<div class="listItem li_6"><c:if test="${empty gift.telephone}">QQ:${gift.qq}</c:if><c:if test="${empty gift.qq}">TEL:${gift.telephone}</c:if></div>
					<div class="listItem li_7"><c:if test="${1 == gift.status}">兑换中</c:if><c:if test="${2 == gift.status}">已兑换</c:if></div>
					<div class="clear"></div>
					
				</li>
				</c:forEach>
			</ul>
			<div class="b"></div>
		</div>
		</div>
		</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
