<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>

<%@include file="/assets/jsp/common.jsp"%>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/mycenter/mycenter_order.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		comment.initListener();
		mycenterObj.initListener(flag);
		orderObj.initListener();
		topNavi.init();
	});

</script>
</head>
<body>

	<div id="backtotop">
		<a href="#"></a>
	</div>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>

	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>

		<div class="partr">
			<div class="section11 tips">
				<div class="t"></div>
				<div class="body">
					欢迎您，亲爱的
					${sessionScope.user.nick_name}, 最近您在酷客网完成了 <em>${sessionScope.user_detail.order_count}</em> 个订单，已经有了 <em>${sessionScope.user_detail.gold}</em> 点积分，<a href="#">礼品中心&nbsp</a>已经上线,更多礼品等你拿~
				</div>
				<div class="b"></div>
			</div>

			<c:forEach items="${dataList}" var="order">

				<div class="section11 orderArea">
					<div class="t"></div>
					<div class="body">
						<div class="infoArea">
							<div class="restName">
								<b>${order.rest_name}</b>
							</div>

							<div class="restTel">
								<b>催餐电话：</b> <em>${order.rest_telephone}</em>
							</div>

							<div class="clear"></div>
							<div class="orderId">
								<b>订单号：</b>
								${order.id}
							</div>
							<div class="orderTime">
								<b>下单时间：</b>
								<fmt:formatDate value="${order.ordered_time}" pattern="yyyy-MM-dd"/>
							</div>
							<div class="clear"></div>
							<table class="detail">
								<colgroup>
									<col width="250">
									<col width="110">
								</colgroup>
								<tbody>

									<c:forEach items="${order.menus}" var="menu">
										<tr>
											<td>${menu.name}</td>
											<td>${menu.count} × ${menu.price}=￥${menu.count*menu.price}</td>
										</tr>
									</c:forEach>
									<tr>
										<td>送餐费</td>
										<td>1 × ${order.carriage}=￥${order.carriage}</td>
									</tr>
								</tbody>
							</table>
							<div class="infoBlock price">
								<b>总计：</b><em>${order.totalPrice}+${order.carriage}=${order.totalPrice+order.carriage}元</em>
							</div>
							<div class="infoBlock">
								<b>您的电话：</b>
								${order.phone}
							</div>
							<div class="clear"></div>
							<div class="remark">
								<b>备注：</b>
								${order.remark}
							</div>
							<div class="address">
								<b>地址：</b>
								${order.address}
							</div>
							<br />
							<div class="status">
							
							<b>订单状态：</b>
								<c:if test="${2 == order.status}">
									<em class="tooltips">烹饪中<span></span></em>
								</c:if>
								<c:if test="${3 == order.status}">
									<em class="tooltips">配送中<span></span></em>
								</c:if>
								<c:if test="${4 == order.status}">
									<em class="tooltips">配送完成<span></span></em>
								</c:if>
								<c:if test="${5 == order.status}">
									<em class="tooltips">已评价<span></span></em>
								</c:if>
								<c:if test="${7 == order.status}">
									<em class="tooltips">配送时间未到<span></span></em>
								</c:if>
							
							</div>
							<div class="clear"></div>

						</div>
						<div class="rateArea">
						
						<c:if test="${5 == order.status && null != order.comment}">
							<div style="margin: 0px auto">订单已评价，20积分已到账~</div>
							<div class="rateBlock" name="r_star_">
								<b>菜品口味：</b>
									<div class="star star_${order.comment.taste}">
										<div val="1"></div>
										<div val="2"></div>
										<div val="3"></div>
										<div val="4"></div>
										<div val="5"></div>
									</div>
							</div>

							<div class="rateBlock" name="r_star_">
								<b>服务态度：</b>
								<div class="star star_${order.comment.attitude}">
									<div val="1"></div>
									<div val="2"></div>
									<div val="3"></div>
									<div val="4"></div>
									<div val="5"></div>
								</div>
							</div>

							<div class="rateBlock" name="r_star_" val="0">
								<b>送餐速度：</b>
								<div class="star star_${order.comment.speed}">
									<div val="1"></div>
									<div val="2"></div>
									<div val="3"></div>
									<div val="4"></div>
									<div val="5"></div>
								</div>
							</div>

							<textarea name="r_comment" class="comment" tips="发表评论">${order.comment.content}</textarea>
							<div class="comment_quick">
								<a href="javascript:void(0);" val="0">赞一个！</a> <a
									href="javascript:void(0);" val="1">味道不错</a> <a
									href="javascript:void(0);" val="2">速度挺快</a> <a
									href="javascript:void(0);" val="3">服务不错</a> <a
									href="javascript:void(0);" val="4">一般</a> <a
									href="javascript:void(0);" val="5">有点失望哦</a> <a
									href="javascript:void(0);" val="6">点击清除</a>
							</div>
						</c:if>
						
						<c:if test="${5 != order.status}">
							<div style="margin: 0px auto">评价订单，即送20积分哦~</div>
							<div value_="taste" class="rateBlock" name="r_star" val="0">
								<b>菜品口味：</b>
									<div class="star star_0">
										<div val="1"></div>
										<div val="2"></div>
										<div val="3"></div>
										<div val="4"></div>
										<div val="5"></div>
									</div>
								<div class="dytext">未打分</div>
								
								<div style="display: none;" class="tips">
									<em>点击小星星为商家打分</em>
								</div>
							</div>


							<div value_="attitude" class="rateBlock" name="r_star" val="0">
								<b>服务态度：</b>
								<div class="star star_0">
									<div val="1"></div>
									<div val="2"></div>
									<div val="3"></div>
									<div val="4"></div>
									<div val="5"></div>
								</div>
								<div class="dytext">未打分</div>
								<div style="display: none;" class="tips">
									<em>点击小星星为商家打分</em>
								</div>
							</div>


							<div value_="speed" class="rateBlock" name="r_star" val="0">
								<b>送餐速度：</b>
								<div class="star star_0">
									<div val="1"></div>
									<div val="2"></div>
									<div val="3"></div>
									<div val="4"></div>
									<div val="5"></div>
								</div>
								<div class="dytext">未打分</div>
								<div style="display: none;" class="tips">
									<em>点击小星星为商家打分</em>
								</div>
							</div>

							<textarea name="r_comment" class="comment" tips="发表评论">您也可以手动填写</textarea>
							<div class="comment_quick">
								<a href="javascript:void(0);" val="0">赞一个！</a> <a
									href="javascript:void(0);" val="1">味道不错</a> <a
									href="javascript:void(0);" val="2">速度挺快</a> <a
									href="javascript:void(0);" val="3">服务不错</a> <a
									href="javascript:void(0);" val="4">一般</a> <a
									href="javascript:void(0);" val="5">有点失望哦</a> <a
									href="javascript:void(0);" val="6">点击清除</a>
							</div>
						
						</c:if>
							<c:if test="${2==order.status || 3==order.status || 4 == order.status}">
								<div class="btn_91_28 submit" name="submit" val="${order.id}">提交评价</div>
							</c:if>

							<c:if test="${5 == order.status}">
								<div class="btn_91_28_g submit">已评价</div>
							</c:if>

							<c:if test="${7 == order.status}">
								<div class="btn_91_28_g submit">订单未完成</div>
							</c:if>
						</div>
						<div class="clear"></div>
					</div>
					<div class="b"></div>
				</div>
			</c:forEach>

		</div>
	</div>

	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
