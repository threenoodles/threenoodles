<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/mycenter/mycenter_feedback.js"></script>


<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myreply.css" rel="stylesheet" type="text/css" media="all"/>



<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		var userId = "<s:property value='#session.id'/>";
		mycenterObj.initListener(flag);
		feedbackObj.initListener();
		//feedback.init(userId);
		topNavi.init();
	});

</script>
</head>
<body>
	<div id="backtotop">
		<a href="#"></a>
	</div>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

	<div class="pop" id="feedback_pop" style="display: none">
		<div class="c" style="top: 273.5px;">
			<div class="t" style="text-align: center;">回复</div>
			<div name="step_1" style="display: block;">
				<div>
					<textarea tips="请输入您的回复信息" class="content" name="content">请输入您的回复信息</textarea>
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
			</div>
			<div name="step_3" style="display: none;">
				<div class="alertText">正在提交中...</div>
			</div>
			<div name="step_4" style="display: none;">
				<div class="alertText">提交成功！</div>
			</div>
			<div class="tel">免费客服热线：${service_tel}</div>
			<div class="close">×</div>
		</div>
		
		<div class="mask"></div>
	</div>

	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>
		
	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>

		<div class="partr">
			<div class="section11 tips">
				<div class="t"></div>
				<div class="body">
					欢迎您，亲爱的
					${sessionScope.user.nick_name}, 最近您在酷客网完成了 <em>${sessionScope.user_detail.order_count}</em> 个订单，已经有了 <em>${sessionScope.user_detail.gold}</em> 点积分，<a href="#">礼品中心&nbsp</a>已经上线,更多礼品等你拿~
				</div>
				<div class="b"></div>
			</div>

			<div class="section11 orderArea">
				<div class="t"></div>
				<ul id="feedbackList" class="body">
					<li class="th">
						<div class="listItem li_0"></div>
						<div class="listItem li_1">您的反馈摘要</div>
						<div class="listItem li_2">反馈的类型</div>
						<div class="listItem li_3">回复者</div>
						<div class="listItem li_4">反馈时间</div>
						<div class="listItem li_5">是否已读</div>
						<div class="listItem li_6" style="float: right;">操作</div>
						<div class="clear"></div>
					</li>

					<c:forEach items="${dataList}" var="feedback">
						<li>
							<div class="listItem li_0"></div>
							<div class="listItem li_1">
								${feedback.content}
							</div>
							<div class="listItem li_2">
								问题反馈
							</div>
							<div class="listItem li_3">
								${feedback.responder_name }
							</div>
							<div class="listItem li_4">
								<fmt:formatDate value="${feedback.time }" pattern="yyyy年MM月dd日 HH:mm:ss" />
							</div>
							<div class="listItem li_5">
								<a href="javascript:void(0);">未读</a>
							</div>
							<div class="listItem li_6" style="float: right;">
								<a href="javascript:void(0);" name="view_detail">查看详情</a>
							</div>
							<div class="clear"></div>



							<div class="detailArea">
								<div class="reply_my">
									<div class="reply_my_name">
										<b>我的评论详情：</b>
									</div>
									<div>
										${feedback.content}
									</div>
								</div>
								
								<c:forEach items="${feedback.replyList_s}" var="reply">
									<div class="reply_other">
									<div class="reply_name">
										<b>${reply.replier_name}的回复：</b>
									</div>
									<div class="reply_content">
										${reply.content}
										<a val="${feedback.id}" href="#">回复</a>
									</div>
									</div>
								</c:forEach>
								
								<div class="clear"></div>
							</div>
						</li>
					</c:forEach>

				</ul>
				<div class="b"></div>
			</div>

			<c:if test="${empty dataList }">
				<div id="moreFeedback" class="more" val="0">您没有“未读”的反馈信息，点击显示“已读”反馈</div>
			</c:if>
			<c:if test="${!empty dataList }">
				<div id="moreFeedback" class="more" val="0">显示更多我的反馈信息</div>
			</c:if>
		</div>
	</div>
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
