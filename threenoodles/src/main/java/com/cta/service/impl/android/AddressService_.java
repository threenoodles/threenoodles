/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl.android;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.dao.impl.android.AddressDao_;

/**
 * @ClassName: AddressService_android
 * @Description:
 * @author chenwenpeng
 * @date 2013-8-28 下午10:24:12
 */
@Service
public class AddressService_ {
	@Autowired
	private AddressDao_ addressDao;
	/**
	 * @throws SQLException
	* @Title: listAddress
	* @Description:
	* @param @return
	* @return List<Map<String,Object>>
	* @throws
	*/
	public List<Map<String, Object>> listAddress() throws SQLException {

		List<Map<String,Object>> regions = addressDao.listRegions();
		if(null != regions && regions.size() > 0){
			for(Map<String,Object> region : regions){
				Integer regionId = (Integer) region.get("id");
				List<Map<String,Object>> buildings = addressDao.listBuildings(regionId);
				region.put("buildingList", buildings);
			}
		}
		return regions;
	}

}
