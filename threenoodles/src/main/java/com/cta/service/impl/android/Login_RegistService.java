/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl.android;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.constant.RegexType;
import com.cta.dao.impl.RegistDao;
import com.cta.dao.impl.android.Login_RegistDao;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ValidateUtil;

/**
 * @ClassName: Login_RegistService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-9-1 上午11:01:57
 */
@Service
public class Login_RegistService {
    @Autowired
	private Login_RegistDao login_registDao;
    
    @Autowired
	private RegistDao registDao;
    

	/**
	 * @throws SQLException 
	 * @throws UnsupportedEncodingException 
	* @Title: login
	* @Description: 
	* @param @param username
	* @param @param password
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public JSONObject login(String username, String password) throws UnsupportedEncodingException, SQLException {
		JSONObject result = new JSONObject();
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
			result.put("flag", DataStatus.FAIL);
			return result;
		}
		username = java.net.URLDecoder.decode(username,"UTF-8");
		Map<String,Object> userInDB = login_registDao.login(username,password);
		if(null == userInDB){
			result.put("flag", DataStatus.FAIL);
			return result;
		}
		result.put("id", userInDB.get("id"));
		result.put("name", userInDB.get("name"));
		result.put("flag", DataStatus.SUCCESS);
		return result;
	}

	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 * @param phone 
	 * @throws UnsupportedEncodingException 
	* @Title: regist
	* @Description: 
	* @param @param username
	* @param @param password
	* @param @return 
	* @return JSONObject 
	* @throws 
	*/ 
	public JSONObject regist(String username, String password, String phone) throws UnsupportedEncodingException, SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		JSONObject result = new JSONObject();
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(phone)){
			result.put("flag", "fail_usernameUsed");
			return result;
		}
		
		username = java.net.URLDecoder.decode(username.trim(),"UTF-8");
	
		UserEntity user = new UserEntity();
		user.setNick_name(username);
		user.setPassword(password);
		user.setTelephone(phone);
		user.setReal_name("none");
		user.setReg_time(new Date());
		user.setOrder_count(0);
		user.setUn_comment(0);
		user.setGold(SystemGlobals.getIntPreference("default.score.regist", 30));
		user.setEnabled(DataStatus.ENABLED_GENERAL);
		
//		List<String> messages = ValidateUtil.validate(user);
		
		Map<String,Object> username_db = login_registDao.checkUserName(username);
		if(null != username_db){
			result.put("flag", "fail_usernameUsed");	
			return result;
		}
		Map<String,Object> phone_db = login_registDao.checkPhone(phone);
		if(null != phone_db){
			result.put("flag", "fail_phoneUsed");	
			return result;
		}
		
		Integer user_id = registDao.insertUser(user);
		if(null != user_id && ValidateUtil.Validate(phone, RegexType.REGEX_PHONE)){
			//注册用户
			registDao.insertUserDetail(user_id);
			//向用户收餐电话表添加一条待验证电话
			registDao.insertUserTelephone(user_id,phone);
			
			result.put("flag", "success");
			result.put("id", user_id);
		}else {
			result.put("flag", "fail_usernameUsed");
		}
		
		return result;
	}
}
