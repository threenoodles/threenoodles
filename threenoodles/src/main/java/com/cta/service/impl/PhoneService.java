/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.RegexType;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.PhoneDao;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ValidateUtil;
import com.cta.utils.ShortMessageUtils;
import com.cta.utils.Utils;

/**
 * @ClassName: PhoneService
 * @Description: 电话的业务
 * @author chenwenpeng
 * @date 2013-6-29 下午8:50:47
 */
@Service
public class PhoneService {
	/** The logger. */
    private static Logger logger = Logger.getLogger(PhoneService.class);
	@Autowired
	private PhoneDao phoneDao;
	/**
	 * @throws SQLException 
	* @Title: listUserPhone
	* @Description: 
	* @param @param session
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listUserPhone(HttpSession session) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		Integer userId = null == user? null : user.getId();
		if(null == userId){
			return null;
		}
		return phoneDao.listUserPhone(userId);
	}
	/**
	 * @throws SQLException 
	* @Title: checkPhone
	* @Description: 验证用户逇电话是否定过餐
	* @param @param session
	* @param @param phone
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean checkPhone(HttpSession session, String phone) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null != user && StringUtils.isNotBlank(phone) && null != phoneDao.getUserPhone(phone)){
			return true;
		}else {
			return false;
		}
	}
	/**
	 * @throws UnsupportedEncodingException 
	* @Title: sendPhoneCode
	* @Description: 发送验证码
	* @param @param session
	* @param @param phone
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean sendPhoneCode(HttpSession session, String phone) throws UnsupportedEncodingException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		Integer userId = null == user ? null : user.getId();
		if(ValidateUtil.Validate(phone, RegexType.REGEX_PHONE) && null != userId){
			String phoneCode = Utils.getRandomCode(4);
			String content = SystemGlobals.getPreference("sms.text.phoneCode","您的验证码为：")+phoneCode+"【酷客外卖网】";
			System.out.println(content);
			//验证码放session
			session.setAttribute(SessionParams.PHONECODE_FIRSTTIME, phoneCode);
			//发送验证码
			boolean flag = ShortMessageUtils.sendMessage(phone, content);
			if(false == flag){
				StringBuilder message = new StringBuilder();
				message.append("发送手机验证码失败--->要发送给的用户ID：");
				message.append(userId);
				message.append("；要发送给的用户的手机号：");
				message.append(phone);
				logger.error(message.toString());
			}
			return flag;
		}else {
			return false;
		}
	}
	
	/**
	 * @throws UnsupportedEncodingException 
	* @Title: sendPhoneCode_resetpassword
	* @Description: 
	* @param @param session
	* @param @param phone
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean sendPhoneCode_resetpassword(HttpSession session, String phone) throws UnsupportedEncodingException {
		
	    if(ValidateUtil.Validate(phone, RegexType.REGEX_PHONE)){
			String phoneCode = Utils.getRandomCode(4);
			String content = SystemGlobals.getPreference("sms.text.phoneCode.resetPassword", "您的验证码为：")+phoneCode+"【酷客外卖网】";
			//验证码放session
			session.setAttribute(SessionParams.PHONECODE_RESETPASSOWRD, phoneCode);
			System.out.println(content);
			//发送验证码
			boolean flag = ShortMessageUtils.sendMessage(phone, content);
			if(false == flag){
				StringBuilder message = new StringBuilder();
				message.append("发送手机验证码失败--->");
				message.append("；要发送给的用户的手机号：");
				message.append(phone);
				logger.error(message.toString());
			}
			return flag;
		}else {
			return false;
		}
	}
	/**
	 * @throws SQLException 
	* @Title: checkPhoneCode
	* @Description: 手机验证码,如果验证通过后手机号码存库
	* @param @param session
	* @param @param code
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean checkPhoneCode(HttpSession session, String code,String phone) throws SQLException {
		String code_db = (String) session.getAttribute(SessionParams.PHONECODE_FIRSTTIME);
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		Integer userId = null == user ? null : user.getId();
		if(StringUtils.isBlank(code) || StringUtils.isBlank(phone) || null == code_db || null == userId){
			return false;
		}
		if(code.equals(code_db)){
			//清除掉这个手机号对应的用户的无用状态
			phoneDao.clearUnCheckPhone(userId,phone);
			phoneDao.addUserPhone(userId,phone);
			return true;
		}
		return false;
	}
	/**
	* @Title: checkPhoneCode_resetPassword
	* @Description: 验证重置密码的验证码
	* @param @param session
	* @param @param code
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean checkPhoneCode_resetPassword(HttpSession session, String code) {
		String phoneCode = (String) session.getAttribute(SessionParams.PHONECODE_RESETPASSOWRD);
		if(StringUtils.isBlank(code) || StringUtils.isEmpty(phoneCode)){
			return false;
		}
		if(phoneCode.equals(code)){
			return true;
		}
		return false;
	}
}
