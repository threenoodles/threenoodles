/**
* @FileName: RestaurantService.java
* @Package com.cta.service
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-20 下午01:36:43
* @version V1.0
*/
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.CacheName;
import com.cta.constant.DataStatus;
import com.cta.constant.MenuType;
import com.cta.constant.OrderSubmitStatus;
import com.cta.constant.RestaurantStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.RestaurantDao;
import com.cta.dto.MenuDto;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BroadCastEntity;
import com.cta.entity.BuildingEntity;
import com.cta.entity.MenuEntity;
import com.cta.entity.RestaurantEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.cache.MemCacheManager;
import com.cta.utils.Utils;
/**
 * @ClassName: RestaurantService
 * @Description: 店铺模块的业务逻辑
 * @author chenwenpeng
 * @date 2013-5-20 下午01:36:43
 *
 */
@Service
public class RestaurantService {

	@Autowired
	private RestaurantDao restaurantDao;

	/**
	 * @throws SQLException 
	 * @param session 
	* @Title: listRestaurants
	* @Description: 列出某个区域下的所有店铺列表
	* @param @param buildingId
	* @param @return
	* @return List<RestaurantEntity>
	* @throws
	*/
	public List<RestaurantDto> listRestaurants(String alias, HttpSession session) throws SQLException {
		//验证
		if(StringUtils.isBlank(alias)){
			return null;
		}
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		BuildingEntity building = obtainBuilding(alias,memCacheManager);
		if(null == building){
			return null;
		}
		//building放session
		session.setAttribute(SessionParams.BUILDING, building);
		//读缓存restaurants
		List<RestaurantDto> restaurantList = memCacheManager.get(CacheName.CACHE_RESTAURANTS+alias, List.class);
		System.out.println((null !=restaurantList && restaurantList.size()>0) ? "缓存------>店铺列表" : "重新加载店铺列表不缓存");
		if(null == restaurantList || restaurantList.size()<=0){
			restaurantList = restaurantDao.listRestaurants(building.getId());
			memCacheManager.add(CacheName.CACHE_RESTAURANTS+alias, restaurantList, List.class);
		}
		
		return restaurantList;
	}

	/**
	 * @param session 
	 * @throws SQLException 
	* @Title: obtainBuilding
	* @Description: 获取buidling，并操作building的缓存
	* @param @param alia
	* @param @param memCacheManager
	* @param @return 
	* @return BuildingEntity 
	* @throws 
	*/ 
	private BuildingEntity obtainBuilding(String alias, MemCacheManager memCacheManager) throws SQLException {
		BuildingEntity building = memCacheManager.get(CacheName.CACHE_BUILDING+alias,BuildingEntity.class);
		if(null == building){
			building = restaurantDao.getBuilding(alias.trim());
			memCacheManager.add(CacheName.CACHE_BUILDING+alias, building, BuildingEntity.class);
		}
		return building;
	}

	/**
	 * @throws SQLException 
	* @Title: listRestCategory
	* @Description: 根据楼宇的id 获取楼宇对应的店铺的目录列表
	* @param @param buildingId
	* @param @return 
	* @return List<Map<String,String>> 
	* @throws 
	*/ 
	public List<Map<String, String>> listRestCategory(HttpSession session) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		if(null == building){
			return null;
		}
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		List<Map<String,String>> cateList = memCacheManager.get(CacheName.CACHE_RESTAURANTS_CATRGORY+building.getAlias(), List.class);
		//缓存中没有
		if(null == cateList){
			cateList = new LinkedList<Map<String,String>>();
			//获取店铺列表
			List<RestaurantDto> restaurantList = listRestaurants(building.getAlias(),session);
			if(null != restaurantList && restaurantList.size()>0){
				for(RestaurantDto restaurant : restaurantList){
					Map<String,String> restCate = new HashMap<String,String>();
					restCate.put("id", restaurant.getId()+"");
					List<Integer> cateIdList = restaurantDao.getCategoryList(restaurant.getId());
					String cates = cateIdList.toString();
					restCate.put("cateList", cates.substring(1,cates.length()-1));
					cateList.add(restCate);
				}
			}
			memCacheManager.add(CacheName.CACHE_RESTAURANTS_CATRGORY+building.getAlias(), cateList, List.class);
		}
		return cateList;
	}

	/**
	 * @throws SQLException 
	* @Title: getRestDetail
	* @Description: 店铺详情 
	* @param @param restId
	* @param @param session
	* @param @return 
	* @return RestaurantEntity 
	* @throws 
	*/ 
	public RestaurantDto getRestDetail(String alias, HttpSession session) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		if(StringUtils.isBlank(alias) || null == building){
			return null;
		}
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		RestaurantDto restaurant = memCacheManager.get(CacheName.CACHE_RESTAURANT+alias, RestaurantDto.class);
		if(null == restaurant){
			restaurant = restaurantDao.getRestaurantDetail(alias);
			//如果alias 对应的店铺不存在就返回null
			if(null == restaurant){
				return null;
			}
			memCacheManager.add(CacheName.CACHE_RESTAURANT+alias, restaurant, RestaurantDto.class);
		}
		if(null != restaurant){
			Double taste = restaurant.getTaste();
			Double speed = restaurant.getSpeed();
			Double attitude = restaurant.getAttitude();
			Integer score_avg = (int) Utils.divide(taste+speed+attitude,3,0);
			restaurant.setScore_avg(score_avg);
			//根据楼宇id 获取店铺配送到这个楼宇的送餐费
			Map<String,Object> carriageMap = restaurantDao.getCarriage(building.getId(),restaurant.getId());
			if(null != carriageMap){
				restaurant.setCarriage((Double)carriageMap.get("carriage"));
			}
		}
		return restaurant;
	}

	/**
	 * @throws SQLException 
	 * @param session 
	* @Title: getMenuList
	* @Description: 根据店铺的alias 获取店铺的订单信息
	* @param @param alias
	* @param @return 
	* @return List<MenuEntity> 
	* @throws 
	*/ 
	@SuppressWarnings("unchecked")
	public Map<String,Map<String,Object>> getMenuList(RestaurantDto restaurant) throws SQLException {
		if(null == restaurant){
			return null;
		}
		String restAlias = restaurant.getAlias();
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		Map<String,Map<String,Object>> cates = memCacheManager.get(CacheName.CACHE_RESTAURANT_MENULIST+restAlias, Map.class);
		List<MenuEntity> menus_recommend = memCacheManager.get(CacheName.CACHE_RESTAURANT_MENULIST_RECOMMEND+restAlias, List.class);
		if(null == cates || null == menus_recommend){
			menus_recommend = new LinkedList<MenuEntity>();
			cates = new HashMap<String,Map<String,Object>>();
			
			List<Map<String,Object>> _cates = restaurantDao.getMenuCateList(restaurant.getId());
			List<MenuEntity> menuList_all = restaurantDao.getMenuList(restaurant.getId());
			//迭代cate
			for(Map<String,Object> cate : _cates){
				cate.put("menu_list", new LinkedList<Map<String,Object>>());
				cates.put(cate.get("id")+"",cate);
			}
			//迭代menulist
			for(MenuEntity menu : menuList_all){
				Integer cateId = menu.getCate_id();
				List<MenuEntity> menuList = (List<MenuEntity>) (((Map<String,Object>)cates.get(cateId+"")).get("menu_list"));
				if(null == menuList){
					menuList = new LinkedList<MenuEntity>();
					((Map<String,Object>)cates.get(cateId+"")).put("menu_list", menuList);
				}
				menuList.add(menu);
				//找出店长推荐的菜单
				if(MenuType.RECOMMENDED == menu.getRecommend()){
					menus_recommend.add(menu);
				}
			}
			memCacheManager.add(CacheName.CACHE_RESTAURANT_MENULIST+restAlias, cates, Map.class);
			memCacheManager.add(CacheName.CACHE_RESTAURANT_MENULIST_RECOMMEND+restAlias, menus_recommend, List.class);
		}
		//推荐菜单放到restaurant，带回到controller
		restaurant.setMenus_recommend(menus_recommend);
		return cates;
	}

	/**
	 * @param carriage 
	 * @param menuList 
	 * @throws SQLException 
	* @Title: checkStatus
	* @Description: 检查店铺和用户的登录状态
	* @param @param restId
	* @param @param request
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String checkStatus(Integer restId, String menuList, Double totalPrice,Double carriage, HttpSession session) throws SQLException {
		
		if(null == restId || StringUtils.isBlank(menuList) || null == carriage || null == totalPrice){
			return OrderSubmitStatus.FAILED;
		}
		RestaurantDto rest = restaurantDao.getRestaurantDetail(restId);
		if(null == rest){
			return OrderSubmitStatus.FAILED;
		}else if(RestaurantStatus.CLOSED == rest.getStatus()){
			return OrderSubmitStatus.RESTCLOSED;
		}else if(RestaurantStatus.OPENED == rest.getStatus()){
			
			if(null == session.getAttribute(SessionParams.USER)){
				return OrderSubmitStatus.NOLOGIN;
			//用户登录了，同时店铺店铺营业中，同时订单达到店铺启送金额
			}else{
				return this.dealOrder(session, menuList, totalPrice, carriage, rest);
			}
		//订单可以预定
		}else if(RestaurantStatus.CLOSED_RESERVATION == rest.getStatus()){
			if(null == session.getAttribute(SessionParams.USER)){
				return OrderSubmitStatus.NOLOGIN;
			}else{ 
				String result = this.dealOrder(session, menuList, totalPrice, carriage, rest);
				//用户登录了，同时店铺店铺可以预定，同时订单达到店铺启送金额
				if(OrderSubmitStatus.SUCCEESS.equals(result)){
					return OrderSubmitStatus.SUCCEESS_RESERVATION;
				}else {
					return result;
				}
			}
		}else {
			return OrderSubmitStatus.FAILED;
		}
	}

	/**
	* @Title: dealOrder
	* @Description: 处理订单，返回处理结果
	* @param @param session
	* @param @param menuList
	* @param @param totalPrice
	* @param @param carriage
	* @param @param rest
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	private String dealOrder(HttpSession session,String menuList,double totalPrice,double carriage,RestaurantEntity rest) throws SQLException {
		StringBuilder menuIds = new StringBuilder();
		//id为key num为value
		Map<String,Integer> menus_site = this.getMenus_site(menuList,menuIds);
		List<MenuDto> menus_db = this.getMenus_db(menuIds.toString());
		Double totalPrice_db = this.getTotalPrice_db(menus_site,menus_db);
		//验证通过后放session
		if(null != totalPrice_db){
			if(totalPrice >= totalPrice_db && (totalPrice+carriage)>=rest.getDeli_price()){
				session.setAttribute(SessionParams.RESTAURANT, rest);
				session.setAttribute(SessionParams.ORDER_MENULIST, menus_db);
				session.setAttribute(SessionParams.ORDER_TOTALCOUNT, totalPrice);
				session.setAttribute(SessionParams.ORDER_CARRIAGE, carriage);
				return OrderSubmitStatus.SUCCEESS;
			}else {
				//未达到启送金额
				return OrderSubmitStatus.LESSTHANDELIVER;
			}
		}else {
			return OrderSubmitStatus.FAILED;
		}
	}

	/**
	* @Title: getTotalCount
	* @Description: 获取menu的id
	* @param @param menuList
	* @param @param carriage
	* @param @return 
	* @return Double 
	* @throws 
	*/ 
	private Map<String,Integer> getMenus_site(String menuList,StringBuilder ids){
		Map<String,Integer> menus = new HashMap<String,Integer>();
		try{
			String[] menuItems = menuList.split(",");
			for(String menuItem : menuItems){
				String[] menu = menuItem.split(";");
				String id = menu[0].split(":")[1].trim();
				Integer num = Integer.parseInt(menu[2].split(":")[1].trim());
				
				ids.append(id+",");
				menus.put(id, num);
				ids.append(id+",");
			}
			if(ids.length() > 0){
				ids.deleteCharAt(ids.length()-1);
			}
			return menus;
		}catch (Exception e) {
			return null;
		}
	}

	/**
	* @Title: getTotalCount_db
	* @Description: 获取数据库中存放的总价格
	* @param @param menuId
	* @param @return
	* @param @throws SQLException 
	* @return Double 
	* @throws 
	*/ 
	private List<MenuDto> getMenus_db(String menuIds) throws SQLException{
		if(StringUtils.isEmpty(menuIds)){
			return null;
		}
		return restaurantDao.getMenuList(menuIds.toString());
	}
	
	/**
	 * @param menus_site 
	* @Title: validateMenuList
	* @Description: 获取后台菜单的总价格
	* @param @param menus_site
	* @param @param menus_db
	* @param @return
	* @param @throws SQLException 
	* @return boolean 
	* @throws 
	*/ 
	private Double getTotalPrice_db(Map<String, Integer> menus_site, List<MenuDto> menus_db){
		
		try{
			Double totalPrice = 0D;
			if( null == menus_db || 0 >= menus_db.size()){
				return null;
			}
			for(int i = 0;i<menus_db.size();i++){
				MenuDto menu = menus_db.get(i);
				double price = menu.getPrice();
				int num = menus_site.get(menu.getId()+"");
				totalPrice += price * num;
				menu.setNum(num);
			}
			return totalPrice;
		}catch (Exception e) {
			return null;
		}
	}

	/**
	 * @throws SQLException 
	* @Title: addFavourite
	* @Description: 
	* @param @param restId
	* @param @param session
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String addFavourite(Integer restId, HttpSession session) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null == restId || null == building || null == building.getId() || null == user || null == user.getId()){
			return DataStatus.FAILED;
		}
		//清除用户的这个店铺在这个区域下面的信息
		restaurantDao.clearFavourite(restId,building.getId(),user.getId());
		//添加
		restaurantDao.addFavourite(restId,building.getId(),user.getId());
		return DataStatus.SUCCESS;
	}

	/**
	* @Title: listLatestBroadcasts
	* @Description: 
	* @param @param session
	* @param @return
	* @param @throws SQLException 
	* @return List<BroadCastEntity> 
	* @throws 
	*/ 
	public List<BroadCastEntity> listLatestBroadcasts(HttpSession session) throws SQLException {MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		List<BroadCastEntity> broadCasts = null;
		
		if(null  != building && null !=  building.getId()){
			//缓存中加载这个楼宇的新闻列表
			broadCasts = memCacheManager.get(CacheName.CACHE_BUILDING_BROADCAST+building.getId(), List.class);
		}
		if(null == broadCasts && null  != building && null !=  building.getId()){
			broadCasts = restaurantDao.listLatestBroadcasts(building.getId());
			if(null == broadCasts || broadCasts.size() == 0){
				//本区域没有轮播，那么加载默认的轮播
				broadCasts = restaurantDao.listDefaultBroadcasts();
			}
			//缓存中加载这个楼宇的新闻列表
			memCacheManager.add(CacheName.CACHE_BUILDING_BROADCAST+building.getId(),broadCasts,List.class);
		}
		return broadCasts;
	} 
}
