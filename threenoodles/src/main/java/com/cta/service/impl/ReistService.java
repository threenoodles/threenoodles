/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.constant.RegexType;
import com.cta.controller.ImageCodeController;
import com.cta.dao.impl.RegistDao;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ValidateUtil;

/**
 * @ClassName: ReistService
 * @Description: 注册
 * @author chenwenpeng
 * @date 2013-7-21 下午3:02:55
 */
@Service
public class ReistService {

	@Autowired
	private RegistDao registDao;
	/**
	 * @throws SQLException 
	* @Title: checkUsername
	* @Description: 验证用户名是否重复
	* @param @param username
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public boolean checkUsername(String username) throws SQLException {
		if(StringUtils.isNotBlank(username)){
			Map<String,Object> user = registDao.checkUsername(username);
			if(null == user){
				return true;
			}
		}
		return false;
	}

	/**
	 * @throws SQLException 
	* @Title: checkPhone
	* @Description: 
	* @param @param phone
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean checkPhone(String phone) throws SQLException {
		if(StringUtils.isNotBlank(phone)){
			Map<String,Object> user = registDao.checkPhone(phone);
			if(null == user){
				return true;
			}
		}
		return false;
	}

	/**
	 * @param request 
	* @Title: checkImagecode
	* @Description: 
	* @param @param imagecode
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public boolean checkImagecode(String imagecode, HttpServletRequest request) {
		if(StringUtils.isNotBlank(imagecode) && imagecode.equals(request.getSession().getAttribute(ImageCodeController.RANDOM_NUMBER_TITLE))){
			return true;
		}
		return false;
	}

	/**
	 * @throws SQLException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @Title: regist
	* @Description: 注册
	* @param @param username
	* @param @param password
	* @param @param qq
	* @param @param request
	* @param @return 
	* @return List<String> 
	* @throws 
	*/ 
	public List<String> addUser(String username, String password, String phone,
			HttpServletRequest request) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, SQLException {
		
		UserEntity user = new UserEntity();
		user.setNick_name(username);
		user.setPassword(password);
		user.setTelephone(phone);
		user.setReal_name("none");
		user.setReg_time(new Date());
		user.setOrder_count(0);
		user.setUn_comment(0);
		user.setGold(SystemGlobals.getIntPreference("default.score.regist", 30));
		user.setEnabled(DataStatus.ENABLED_GENERAL);
		List<String> messages = ValidateUtil.validate(user);
		if(messages.size()>0){
			return messages;
		}
		if(!this.checkUsername(username)){
			messages.add("注册失败，用户名已经存在!");
			return messages;
		}
		Integer user_id = registDao.insertUser(user);
		if(null != user_id && ValidateUtil.Validate(phone, RegexType.REGEX_PHONE)){
			//注册用户
			registDao.insertUserDetail(user_id);
			//向用户收餐电话表添加一条待验证电话
			registDao.insertUserTelephone(user_id,phone);
		}else {
			messages.add("注册失败，请您按照提示操作！");
			return messages;
		}
		return null;
	}
}
