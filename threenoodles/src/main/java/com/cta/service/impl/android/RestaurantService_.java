/**
 *
 */
package com.cta.service.impl.android;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.dao.impl.android.RestaurantDao_;

/**
 * @author Administrator
 *
 */
@Service
public class RestaurantService_ {

	@Autowired
	private RestaurantDao_ restaurantDao;
	/**
	 * @param buildingId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getRestaurants(Integer buildingId) throws SQLException {

		if(null == buildingId){
			return new ArrayList<Map<String,Object>>();
		}
		List<Map<String, Object>> restaurants = restaurantDao.getRestaurants(buildingId);
		for(Map<String, Object> restaurant : restaurants){
			try{
				if(null != restaurant.get("score")){
					double score_d = (Double) restaurant.get("score");
					restaurant.put("score", (int)score_d);
				}else {
					restaurant.put("score", 3);
				}
			}catch (Exception e) {
				restaurant.put("score", 3);
			}
		}
		return restaurants;
	}
	/**
	 * @param restId
	 * @param buildingId
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> getRestInfo(Integer restId, Integer buildingId) throws SQLException {

		if(null == restId || null == buildingId){
			return new HashMap<String,Object>();
		}
		Map<String,Object> restInfoMap = this.getRestaurntInfo(restId,buildingId);
		if(null == restInfoMap){
			return new HashMap<String,Object>();
		}
		//所有的店铺菜单大类
		List<Map<String,Object>> categoryList = new LinkedList<Map<String,Object>>();
		HashMap<String,Object> cate_recommendMap = new HashMap<String,Object>();
		cate_recommendMap.put("id", DataStatus.NONE_INT);
		cate_recommendMap.put("name", "店长推荐");
		List<Map<String,Object>> menuList_recommend = restaurantDao.getRecommendMenuList(restId);
		cate_recommendMap.put("menuList", menuList_recommend);
		//为categoryList填充数据
		categoryList.add(cate_recommendMap);
		List<Map<String,Object>> categoryList_ = restaurantDao.getCategoryList(restId);
		categoryList.addAll(categoryList_);
		restInfoMap.put("categoryList", categoryList);

		return restInfoMap;
	}
	/**
	 * @param restId
	 * @param buildingId
	 * @return
	 * @throws SQLException
	 */
	private Map<String, Object> getRestaurntInfo(Integer restId,
			Integer buildingId) throws SQLException {
		if(null == restId || null == buildingId){
			return new HashMap<String,Object>();
		}
		Map<String,Object> restInfo = restaurantDao.getRestInfo(restId);
		if(null != restInfo){
			Map<String,Object> carriage = restaurantDao.getRestCarriage(restId,buildingId);
			if(null != carriage){
				restInfo.put("carriage", carriage);
			}
		}
		return restInfo;
	}
	/**
	 * @throws SQLException 
	* @Title: getCategoyr_menuList
	* @Description: 获取某个菜单大类下面的菜单信息
	* @param @param restId
	* @param @param cateId
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public List<Map<String, Object>> getCategoyr_menuList(Integer restId,
			Integer cateId) throws SQLException {
		if(null == restId || null == cateId){
			return new ArrayList<Map<String,Object>>();
		}
		
		return restaurantDao.getCategoyr_menuList(restId,cateId);
	}
}
