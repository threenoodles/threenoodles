/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.CacheName;
import com.cta.constant.DataStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.NewsDao;
import com.cta.entity.BuildingEntity;
import com.cta.entity.NewsEntity;
import com.cta.platform.cache.MemCacheManager;

/**
 * @ClassName: NewsService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午2:53:41
 */
@Service
public class NewsService {
	@Autowired
	private NewsDao newsDao;
	
	/**
	 * @param session 
	 * @throws SQLException 
	* @Title: listNews
	* @Description: 列出前6条新闻
	* @param @return 
	* @return List<NewsEntity> 
	* @throws 
	*/ 
	public List<NewsEntity> listLatestNews(HttpSession session) throws SQLException {
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		List<NewsEntity> newsList = null;
		
		if(null  != building && null !=  building.getId()){
			//缓存中加载这个楼宇的新闻列表
			newsList = memCacheManager.get(CacheName.CACHE_BUILDING_LATEST_NEWSLIST+building.getId(), List.class);
		}
		
		if(null == newsList && null  != building && null !=  building.getId()){
			newsList = newsDao.listLatestNews(building.getId());
			if(null == newsList || newsList.size() == 0){
				//本区域没有新闻难么加载默认新闻
				newsList = newsDao.listDefaultNews();
			}
			//缓存中加载这个楼宇的新闻列表
			memCacheManager.add(CacheName.CACHE_BUILDING_LATEST_NEWSLIST+building.getId(),newsList,List.class);
		}
		return newsList;
	}

	/**
	 * @throws SQLException 
	* @Title: listNews
	* @Description: 
	* @param @param session
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listNewsTile(HttpSession session) throws SQLException {
		MemCacheManager memCacheManager = MemCacheManager.getMemCacheManager(MemCacheManager.SITE_CACHEMANAGER);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		List<Map<String,Object>> newsList = null;
		if(null  != building && null !=  building.getId()){
			//缓存中加载这个楼宇的新闻列表
			newsList = memCacheManager.get(CacheName.CACHE_BUILDING_NEWSLIST+building.getId(), List.class);
		}
		
		if(null == newsList){
			List<Integer> params = new LinkedList<Integer>();
			String sql = "select id,title,type from mf_news where status = ? order by send_time desc";
			params.add(DataStatus.ENABLED_GENERAL);
			
			if(null  != building && null !=  building.getId()){
				sql = "select id,title,type from mf_news where status = ? and building_id = ? order by send_time desc";
				params.add(building.getId());
			}
			newsList = newsDao.listNewsTile(sql,params.toArray());
			if(null  != building && null !=  building.getId()){
				//缓存中加载这个楼宇的新闻列表
				memCacheManager.add(CacheName.CACHE_BUILDING_NEWSLIST+building.getId(),newsList,List.class);
			}
		}
		
		return newsList;
	}

	/**
	 * @throws SQLException 
	* @Title: listNewsDetail
	* @Description: 
	* @param @param news_id
	* @param @return 
	* @return NewsService 
	* @throws 
	*/ 
	public NewsEntity listNewsDetail(Integer news_id) throws SQLException {
		if(null == news_id){
			return null;
		}
		return newsDao.listNewsDetail(news_id);
	}
}
