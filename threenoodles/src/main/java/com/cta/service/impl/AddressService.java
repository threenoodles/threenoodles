/**
* @FileName: AddressService.java
* @Package com.cta.service
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-18 下午12:56:19
* @version V1.0
*/
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.SessionParams;
import com.cta.dao.impl.AddressDao;
import com.cta.entity.BuildingEntity;
import com.cta.entity.UserEntity;

/**
 * @ClassName: AddressService
 * @Description: 地址操作
 * @author chenwenpeng
 * @date 2013-5-18 下午12:56:19
 *
 */
@Service
public class AddressService {

	@Autowired
	private AddressDao addressDao;
	/**
	 * @throws SQLException 
	* @Title: listUserAddress
	* @Description: 
	* @param @param session
	* @param @return 
	* @return List<String> 
	* @throws 
	*/ 
	public List<Map<String,Object>> listUserAddress(HttpSession session) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		Integer userId = null == user ? null : user.getId();
		Integer buildingId = null == building ? null : building.getId();
		if(null == userId || null == buildingId){
			return null;
		}
		return addressDao.listUserAddress(userId,buildingId);
	}
}
