/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.constant.MenuType;
import com.cta.constant.OrderStatus;
import com.cta.constant.OrderSubmitStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.CommentDao;
import com.cta.dao.impl.OrderDao;
import com.cta.dto.CommentDto;
import com.cta.dto.Reply_CommentDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.CommentEntity;
import com.cta.entity.Reply_CommentEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.util.ListPager;
import com.cta.platform.util.ValidateUtil;
import com.cta.utils.Utils;

/**
 * @ClassName: CommentService
 * @Description: 评论的业务
 * @author chenwenpeng
 * @date 2013-6-11 下午3:03:44
 */
@Service
public class CommentService {
	@Autowired
	private CommentDao commentDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private LoginService LoginService;
	
	/**
	 * @throws SQLException 
	 * @param restId 
	* @Title: listRestComments
	* @Description: 
	* @param @param pager 
	* @return void 
	* @throws 
	*/ 
	public void listRestComments(ListPager pager, Integer restId) throws SQLException {
		if(null == restId){
			pager.setPageData(new LinkedList<CommentDto>());
			return;
		}
		List<CommentDto> commentList = commentDao.listRestComments(pager,restId);
		for(CommentDto comment : commentList){
			Integer attitude = (null == comment.getAttitude()?4:comment.getAttitude());
			Integer speed = (null == comment.getSpeed()?4:comment.getSpeed());
			Integer taste = (null == comment.getTaste()?4:comment.getTaste());
			Integer score_avg = (int) Utils.divide(attitude+speed+taste, 3, 0);
			
			Integer commetId = comment.getId();
			List<Reply_CommentDto> replyList = commentDao.listReplyByCommentId(commetId);
			
			comment.setReplyList(replyList);
			comment.setScore_avg(score_avg);
		}
		pager.setPageData(commentList);
	}
	
	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	* @Title: submitComment
	* @Description: 
	* @param @param comment
	* @param @param session
	* @param @return 
	* @return boolean 
	* @throws 
	*/ 
	public List<String> addComment(CommentEntity comment, HttpSession session) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		Integer rest_id=(null == comment.getOrder_id())?null:commentDao.getRestIdByOrderId(comment.getOrder_id());
		comment.setRest_id(rest_id);
		comment.setUser_id(null == user? null : user.getId());
		List<String> messages = ValidateUtil.validate(comment);
		if(messages.size()>0){
			return messages;
		}
		//添加评论
		commentDao.addComment(comment);
		//更新订单状态
		orderDao.updateOrderStatus(comment.getOrder_id(), OrderStatus.COMMENT);
		return null;
	}

	/**
	 * @throws SQLException 
	* @Title: listRestComments
	* @Description: 列出楼宇的评论信息
	* @param @param pager
	* @param @param session 
	* @return void 
	* @throws 
	*/ 
	public List<CommentDto> listBuildingComments(ListPager pager, HttpSession session) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		if(null != building && null != building.getId()){
			List<CommentDto> comments = commentDao.listBuildingComment(pager,building.getId());
			for(CommentDto comment : comments){
				Integer order_id = comment.getOrder_id();
				//获取评论对应的菜单详情
				List<Map<String,Object>> menus = orderDao.listOrderMenus(order_id);
				comment.setMenus(menus);
				//获取评论的回复
				List<Map<String,Object>> replies = commentDao.ListCommentReply_s(comment.getId());
				comment.setReplyList_s(replies);
			}
			return comments;
		}
		return null;
	}

	/**
	 * @throws SQLException 
	* @Title: getMenuPic
	* @Description: 
	* @param @param menu_id
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String getMenuPic(Integer menu_id) throws SQLException {
		String pic_name = "none.png";
		if(null == menu_id){
			return pic_name;
		}
		Map<String,Object> menu = commentDao.getMenuPic(menu_id);
		if(null != menu && null != menu.get("recommend")){
			Integer recommend = (Integer)menu.get("recommend");
			if(MenuType.RECOMMENDED == recommend){
				pic_name = (String)menu.get("pic");
			}
		}
		
		return pic_name;
	}
	
	/**
	 * @throws UnsupportedEncodingException 
	 * @param password 
	 * @param username 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	* @Title: addReply
	* @Description: 添加回复信息
	* @param @param reply
	* @param @param session
	* @param @return 
	* @return List<String> 
	* @throws 
	*/ 
	public String addReply(Reply_CommentEntity reply, String username, String password, HttpSession session) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException {
		UserEntity user = (UserEntity)session.getAttribute(SessionParams.USER);
		if(null == user || null == user.getId() || null == reply.getComment_id()){
			//没有登录验证登陆信息
			if(StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)){
				String result = LoginService.login(username, password, session);
				if(OrderSubmitStatus.SUCCEESS.equals(result)){
					user = (UserEntity)session.getAttribute(SessionParams.USER);
				}else {
					return DataStatus.NO_LOGIN;
				}
			}else {
				return DataStatus.NO_LOGIN;
			}
		}
		
		
		Integer comment_id = reply.getComment_id();
		CommentEntity comment = commentDao.getComment_s(comment_id);
		if(null != comment){
			reply.setCommenter_id(comment.getUser_id());
			reply.setReplier_id(user.getId());
			List<String> messages = ValidateUtil.validate(reply);
			if(messages.size()<=0){
				commentDao.addReply(reply);
				return DataStatus.SUCCESS;
			}
		}
		return DataStatus.FAILED;
	}
}
