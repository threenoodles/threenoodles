/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.OrderDeliverType;
import com.cta.constant.OrderMessageSendType;
import com.cta.constant.OrderStatus;
import com.cta.constant.OrderSubmitStatus;
import com.cta.constant.OrderType;
import com.cta.constant.SessionParams;
import com.cta.constant.TaskType;
import com.cta.dao.impl.OrderDao;
import com.cta.dto.MenuDto;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.DelivererEntity;
import com.cta.entity.OrderEntity;
import com.cta.entity.RestaurantEntity;
import com.cta.entity.TaskEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ValidateUtil;
import com.cta.utils.DateUtils;
import com.cta.utils.ShortMessageUtils;

/**
 * @ClassName: OrderService
 * @Description: 订单的service
 * @author chenwenpeng
 * @date 2013-7-3 下午10:08:20
 */
@Service
public class OrderService {
	@Autowired
	private OrderDao orderDao;

	/**
	 * @throws ParseException 
	 * @throws UnsupportedEncodingException 
	 * @throws SQLException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @param session 
	* @Title: submitOrder
	* @Description: 提交订单
	* @param @param address
	* @param @param phone
	* @param @param remarks
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String addOrder(HttpSession session, String address, String phone, String remarks,Integer orderType,String reserveTime_str) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, SQLException, UnsupportedEncodingException, ParseException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		RestaurantDto restaurant = (RestaurantDto) session.getAttribute(SessionParams.RESTAURANT);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		List<MenuDto> menus = (List<MenuDto>) session.getAttribute(SessionParams.ORDER_MENULIST);
		Double totalCount = (Double) session.getAttribute(SessionParams.ORDER_TOTALCOUNT);
		Double carriage = (Double) session.getAttribute(SessionParams.ORDER_CARRIAGE);
		if(null == user || null == restaurant || null == building || null == menus || null == totalCount || null == carriage || null == orderType){
			//请求失败，可能是用户的第二次提交,或者是用户直接输入地址跳转过来的
			return OrderSubmitStatus.FAILED;
		}
		//要发送的订单内容(只有菜单)
		StringBuilder menu_message = new StringBuilder();
		//要发送的订单内容(菜单和收餐用户信息)
		StringBuilder order_message = new StringBuilder();
		
		OrderEntity order = new OrderEntity();
		order.setAddress(building.getName() + address);
		order.setBuilding_id(building.getId());
		order.setCarriage(carriage);
		order.setOrdered_time(new Date());
		order.setPhone(phone);
		order.setRemark(remarks);
		order.setRest_id(restaurant.getId());
		order.setStatus(OrderStatus.COOKING);
		order.setTotalPrice(totalCount);
		order.setType(orderType);
		order.setUser_id(user.getId());
		
		List<String> messages = ValidateUtil.validate(order);
		if(messages.size()>0){
			return OrderSubmitStatus.FAILED;
		}
		Integer orderId = orderDao.addOrder(order);
		//订单插入失败
		if(null == orderId){
			return OrderSubmitStatus.FAILED;
		}
		order.setId(orderId);
		
		//如果用户是预定
		if(OrderType.ORDER_RESERVE == orderType && StringUtils.isNotEmpty(reserveTime_str) && OrderSubmitStatus.SUCCEESS.equals(this.checkReserveTime(reserveTime_str, restaurant))){
			orderDao.updateOrderStatus(orderId, OrderStatus.RESERVE);
			//保存订单的菜单
			for(MenuDto menu : menus){
	 			orderDao.addOrderMenu(order.getId(),menu);
			}
			//添加订单订单预定的系统任务
			TaskEntity task = new TaskEntity();
			task.setTarget_id(orderId);
			task.setTime(DateUtils.parseDateTimeByTime(reserveTime_str));
			task.setType(TaskType.RESERVEORDER);
			orderDao.addTask(task);
			
			session.setAttribute(SessionParams.ORDER_RESERVETIME, reserveTime_str);
		}else {
			this.generateMessage(menu_message,order_message,menus,order,restaurant);
			boolean flag = this.sendOrderMessage(order,restaurant,building,menu_message,order_message);
			//更新订单状态
			if(false == flag){
				//失败
				orderDao.updateOrderStatus(order.getId(),OrderStatus.FAILD_ADVICE);
			}
		}
		//更新订单对应的店铺，个人，楼宇的信息
		Double score = null == order.getTotalPrice() ? 0 : order.getTotalPrice()*5;
		Integer user_id = user.getId();
		Integer rest_id = restaurant.getId();
		Integer building_id = building.getId();
		//更新订单对应的信息
		orderDao.updateUserMsg(user_id,score);
		orderDao.updateUserAddressMsg(user_id,building.getId(),address);
		orderDao.updateRestMsg(rest_id);
		orderDao.updateBuildingMsg(building_id);
		
		//处理session
		session.setAttribute(SessionParams.ORDER_TOTALSCORE, score);
		session.setAttribute(SessionParams.ORDER_REST_NAME, restaurant.getName());
		session.removeAttribute(SessionParams.RESTAURANT);
		session.removeAttribute(SessionParams.ORDER_MENULIST);
		session.removeAttribute(SessionParams.ORDER_TOTALCOUNT);
		session.removeAttribute(SessionParams.ORDER_CARRIAGE);
		session.removeAttribute(SessionParams.USER_DETAIL);
		return OrderSubmitStatus.SUCCEESS;
	}

	/**
	* @Title: sendOrderMessage
	* @Description: 发送订单
	* @param @param order
	* @param @param restaurant
	* @param @param building
	* @param @param menu_message
	* @param @param order_message
	* @param @throws UnsupportedEncodingException
	* @param @throws SQLException 
	* @return void 
	* @throws 
	*/ 
	public boolean sendOrderMessage(OrderEntity order,RestaurantEntity restaurant,BuildingEntity building,
			StringBuilder menu_message,StringBuilder order_message) throws UnsupportedEncodingException, SQLException {
		Integer deli_type = restaurant.getDeli_type();
		//由店铺配送
		if(OrderDeliverType.ORDER_DELIVER_RESTAURANT == deli_type){
			//短信发送全部信息
			return this.sendToRestaurant(restaurant,order_message);
		//由我们配送
		}else {
			//短信发送店铺简要信息
			boolean flag = this.sendToRestaurant(restaurant,menu_message);
			//订单发送失败
			if(!flag){
				return false;
			}
			//根据楼宇id 和 店铺 id ，找出店铺的配送员确定通知方式然后发送订单
			DelivererEntity deliverer = orderDao.getRestDeliverer(building.getId(),restaurant.getId());
			if(null != deliverer && deliverer.getSend_type() == OrderMessageSendType.SHORT_MESSAGE){
				String phone_deliverer = deliverer.getTelephone();
				boolean flag_deliver = ShortMessageUtils.sendMessage(phone_deliverer, order_message.toString());
				//订单发送失败
				if(!flag_deliver){
					return false;
				}
			
			//未设置配送员，标记订单发送失败
			}else if(null == deliverer || null == deliverer.getSend_type()) {
				//如果没有找到送餐员查找默认的收餐电话
				 String phone_deliverer = SystemGlobals.getPreference("default.deliverer.telephone","15801377079");
				 boolean flag_deliver = ShortMessageUtils.sendMessage(phone_deliverer, order_message.toString());
				//订单发送失败
				if(!flag_deliver){
					return false;
				}
			}
			
			return true;
		}
	}

	/**
	* @Title: sendToRestaurant
	* @Description: 发送订单给店铺
	* @param @param restaurant
	* @param @param message
	* @param @return
	* @param @throws UnsupportedEncodingException 
	* @return boolean 
	* @throws 
	*/ 
	public boolean sendToRestaurant(RestaurantEntity restaurant,StringBuilder message) throws UnsupportedEncodingException {
		String phone_rest = restaurant.getTelephone();
			//短信发送
			if(null != restaurant.getSend_type() && OrderMessageSendType.SHORT_MESSAGE == restaurant.getSend_type()){
				if(StringUtils.isNotEmpty(phone_rest)){
					//完整信息发送给店铺
					return ShortMessageUtils.sendMessage(phone_rest, message.toString());
				}
			}
			return false;
	}

	/**
	* @Title: generateMessage
	* @Description: 生成订单和菜单信息
	* @param @param menu_message
	* @param @param order_message
	* @param @param menus
	* @param @param order
	* @param @param restaurant
	* @param @param building
	* @param @throws SQLException 
	* @return void 
	* @throws 
	*/ 
	public void generateMessage(StringBuilder menu_message,StringBuilder order_message,List<MenuDto> menus,OrderEntity order,RestaurantEntity restaurant) throws SQLException {
		menu_message.append("菜单:");
		int i = 1;
		//保存菜单
		for(MenuDto menu : menus){
			menu_message.append("["+i+"]");
			menu_message.append(menu.getName()); 
			menu_message.append("*"); 
			menu_message.append(menu.getNum()); 
			menu_message.append("="); 
			menu_message.append(menu.getPrice() * menu.getNum());
 			orderDao.addOrderMenu(order.getId(),menu);
			i++;
		}
		
		menu_message.append("|订单总计:").append("￥").append(order.getTotalPrice()).append("+").append(order.getCarriage());
		menu_message.append("|备注:");
		menu_message.append(order.getRemark());
		menu_message.append("|烹饪方:");
		menu_message.append(restaurant.getName());
		
		//由谁配送
		Integer deli_type = restaurant.getDeli_type();
		
		order_message.append(menu_message);
		order_message.append("|配送方:");
		order_message.append(OrderDeliverType.ORDER_DELIVER_OUR == deli_type ? SystemGlobals.getPreference("company.name") : restaurant.getName());
		order_message.append("|配送地址:");
		order_message.append(order.getAddress());
		order_message.append("|收餐电话：");
		order_message.append(order.getPhone());
		}

	/**
	 * @throws ParseException 
	* @Title: checkReserveTime
	* @Description: 
	* @param @param reserveTime
	* @param @param attribute
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String checkReserveTime(Date reserveTime, RestaurantDto restaurant) throws ParseException {
		if(null == reserveTime || null == restaurant){
			return "failed";
		}
		//判断用户选择的送餐时间是否已过
		if(DateUtils.compareDateTime(DateUtils.parseTime(DateUtils.formatTime(new Date())),reserveTime)){
			return "failed_past";
		}else {
			boolean flag_am = DateUtils.compareDateTime(reserveTime, restaurant.getOpen_time_am(), restaurant.getClose_time_am());
			boolean flag_pm = DateUtils.compareDateTime(reserveTime, restaurant.getOpen_time_pm(), restaurant.getClose_time_pm());
			if(flag_am || flag_pm){
				return "success";
			}else {
				return "failed";
			}
		}
	}
	/**
	 * @throws ParseException 
	 * @Title: checkReserveTime
	 * @Description: 
	 * @param @param reserveTime
	 * @param @param attribute
	 * @param @return 
	 * @return String 
	 * @throws 
	 */ 
	public String checkReserveTime(String reserveTime_, RestaurantDto restaurant) throws ParseException {
		if(StringUtils.isBlank(reserveTime_) || null == restaurant){
			return "failed";
		}
		return this.checkReserveTime(DateUtils.parseTime(reserveTime_), restaurant);
	}
}
