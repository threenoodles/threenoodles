/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.dao.impl.OrderDao;

/**
 * @ClassName: NoticeService
 * @Description: 公告的业务逻辑
 * @author chenwenpeng
 * @date 2013-5-26 下午4:11:23
 */
@Service
public class NoticeService {
	@Autowired
	private OrderDao orderDao;
	
	/**
	 * @throws SQLException 
	* @Title: listOrders
	* @Description: 列出最新的订单
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listOrders() throws SQLException {
		
		return orderDao.listLatestOrders();
	}
}
