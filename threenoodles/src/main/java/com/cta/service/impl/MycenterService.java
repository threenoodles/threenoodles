/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.constant.MycenterFlag;
import com.cta.constant.OrderStatus;
import com.cta.constant.RegexType;
import com.cta.constant.ReplyStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.MycenterDao;
import com.cta.dto.CommentDto;
import com.cta.dto.FeedbackDto;
import com.cta.dto.OrderDto;
import com.cta.entity.CommentEntity;
import com.cta.entity.FeedbackEntity;
import com.cta.entity.Reply_CommentEntity;
import com.cta.entity.Reply_FeedbackEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.util.ListPager;
import com.cta.platform.util.ValidateUtil;

/**
 * @ClassName: MycenterService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-7-23 下午10:54:43
 */
@Service
public class MycenterService {

	@Autowired
	private MycenterDao mycenterDao;
	@Autowired
	private LoginService loginService;

	/**
	 * @param pager 
	 * @throws SQLException 
	* @Title: listOrder
	* @Description: 转到个人中心
	* @param @param flag
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public List<OrderDto> listData(String flag,HttpSession session) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		checkUserDetail(user.getId(),session);
		//列出用户今天的所有订单
		if(MycenterFlag.TODAY.equals(flag)){
			List<OrderDto> orders = mycenterDao.listOrder_today(user.getId());
			for(OrderDto order : orders){
				Integer order_id = order.getId();
				List<Map<String,Object>> menus = mycenterDao.listOrderMenus(order_id);
				order.setMenus(menus);
				//订单已经评论
				if(OrderStatus.COMMENT == order.getStatus()){
					CommentEntity comment = mycenterDao.getOrderComment(order_id);
					order.setComment(comment);
				}
			}
			return orders;
		}else if(MycenterFlag.HISTORY.equals(flag)) {
			List<OrderDto> orders = mycenterDao.listOrder_history(user.getId());
			for(OrderDto order : orders){
				Integer order_id = order.getId();
				List<Map<String,Object>> menus = mycenterDao.listOrderMenus(order_id);
				order.setMenus(menus);
				//订单已经评论
				if(OrderStatus.COMMENT == order.getStatus()){
					CommentEntity comment = mycenterDao.getOrderComment(order_id);
					order.setComment(comment);
				}
			}
			return orders;
		}
		return null;
	}

	/**
	 * @throws SQLException 
	* @Title: checkUserDetail
	* @Description: 检查用户详情，如果没有就重新查询一遍，放session中
	* @param @param id
	* @param @param session 
	* @return void 
	* @throws 
	*/ 
	private void checkUserDetail(Integer user_id, HttpSession session) throws SQLException {
		
		if(null == session.getAttribute(SessionParams.USER_DETAIL)){
			Map<String,Object> user_detail = mycenterDao.getUserDetail(user_id);
			session.setAttribute(SessionParams.USER_DETAIL, user_detail);
		}
	}

	/**
	 * @throws SQLException 
	* @Title: listHistoryOrder
	* @Description: 列出我的历史订单
	* @param @param session
	* @param @param pager
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public List<OrderDto> listHistoryOrder(HttpSession session, ListPager pager) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		List<OrderDto> orders = mycenterDao.listOrder_history(user.getId(),pager);
		for(OrderDto order : orders){
			Integer order_id = order.getId();
			List<Map<String,Object>> menus = mycenterDao.listOrderMenus(order_id);
			order.setMenus(menus);
			//订单已经评论
			if(OrderStatus.COMMENT == order.getStatus()){
				CommentEntity comment = mycenterDao.getOrderComment(order_id);
				order.setComment(comment);
			}
		}
		return orders;
	}

	/**
	 * @throws SQLException 
	* @Title: listUnReadComments
	* @Description: 列出用户未查看的评论信息
	* @param @param id
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public List<CommentDto> listUnReadCommentReplies(Integer user_id) throws SQLException {
		if(null == user_id){
			return null;
		}
		//列出有未读回复的评论
		List<CommentDto> comments = mycenterDao.listComments(ReplyStatus.UNREAD,user_id);
		//同时把未读的回复信息修改为已读
		mycenterDao.modifyCommentReplyStatus(user_id,ReplyStatus.READ);
		for(CommentDto comment : comments){
			List<Map<String,Object>> replyList_s = mycenterDao.listReplies(comment.getId());
			comment.setReplyList_s(replyList_s);
		}
		return comments;
	}
	
	/**
	 * @throws SQLException 
	 * @param flag 
	* @Title: listComments
	* @Description: 列出用户的所有评论
	* @param @param session
	* @param @param pager
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public Object listComments(Integer user_id,ListPager pager) throws SQLException {
		if(null == user_id){
			return null;
		}
		//列出有未读回复的评论
		List<CommentDto> comments = mycenterDao.listComments(ReplyStatus.READ,user_id,pager);
		for(CommentDto comment : comments){
			List<Map<String,Object>> replyList_s = mycenterDao.listReplies(comment.getId());
			comment.setReplyList_s(replyList_s);
		}
		return comments;
	}
	/**
	 * @throws SQLException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @Title: submitComment
	* @Description: 提交评论
	* @param @param comment_id
	* @param @param content 
	* @return void 
	* @throws 
	*/ 
	public List<String> submitComment(Integer comment_id,Integer commenter_id, String content) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, SQLException {
		Reply_CommentEntity reply_comment = new Reply_CommentEntity();
		reply_comment.setComment_id(comment_id);//评论的id 
		reply_comment.setCommenter_id(commenter_id);//评论者的id
		reply_comment.setContent(content);
		reply_comment.setReplier_id(commenter_id);
		reply_comment.setReply_time(new Date());
		reply_comment.setStatus(ReplyStatus.UNREAD);
		
		List<String> messages = ValidateUtil.validate(reply_comment);
		if(messages.size()>0){
			return messages;
		}
		
		mycenterDao.submitComment(reply_comment);
		return null;
	}

	/**
	 * @throws SQLException 
	* @Title: listUnReadFeedbacks
	* @Description: 列出所有未读信息
	* @param @param id
	* @param @return 
	* @return List<Reply_FeedbackDto> 
	* @throws 
	*/ 
	public List<FeedbackDto> listUnReadFeedbacks(Integer user_id) throws SQLException {
		if(null == user_id){
			return null;
		}
		//列出未读的反馈信息
		List<FeedbackDto> feedbacks = mycenterDao.listUnReadFeedbacks(user_id);
		//同时把未读的回复信息修改为已读
		mycenterDao.modifyFeedbackReplyStatus(user_id,ReplyStatus.READ);
		for(FeedbackDto feedback : feedbacks){
			 Integer feedback_id = feedback.getId();
			 List<Map<String,Object>> replies = mycenterDao.listFeedbackReplies(feedback_id);
			 feedback.setReplyList_s(replies);
		}
		
		return feedbacks;
	}

	/**
	 * @throws SQLException 
	* @Title: listFeedbacks
	* @Description: 
	* @param @param id
	* @param @param pager
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public List<FeedbackDto> listFeedbacks(Integer user_id, ListPager pager) throws SQLException {
		if(null == user_id){
			return null;
		}
		List<FeedbackDto> feedbacks = mycenterDao.listFeedbacks(user_id,ReplyStatus.READ,pager);
		for(FeedbackDto feedback : feedbacks){
			 Integer feedback_id = feedback.getId();
			 List<Map<String,Object>> replies = mycenterDao.listFeedbackReplies(feedback_id);
			 feedback.setReplyList_s(replies);
		}
		return feedbacks;
	}

	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	* @Title: submitFeedback
	* @Description: 
	* @param @param submitFeedback
	* @param @param id
	* @param @param content
	* @param @return 
	* @return List<String> 
	* @throws 
	*/ 
	public List<String> submitFeedbackReply(Integer feedback_id, Integer user_id,String content) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Reply_FeedbackEntity reply_feedback = new Reply_FeedbackEntity();
		reply_feedback.setContent(content);
		reply_feedback.setFeedback_id(feedback_id);
		reply_feedback.setReplier_id(user_id);
		reply_feedback.setReply_time(new Date());
		reply_feedback.setResponder_id(user_id);
		reply_feedback.setStatus(ReplyStatus.UNREAD);
		
		List<String> messages = ValidateUtil.validate(reply_feedback);
		if(messages.size()>0){
			return messages;
		}
		
		mycenterDao.submitFeedbackReply(reply_feedback);
		return null;
	}

	/**
	 * @throws SQLException 
	* @Title: listMygifts
	* @Description: 
	* @param @param user_id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String,Object>> listMygifts(Integer user_id) throws SQLException {
		if(null == user_id){
			return null;
		}
		
		List<Map<String,Object>> gifts = mycenterDao.listMygifts(user_id);
		return gifts;
	}

	/**
	 * @throws SQLException 
	* @Title: resetPassword
	* @Description: 修改密码
	* @param @param password
	* @param @return 
	* @return Boolean 
	* @throws 
	*/ 
	public Boolean resetPassword(Integer user_id ,String password) throws SQLException {
		if(null == user_id || null == password || password.trim().length()<6 || password.trim().length()>20){
			return false;
		}
		mycenterDao.resetPassworde(user_id,password.trim());
		return true;
	}

	/**
	 * @throws SQLException 
	* @Title: queryUserInfo
	* @Description: 
	* @param @param username
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public Map<String, Object> queryUserInfo(String data) throws SQLException {
		if(StringUtils.isBlank(data)){
			return null;
		}
		Map<String,Object> user_info = null;
		//如果是电话
		if(ValidateUtil.Validate(data.trim(), RegexType.REGEX_PHONE)){
			user_info = mycenterDao.queryUserInfoByPhone(data);
		}else {
			user_info = mycenterDao.queryUserInfoByName(data);
		}
		return user_info;
	}

	/**
	 * @throws SQLException 
	* @Title: resetPassword
	* @Description: 
	* @param @param session
	* @param @param password
	* @param @param code
	* @param @return 
	* @return Boolean 
	* @throws 
	*/ 
	public Boolean resetPassword(HttpSession session, String password,String username,String code) throws SQLException {
		if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(code) && null != session.getAttribute(SessionParams.PHONECODE_RESETPASSOWRD) && StringUtils.isNotBlank(password) && password.length()>=6 && password.length()<=20){
			if(code.equals(session.getAttribute(SessionParams.PHONECODE_RESETPASSOWRD))){
				mycenterDao.resetPassword(username,password);
				return true;
			}
		}
		return false;
	}

	/**
	 * @throws SQLException 
	 * @throws UnsupportedEncodingException 
	* @Title: submitFeedback
	* @Description: 提交反馈信息
	* @param @param session
	* @param @param username
	* @param @param password
	* @param @param content
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String submitFeedback(HttpSession session, String username,String password, String content) throws UnsupportedEncodingException, SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null == user && StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)){
			String result = loginService.login(username, password, session);
			if(DataStatus.SUCCESS.equals(result)){
				this.submitFeedback(session,content);
				return DataStatus.SUCCESS;
			}
		}else if(null != user && null != user.getId()) {
			this.submitFeedback(session,content);
			return DataStatus.SUCCESS;
		}
		return DataStatus.NO_LOGIN;
	}

	/**
	 * @throws SQLException 
	* @Title: submitFeedback
	* @Description: 
	* @param @param session
	* @param @param content 
	* @return void 
	* @throws 
	*/ 
	private void submitFeedback(HttpSession session,String content) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null != user && null != user.getId()){
			FeedbackEntity feedback = new FeedbackEntity();
			feedback.setContent(content);
			feedback.setStatus(DataStatus.ENABLED_GENERAL);
			feedback.setTime(new Date());
			feedback.setUser_id(user.getId());
			feedback.setType(1);
			//保存反馈信息
			mycenterDao.submitFeedbackReply(feedback);
		}
	}
}
