/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl.android;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.cta.constant.OrderStatus;
import com.cta.constant.OrderSubmitStatus;
import com.cta.dao.impl.android.OrderDao_;
import com.cta.dto.MenuDto;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.OrderEntity;
import com.cta.entity.OrderMenuEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.util.ValidateUtil;
import com.cta.service.impl.OrderService;
import com.cta.utils.DateUtils;

/**
 * @ClassName: OrderService_
 * @Description:
 * @author chenwenpeng
 * @date 2013-9-1 下午2:08:03
 */
@Service
public class OrderService_ {

	@Autowired
	private OrderDao_ orderDao;
	@Autowired
	private OrderService orderService;

	/**
	 * @throws SQLException
	 * @Title: getOrder_today
	 * @Description:
	 * @param @param userId
	 * @param @return
	 * @return List<Map<String,Object>>
	 * @throws
	 */
	public List<Map<String, Object>> getOrder_today(Integer userId)
			throws SQLException {
		if (null == userId) {
			return new ArrayList<Map<String, Object>>();
		}
		List<Map<String, Object>> orders = orderDao.getOrder_today(userId);
		for (Map<String, Object> order : orders) {
			int order_id = (Integer) order.get("order_id");
			List<Map<String, Object>> menus = orderDao.listOrderMenus(order_id);
			order.put("menuList", menus);
		}
		return orders;
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws SQLException
	 * @param menuList
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @Title: saveOrder
	 * @Description:
	 * @param @param order
	 * @param @return
	 * @return String
	 * @throws
	 */
	public String saveOrder(OrderEntity order, String menuList)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException, SQLException, UnsupportedEncodingException {

		List<String> messages = ValidateUtil.validate(order);
		if (messages.size() > 0 || StringUtils.isBlank(menuList)) {
			return OrderSubmitStatus.FAILED;
		}
		Integer orderId = orderDao.addOrder(order);
		if (null == orderId) {
			return OrderSubmitStatus.FAILED;
		}
		UserEntity user = new UserEntity();
		user.setId(order.getUser_id());
		RestaurantDto restaurant = orderDao.getRestaurantDetail(order
				.getRest_id());
		BuildingEntity building = (BuildingEntity) orderDao
				.getBuildingDetail(order.getBuilding_id());
		List<MenuDto> menus = this.getMenus(menuList);
		Double totalCount = order.getTotalPrice();
		Double carriage = order.getCarriage();
		String address = order.getAddress();
		if (null == user || null == restaurant || null == building
				|| null == menus || null == totalCount || null == carriage) {
			// 请求失败，可能是用户的第二次提交,或者是用户直接输入地址跳转过来的
			return OrderSubmitStatus.FAILED;
		}
		order.setId(orderId);
		order.setAddress(building.getName() + address);
		// 要发送的订单内容(只有菜单)
		StringBuilder menu_message = new StringBuilder();
		// 要发送的订单内容(菜单和收餐用户信息)
		StringBuilder order_message = new StringBuilder();

		orderService.generateMessage(menu_message, order_message, menus, order,
				restaurant);
		boolean flag = orderService.sendOrderMessage(order, restaurant,
				building, menu_message, order_message);
		// 更新订单状态
		if (false == flag) {
			// 失败
			orderDao.updateOrderStatus(order.getId(), OrderStatus.FAILD_ADVICE);
		}

		// 更新订单对应的店铺，个人，楼宇的信息
		Double score = null == order.getTotalPrice() ? 0 : order
				.getTotalPrice() * 5;
		Integer user_id = user.getId();
		Integer rest_id = restaurant.getId();
		Integer building_id = building.getId();
		// 更新订单对应的信息
		orderDao.updateUserMsg(user_id, score);
		orderDao.updateUserAddressMsg(user_id, building.getId(), address);
		orderDao.updateRestMsg(rest_id);
		orderDao.updateBuildingMsg(building_id);

		return OrderSubmitStatus.PASS;
	}

	/**
	 * @Title: getMenus
	 * @Description:
	 * @param @param menuList
	 * @param @return
	 * @return List<MenuDto>
	 * @throws
	 */
	private List<MenuDto> getMenus(String menuList) {
		List<MenuDto> menus = new ArrayList<MenuDto>();
		String[] menus_ = menuList.split(",");
		for (String menu_ : menus_) {
			String[] item = menu_.split(";");
			MenuDto menu = new MenuDto();
			menu.setId(Integer.parseInt(item[0].substring(
					item[0].indexOf(":") + 1, item[0].length())));
			menu.setName(item[1].substring(item[1].indexOf(":") + 1,
					item[1].length()));
			menu.setNum(Integer.parseInt(item[2].substring(
					item[2].indexOf(":") + 1, item[2].length())));
			menu.setPrice(Double.parseDouble(item[3].substring(
					item[3].indexOf(":") + 1, item[3].length())));

			menus.add(menu);
		}
		return menus;
	}

	public List<Map<String, Object>> getOrderByTimeIntervalAndUserId(
			Integer userId, Date month, Date now) throws SQLException {

		if (null == userId) {
			return new ArrayList<Map<String, Object>>();
		}
		List<Map<String, Object>> orders = orderDao
				.getOrderByTimeIntervalAndUserId(userId, month, now);
		for (Map<String, Object> order : orders) {
			int order_id = (Integer) order.get("order_id");
			List<Map<String, Object>> menus = orderDao.listOrderMenus(order_id);
			order.put("menuList", menus);
		}
		return orders;
	}

	/**
	 * 只保存订单
	 * 
	 * @param order
	 * @param menuList
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	public String saveOrderWithNoNotice(OrderEntity order, String menuStr)
			throws SQLException, JSONException {

		String startTime = DateUtils.formatDateTime(
				DateUtils.getStartTime(new Date()), "yyyy-MM-dd HH:mm:ss");
		String endTime = DateUtils.formatDateTime(
				DateUtils.getEndTime(new Date()), "yyyy-MM-dd HH:mm:ss");

		UserEntity user = orderDao.getUserByOrderId(order.getUser_id());
		String sql = "select count(1) count from mf_order where ordered_time >=? and ordered_time <=? and rest_id = ?";
		List<Integer> orderCount = orderDao.executeQueryForInt(sql,
				new Object[] { startTime, endTime, order.getRest_id() });
		Integer secret = CollectionUtils.isEmpty(orderCount) ? 1 : orderCount
				.get(0) + 1;
		String userPhone = (user == null) ? "13555555555" : user.getTelephone();
		order.setPhone(userPhone);
		order.setSecret(Integer.valueOf(secret
				+ userPhone.substring(userPhone.length() - 4,
						userPhone.length())));
		
		Integer orderId = orderDao.insert_pk(order);

		JSONArray menuList = new JSONArray(menuStr);
		JSONObject menuJson = null;
		Integer menuId = null;
		String menuName = null;
		Integer menuNum = null;
		Double menuPrice = null;
		String isPackage = null;
		String taste = null;
		String other = "";
		String message = "取餐时间: " + order.getRemark() + ";";

		OrderMenuEntity orderMenu = new OrderMenuEntity();
		for (int i = 0, n = menuList.length(); i < n; i++) {

			menuJson = menuList.getJSONObject(i);
			menuId = menuJson.getInt("id");
			menuName = menuJson.getString("name");
			menuPrice = menuJson.getDouble("price");
			menuNum = menuJson.getInt("num");

			isPackage = menuJson.getString("package");
			taste = menuJson.getString("taste");
			other = menuJson.getString("message");

			orderMenu.setOrder_id(orderId);
			orderMenu.setMenu_id(menuId);
			orderMenu.setName(menuName);
			orderMenu.setCount(menuNum);
			orderMenu.setPrice(menuPrice);

			message += ("菜单:" + (i + 1) + isPackage + ",");
			message += ("口味:" + taste + ",");
			message += ("备注:" + other + "; ");

			orderDao.insert(orderMenu);

		}
		orderDao.executeUpdate("update mf_order set remark = ? where id = ?",
				new Object[] { message, orderId });

		return message;
	}
}
