/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.dao.impl.AboutDao;

/**
 * @ClassName: AboutService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午5:45:27
 */
@Service
public class AboutService {

	@Autowired
	private AboutDao aboutDao;
	/**
	 * @throws SQLException 
	* @Title: getContent
	* @Description: 
	* @param @param aboutUs
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String getContent(Integer type) throws SQLException {
		
		Map<String,Object> about = aboutDao.getContent(type);
		if(null != about){
			return (String)about.get("content");
		}
		return "管理员没有添加内容！";
	}
}
