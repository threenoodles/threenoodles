/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.ReplyStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.CommonDao;
import com.cta.entity.BuildingEntity;
import com.cta.entity.SEOEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.utils.Utils;

/**
 * @ClassName: CommonService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 上午7:53:57
 */
@Service
public class CommonService {

	@Autowired
	private CommonDao commonDao;

	/**
	 * @param rest_ids 
	 * @throws UnsupportedEncodingException 
	 * @param request 
	 * @throws SQLException 
	* @Title: listSearchResult
	* @Description: 
	* @param @param menu_name
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public Map<String, List<Map<String,Object>>> listSearchResult(String menu_name, HttpSession session, List<String> rest_ids) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		if(StringUtils.isBlank(menu_name) || null == building || null == building.getId()){
			return null;
		}
		
		Map<String, List<Map<String,Object>>> restaurants = new HashMap<String,List<Map<String,Object>>>();
		List<Map<String,Object>> menus = commonDao.listSearchResult(menu_name,building.getId());
		for(Map<String,Object> menu : menus){
			Integer rest_id = (Integer) menu.get("rest_id");
			if(null != rest_id){
				double attitude = (Double) menu.get("attitude");
				double speed = (Double) menu.get("speed");
				double taste = (Double) menu.get("taste");
				int score = (int) Utils.divide(attitude+speed+taste, 3, 0);
				menu.put("score", score);
				
				if(null != restaurants.get(rest_id+"")){
					List<Map<String,Object>> restaurant = restaurants.get(rest_id+"");
					restaurant.add(menu);
				}else {
					List<Map<String,Object>> restaurant = new LinkedList<Map<String,Object>>();
					restaurant.add(menu);
					restaurants.put(rest_id+"", restaurant);
					
					rest_ids.add(rest_id+"");
				}
			}
		}
		return restaurants;
	}
	

	/**
	 * @throws SQLException 
	 * @param session 
	* @Title: listMyMessage
	* @Description: 
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> listMyMessage(HttpSession session) throws SQLException {
		Map<String,Object> count = new HashMap<String,Object>();
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null == user || null == user.getId()){
			count.put("count_comment",0);
			count.put("count_feedback",0);
			return new HashMap<String,Object>();
		}
		Map<String,Object> count_comment = commonDao.getCommentCount(user.getId(),ReplyStatus.UNREAD);
		Map<String,Object> count_feedback = commonDao.getFeedbackCount(user.getId(),ReplyStatus.UNREAD);
		
		count.put("count_comment", null == count_comment ? 0:count_comment.get("count"));
		count.put("count_feedback", null == count_feedback ? 0:count_feedback.get("count"));
		return count;
	}


	/**
	 * @throws SQLException 
	* @Title: getRestFavorite
	* @Description: 
	* @param @param session
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public List<Map<String,Object>> getFavoriteRest(HttpSession session) throws SQLException {
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null == building || null == building.getId() || null == user || null == user.getId()){
			return null;
		}
		List<Map<String,Object>> rests = commonDao.getFavoriteRest(user.getId(),building.getId());
		return rests;
	}

	/**
	 * @throws SQLException 
	* @Title: getSEOInfo
	* @Description: 
	* @param @param seoIndex
	* @param @return 
	* @return SEOEntity 
	* @throws 
	*/ 
	public SEOEntity getSEOInfo(int type,String variable) throws SQLException {
		
		SEOEntity seo = commonDao.getSEOInfo(type);
		if(null == seo){
			seo = new SEOEntity();
			seo.setTitle(SystemGlobals.getPreference("default.seo.title").replaceAll("@", variable));
			seo.setKeywords(SystemGlobals.getPreference("default.seo.keywords").replaceAll("@", variable));
			seo.setDescription(SystemGlobals.getPreference("default.seo.description").replaceAll("@", variable));
			return seo;
		}
		seo.setTitle(seo.getTitle().replaceAll("@",variable));
		seo.setKeywords(seo.getKeywords().replaceAll("@", variable));
		seo.setDescription(seo.getDescription().replaceAll("@", variable));
		return seo;
	}
}
