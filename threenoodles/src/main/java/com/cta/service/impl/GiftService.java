/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.DataStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.GiftDao;
import com.cta.entity.GiftEntity;
import com.cta.entity.Gift_UserEntity;
import com.cta.entity.UserEntity;

/**
 * @ClassName: GiftService
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-8 下午1:03:44
 */
@Service
public class GiftService {

	@Autowired
	private GiftDao giftDao;
	
	/**
	 * @param order_by 
	 * @throws SQLException 
	* @Title: listGifts
	* @Description: 
	* @param @return 
	* @return List<GiftEntity> 
	* @throws 
	*/ 
	public Map<String,List<GiftEntity>> listGifts(List<String> order_by,HttpSession session) throws SQLException {
		Map<String,List<GiftEntity>> gifts = new HashMap<String,List<GiftEntity>>();
		List<GiftEntity> gifts_ = giftDao.listGifts();
		
		for(GiftEntity gift_ : gifts_){
			Integer points = gift_.getPoints();
			List<GiftEntity> gs = gifts.get(points+"");
			if(null == gifts.get(points+"")){
				gs = new LinkedList<GiftEntity>();
			}
			gs.add(gift_);
			gifts.put(points+"", gs);
			//记录下points
			if(!order_by.contains(points+"")){
				order_by.add(points+"");
			}
		}
		
		//如果用户登录了查询用户的积分
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null != user && null != user.getId()){
			Map<String,Object> user_ = giftDao.getUserPoints(user.getId());
			Integer user_points = null == user ? 0 : (Integer)user_.get("gold");
			user.setGold(user_points);
			session.setAttribute(SessionParams.USER, user);
		}
		return gifts;
	}

	/**
	 * @throws SQLException 
	* @Title: exchangeGift
	* @Description: 兑换礼品
	* @param @param session
	* @param @param gift_id
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String addUserGift(HttpSession session, Integer gift_id) throws SQLException {
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		if(null == user || null == user.getId()){
			return DataStatus.NO_LOGIN;
		}
		if(null == gift_id){
			return DataStatus.FAILED;
		}
		
		Gift_UserEntity gift_user = new Gift_UserEntity();
		gift_user.setGift_id(gift_id);
		gift_user.setUser_id(user.getId());
		gift_user.setStatus(DataStatus.ENABLED_GENERAL);
		gift_user.setTime_exchange(new Date());
		
		//验证用户的积分是否大于要兑换的积分数
		Map<String,Object> user_ = giftDao.getUserPoints(user.getId());
		Map<String,Object> gift_ = giftDao.getGiftPoints(gift_id);
		if(null != user_ && null != user_.get("gold") && null != gift_ && null != gift_.get("points")){
			int user_points = (Integer) user_.get("gold");
			int gift_points = (Integer) gift_.get("points");
			//验证通过
			if(user_points >= gift_points){
				giftDao.addUserGift(gift_user);
				giftDao.updateUserPoints(user.getId(),user_points - gift_points);
				return DataStatus.SUCCESS;
			}
		}
		
		return DataStatus.FAILED;
	}
}
