/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cta.constant.OrderSubmitStatus;
import com.cta.constant.SessionParams;
import com.cta.dao.impl.LoginDao;
import com.cta.entity.UserEntity;

/**
 * @ClassName: LoginService
 * @Description: 登录
 * @author chenwenpeng
 * @date 2013-6-20 上午12:04:34
 */
@Service
public class LoginService {
	@Autowired
	private LoginDao loginDao;
	/**
	 * @throws UnsupportedEncodingException 
	 * @throws SQLException 
	* @Title: login
	* @Description: 
	* @param @param username
	* @param @param password
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public String login(String telephone, String password,HttpServletRequest request) throws SQLException, UnsupportedEncodingException {
		String result = OrderSubmitStatus.FAILED;
		if(StringUtils.isBlank(telephone) || StringUtils.isBlank(password)){
			return result;
		}
		telephone = URLDecoder.decode(telephone, "UTF-8");
		UserEntity user = loginDao.getUserByName(telephone.trim());
		if(null != user){
			String password_db = user.getPassword();
			if(password.equals(password_db)){
				request.getSession().setAttribute(SessionParams.USER, user);
				result = OrderSubmitStatus.SUCCEESS;
			}
		}
		return result;
	}
	
	
	/**
	* @Title: login
	* @Description: 
	* @param @param username
	* @param @param password
	* @param @param session
	* @param @return
	* @param @throws SQLException
	* @param @throws UnsupportedEncodingException 
	* @return String 
	* @throws 
	*/ 
	public String login(String username, String password,HttpSession session) throws SQLException, UnsupportedEncodingException {
		String result = OrderSubmitStatus.FAILED;
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
			return result;
		}
		username = URLDecoder.decode(username, "UTF-8");
		UserEntity user = loginDao.getUserByName(username.trim());
		if(null != user){
			String password_db = user.getPassword();
			if(password.equals(password_db)){
				session.setAttribute(SessionParams.USER, user);
				result = OrderSubmitStatus.SUCCEESS;
			}
		}
		return result;
	}
}
