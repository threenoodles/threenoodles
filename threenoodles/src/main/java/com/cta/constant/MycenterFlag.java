/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: MycenterFlag
 * @Description: 个人中心的flag
 * @author chenwenpeng
 * @date 2013-7-23 下午11:27:13
 */
public class MycenterFlag {

	/** The String TODAY*/
	public static final String TODAY = "today";
	public static final String HISTORY = "history";
	public static final String COMMENT = "comment";
	public static final String FADEBACK = "fadback";
	public static final String GIFT = "gift";
}
