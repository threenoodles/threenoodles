/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: MenuType
 * @Description: 店铺菜单类型
 * @author chenwenpeng
 * @date 2013-6-7 下午11:40:57
 */
public class MenuType {

	/** The int RECOMMENDED*/
	public static final int RECOMMENDED = 2;
	/** The int GENERAL*/
	public static final int GENERAL = 1;
}
