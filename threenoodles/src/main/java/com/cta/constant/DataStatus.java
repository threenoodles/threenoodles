/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: JsonStatus
 * @Description:
 * @author chenwenpeng
 * @date 2013-7-21 下午3:09:21
 */
public class DataStatus {

	/** The String SUCCESS*/
	public static final String SUCCESS = "success";
	/** The String FAILED*/
	public static final String FAILED = "failed";
	/** The String FAIL android 登录用的是fail*/
	public static final String FAIL = "fail";

	/** The String NO_LOGIN 用户没有登录*/
	public static final String NO_LOGIN = "no_login";

	/** The Boolean SUCCESS_BOOLEAN 成功*/
	public static final Boolean SUCCESS_BOOLEAN = true;
	/** The Boolean FAILED_BOOLEAN 失败*/
	public static final Boolean FAILED_BOOLEAN = false;

	/** The String VALID 有效的*/
	public static final int ENABLED_GENERAL = 1;
	/** The String INVALID 无效的*/
	public static final int UNENABLE_GENERAL = -1;
	/** The Integer DEFAULT_GENERAL 默认的数据*/
	public static final int DEFAULT_GENERAL = 0;

	/** The String NONE_STRING*/
	public static final String NONE_STRING = "none";
	public static final int NONE_INT = 0;
}
