/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

import java.io.Serializable;

/**
 * @ClassName: Status
 * @Description: 订单提交返回状态的常量类
 * @author chenwenpeng
 * @date 2013-6-19 下午9:47:16
 */
public class OrderSubmitStatus implements Serializable {
	/** The long serialVersionUID*/
	private static final long serialVersionUID = 5749432529422490628L;
	
 	/** The String SUCCEESS 验证通过成功 可以马上订餐*/
 	public static final String SUCCEESS = "success";
 	/** The String PASS*/
 	public static final String PASS = "\"pass\"";
 	/** The String SUCCEESS_RESERVATION 验证成功，可以预定*/
 	public static final String SUCCEESS_RESERVATION = "success_reservation";
 	/** The String FAILED 请求失败*/
 	public static final String FAILED = "failed";
 	/** The String NOLOGIN 用户没有登录*/
 	public static final String NOLOGIN = "no_login";
 	/** The String RESTCLOSED 店铺打样*/
 	public static final String RESTCLOSED = "rest_closed";
 	/** The String LESSTHANDELIVER 少于启送金额*/
 	public static final String LESSTHANDELIVER = "less_delivery";
 	
}
