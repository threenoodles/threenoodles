/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: AboutType
 * @Description: 关于网站 的类型
 * @author chenwenpeng
 * @date 2013-8-9 下午5:50:18
 */
public class AboutType {

	/** The String ABOUT_US*/
	public static final Integer ABOUT_US = 1;
	/** The String ABOUT_HELP*/
	public static final Integer ABOUT_HELP = 2;
	/** The String ABOUT_EMPLOYEE*/
	public static final Integer ABOUT_EMPLOYEE = 3;
	/** The String ABOUT_JOINUS*/
	public static final Integer ABOUT_JOINUS = 4;
	/** The Integer ABOUT_AGREEMENT 服务协议*/
	public static final Integer ABOUT_AGREEMENT = 5;
}
