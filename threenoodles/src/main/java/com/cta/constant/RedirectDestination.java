/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: RedirectDestination
 * @Description: 转向的地址
 * @author chenwenpeng
 * @date 2013-6-23 下午9:34:47
 */
public class RedirectDestination {
	
	/** The String TORESTAURANT 转到店铺详情*/
	public static final String TORESTAURANT = "to_restaurant";
	/** The String TORESTUAURANTS 转到店铺列表*/
	public static final String TORESTUAURANTS = "to_restaurants";
}
