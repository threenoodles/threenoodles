/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: SessionParams
 * @Description: session中存放的常量名
 * @author chenwenpeng
 * @date 2013-6-3 下午9:31:09
 */
public class SessionParams {

	/** The String USER*/
	public static final String USER = "user";
	/** The String USER_DETAIL 用户详情*/
	public static final String USER_DETAIL = "user_detail";
	/** The String BUILDING session中存放的用户点餐的building*/
	public static final String BUILDING = "building";
	 /** The String RESTAURANT 用户点餐的店铺*/
	public static final String RESTAURANT = "restaurant";
	/** The String PHONECODE_FIRSTTIME 用户第一次订餐的验证码*/
	public static final String PHONECODE_FIRSTTIME = "phonecode_firsttime";
	/** The String PHONECODE_FIRSTTIME 用户重置密码的验证码*/
	public static final String PHONECODE_RESETPASSOWRD = "phonecode_resetpassowrd";
	
	public static final String ORDER_MENULIST = "order_menuList";
	public static final String ORDER_TOTALCOUNT = "order_totalcount";
	public static final String ORDER_CARRIAGE = "order_carriage";
	/** The String ORDER_TOTALSCORE 订单的积分*/
	public static final String ORDER_TOTALSCORE = "order_score";
	/** The String ORDER_RESERVETIME 订单下发时间*/
	public static final String ORDER_RESERVETIME = "order_reserveTime";
	public static final String ORDER_REST_NAME = "order_rest_name";
	
}
