/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: CommentStatus
 * @Description: 评论的状态表
 * @author chenwenpeng
 * @date 2013-7-30 下午11:35:11
 */
public class ReplyStatus {

	/** The int UNREAD 未读*/
	public static final int UNREAD = 1;
	
	/** The int READ 已读*/
	public static final int READ = 2;
}
