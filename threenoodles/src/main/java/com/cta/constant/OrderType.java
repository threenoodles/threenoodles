/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: OrderType
 * @Description: 订单类型
 * @author chenwenpeng
 * @date 2013-6-24 下午11:25:16
 */
public class OrderType {
	/** The int ORDER_GENERAL 普通定案*/
	public static final int ORDER_GENERAL = 1;
	/** The int ORDER_PREDETERMINE预定订单*/
	public static final int ORDER_RESERVE = 2;
	/** The int ORDER_ANDROID 安卓订餐*/
	public static final int ORDER_ANDROID = 3;
	/** The int ORDER_IPHONE iphone订餐*/
	public static final int ORDER_IPHONE = 4;
	/** The int ORDER_GROUPRESERVATION 企业团餐*/
	public static final int ORDER_GROUPRESERVATION = 5;
	
	/**@Field the int ORDER_TYPE_ADD 管理员后台添加订单*/
	public static final int ORDER_ADD = 5;
}
