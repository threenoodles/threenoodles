/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.constant;

/**
 * @ClassName: TopNavis
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午7:23:26
 */
public class TopNavis {

	/** The String GIFT*/
	public static final String GIFT = "gift";
	/** The String MYCENTER*/
	public static final String MYCENTER = "mycenter";
	/** The String COMMENT*/
	public static final String COMMENTS = "comments";
}
