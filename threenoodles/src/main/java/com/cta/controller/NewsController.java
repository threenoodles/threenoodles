/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.constant.SEOType;
import com.cta.entity.NewsEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.NewsService;

/**
 * @ClassName: NewsController
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午2:48:50
 */
@Controller
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private NewsService newsService;
	@Autowired
	private CommonService commonService;
	/**
	* @Title: listNewsDetail
	* @Description: 返回新闻详情
	* @param @param model
	* @param @param request
	* @param @param news_id
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/listNewsDetail/{news_id}")
	public String listNewsDetail(ModelMap model,HttpServletRequest request,
			@PathVariable("news_id") Integer news_id) throws SQLException{
		
		List<Map<String,Object>> news_title_list = newsService.listNewsTile(request.getSession());
		NewsEntity news = newsService.listNewsDetail(news_id);
		model.put("news", null == news ? new NewsEntity() : news);
		model.put("newsList", news_title_list);
		
		model.put("seo", commonService.getSEOInfo(SEOType.SEO_NEWS,null == news? SystemGlobals.getPreference("company.name") : news.getTitle()));
		return "/news/news";
	}
}
