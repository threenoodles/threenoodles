/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.MycenterFlag;
import com.cta.constant.SEOType;
import com.cta.constant.SessionParams;
import com.cta.constant.TopNavis;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ListPager;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.MycenterService;

/**
 * @ClassName: MycenterController
 * @Description: 个人中心
 * @author chenwenpeng
 * @date 2013-7-23 下午10:52:06
 */
@Controller
@RequestMapping("/mycenter")
public class MycenterController {

	@Autowired
	private MycenterService mycenterService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * @throws SQLException 
	* @Title: toMycenter
	* @Description: 转到个人中心
	* @param @param modelMap
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/order")
	public String toMycenter(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "flag", defaultValue = "") String flag) throws SQLException{
		HttpSession session = request.getSession();
		if(StringUtils.isEmpty(flag) || null == session.getAttribute(SessionParams.USER)){
			return "redirect:/";
		}
		
		modelMap.put("dataList", mycenterService.listData(flag,session));
		modelMap.put("flag", flag);
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_MYCENTER,""));
		return MycenterFlag.TODAY.equals(flag) ? "/mycenter/mycenter_today" : "/mycenter/mycenter_history";
	}
	
	/**
	* @Title: listHistoryOrder
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param flag
	* @param @param start
	* @param @param limit
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/order/list_historyOrder")
	public String listHistoryOrder(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws SQLException{
		HttpSession session = request.getSession();
		if(null == session.getAttribute(SessionParams.USER)){
			return "jsonView";
		}
		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);
		
		modelMap.put("dataList", mycenterService.listHistoryOrder(session,pager));
		return "jsonView";
	}
	
	
	/**
	* @Title: toMyComments
	* @Description: 列出未读回复的评论
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/comment")
	public String toMyComments(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "flag", defaultValue = "") String flag) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "redirect:/";
		}
		modelMap.put("dataList", mycenterService.listUnReadCommentReplies(user.getId()));
		modelMap.put("service_tel", SystemGlobals.getPreference("service.tel"));
		modelMap.put("flag", flag);
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_comment";
	}
	
	
	/**
	* @Title: listComments
	* @Description: 列出所有的评论
	* @param @param modelMap
	* @param @param request
	* @param @param start
	* @param @param limit
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/comment/listComments")
	public String listComments(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "jsonView";
		}
		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);
		
		modelMap.put("dataList", mycenterService.listComments(user.getId(),pager));
		return "jsonView";
	}
	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @Title: submitComment
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param flag
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/comment/submitComment")
	public String submitComment(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "comment_id", defaultValue = "") Integer comment_id,
			@RequestParam(value = "content", defaultValue = "") String content) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			modelMap.put("success", "false");
			return "jsonView";
		}
		
		List<String> messages = mycenterService.submitComment(comment_id,user.getId(),content);
		modelMap.put("success",null==messages?true:false);
		return "jsonView";
	}
	
	/**
	* @Title: toMyFeedbacks
	* @Description: 转到我的未读反馈
	* @param @param modelMap
	* @param @param request
	* @param @param flag
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/feedback")
	public String toMyFeedbacks(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "flag", defaultValue = "") String flag) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "redirect:/";
		}
		modelMap.put("dataList", mycenterService.listUnReadFeedbacks(user.getId()));
		modelMap.put("service_tel", SystemGlobals.getPreference("service.tel"));
		modelMap.put("flag", flag);
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_feedback";
	}
	
 
	/**
	* @Title: listFeedbacks
	* @Description: 列出所有历史反馈
	* @param @param modelMap
	* @param @param request
	* @param @param start
	* @param @param limit
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/feedback/listFeedbacks")
	public String listFeedbacks(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "jsonView";
		}
		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);
		
		modelMap.put("dataList", mycenterService.listFeedbacks(user.getId(),pager));
		return "jsonView";
	}
	

	/**
	 * @throws UnsupportedEncodingException 
	* @Title: submitFeedback
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param username
	* @param @param password
	* @param @param content
	* @param @return
	* @param @throws SQLException
	* @param @throws IllegalAccessException
	* @param @throws InvocationTargetException
	* @param @throws NoSuchMethodException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/feedback/submitFeedback")
	public String submitFeedback(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "password", defaultValue = "") String password,
			@RequestParam(value = "content", defaultValue = "") String content) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException{
	
		String reuslt = mycenterService.submitFeedback(request.getSession(),username,password,content);
		modelMap.put("success",reuslt);
		return "jsonView";
	}
	
	
	/**
	 * @Title: submitFeedback
	 * @Description: 继续反馈
	 * @param @param modelMap
	 * @param @param request
	 * @param @param submitFeedback
	 * @param @param content
	 * @param @return
	 * @param @throws SQLException
	 * @param @throws IllegalAccessException
	 * @param @throws InvocationTargetException
	 * @param @throws NoSuchMethodException 
	 * @return String 
	 * @throws 
	 */ 
	@RequestMapping("/feedback/submitFeedbackReply")
	public String submitFeedbackReply(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "feedback_id", defaultValue = "") Integer submitFeedback,
			@RequestParam(value = "content", defaultValue = "") String content) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			modelMap.put("success", "false");
			return "jsonView";
		}
		
		List<String> messages = mycenterService.submitFeedbackReply(submitFeedback,user.getId(),content);
		modelMap.put("success",null==messages?true:false);
		return "jsonView";
	}
	
	/**
	* @Title: toMygifts
	* @Description: 列出我的所有兑换的礼品
	* @param @param modelMap
	* @param @param request
	* @param @param flag
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/gift")
	public String toMygifts(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "flag", defaultValue = "") String flag) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "redirect:/";
		}
		modelMap.put("dataList", mycenterService.listMygifts(user.getId()));
		modelMap.put("service_tel", SystemGlobals.getPreference("service.tel"));
		modelMap.put("flag", flag);
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_gift";
	}
	
	/**
	* @Title: toModifyPassword
	* @Description: 修改密码
	* @param @param modelMap
	* @param @param request
	* @param @param flag
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/password")
	public String toModifyPassword(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "flag", defaultValue = "") String flag) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "redirect:/";
		}
		modelMap.put("flag", flag);
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_modifyPassword";
	}
	
	/**
	* @Title: resetPassword
	* @Description: 修改密码
	* @param @param modelMap
	* @param @param request
	* @param @param password
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/
	@RequestMapping("/password/resetPassword")
	public String resetPassword(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "password", defaultValue = "") String password) throws SQLException{
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user){
			return "redirect:/";
		}
		Boolean flag = mycenterService.resetPassword(user.getId(),password);
		modelMap.put("success", flag);
		return "jsonView";
	}
	
	/**
	* @Title: toQuery
	* @Description: 转到查询用户页面(用户没用登录)
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/password/toQuery")
	public String toQuery(ModelMap modelMap) throws SQLException{
		
		modelMap.put("flag", "password");
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_modifyPassword_query";
	}
	/**
	* @Title: queryUserInfo
	* @Description: 查询用户(用户没用登录)
	* @param @param modelMap
	* @param @param request
	* @param @param username
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/password/queryUserInfo")
	public String queryUserInfo(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "data", defaultValue = "") String data) throws SQLException{

		Map<String,Object> user_info = mycenterService.queryUserInfo(data);
		modelMap.put("data", user_info);
		return "jsonView";
	}
	
	/**
	* @Title: toModifyPassword
	* @Description: 转到修改页面(用户没用登录)
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/password/toModifyPassword")
	public String toModifyPassword(ModelMap modelMap,
			@RequestParam(value = "username", defaultValue = "") String username) throws SQLException{
		
		Map<String,Object> user_info = mycenterService.queryUserInfo(username);
		if(null == user_info){
			return "redirect:/";
		}
		modelMap.put("data", user_info);
		modelMap.put("flag", "password");
		modelMap.put("naviFlag", TopNavis.MYCENTER);
		return "/mycenter/mycenter_modifyPassword_uk";
	}
	
	/**
	* @Title: resetPassword_uk
	* @Description: 重置密码(用户没有登录)
	* @param @param modelMap
	* @param @param request
	* @param @param password
	* @param @param code
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/password/resetPassword_uk")
	public String resetPassword_uk(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "password", defaultValue = "") String password,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "code", defaultValue = "") String code) throws SQLException{
		
		Boolean flag = mycenterService.resetPassword(request.getSession(),password,username,code);
		modelMap.put("success", flag);
		return "jsonView";
	}
	
}
