/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.SEOType;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.LoginService;

/**
 * @ClassName: LoginController
 * @Description: 登录控制器
 * @author chenwenpeng
 * @date 2013-6-20 上午12:00:40
 */
@Controller
@RequestMapping("/login")
public class LoginController {
	@Autowired
	private LoginService loginService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * @throws SQLException 转到登录页
	* @Title: toRestaurants
	* @Description: 
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toLogin")
	public String toLogin(
			@RequestParam(value = "building_alias", defaultValue = "") String building_alias,
			@RequestParam(value = "rest_alias", defaultValue = "") String rest_alias,
			@RequestParam(value = "login_flag", defaultValue = "") String login_flag,
			ModelMap modelMap) throws SQLException{
		
		modelMap.put("login_flag", login_flag);
		modelMap.put("building_alias", building_alias);
		modelMap.put("rest_alias", rest_alias);
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_LOGIN,""));
		return "login/login";
	}
	
	/**
	 * @throws UnsupportedEncodingException 
	* @Title: login
	* @Description: 登录
	* @param @param username
	* @param @param password
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/testLogin")
	public String testLogin(
			@RequestParam(value = "username", defaultValue = "") String telephone,
			@RequestParam(value = "password", defaultValue = "") String password,
			HttpServletRequest request,
			ModelMap modelMap) throws SQLException, UnsupportedEncodingException{
		
		String result = loginService.login(telephone,password,request);
		modelMap.put("success", result);
		return "jsonView";
	}
	
	/**
	* @Title: logout
	* @Description: 退出
	* @param @param request
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException
	* @param @throws UnsupportedEncodingException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/logout")
	public String logout(
			HttpServletRequest request,
			ModelMap modelMap) throws SQLException, UnsupportedEncodingException{
		
		request.getSession().invalidate();
		return "redirect:/";
	}
}
