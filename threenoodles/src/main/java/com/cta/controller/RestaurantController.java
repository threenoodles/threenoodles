/**
 * @FileName: RestaurantController.java
 * @Package com.cta.controller
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-20 下午01:34:12
 * @version V1.0
 */
package com.cta.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.constant.SEOType;
import com.cta.constant.SessionParams;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BroadCastEntity;
import com.cta.entity.BuildingEntity;
import com.cta.entity.NewsEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.NewsService;
import com.cta.service.impl.RestaurantService;

/**
 * @ClassName: RestaurantController
 * @Description: 店铺模块的控制器
 * @author chenwenpeng
 * @date 2013-5-20 下午01:34:12
 *
 */
@Controller
@RequestMapping("/restaurant")
public class RestaurantController {

	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommonService commonService;
	/**
	 * @throws IOException 
	 * @throws SQLException 
	 * @Title: toRestaurants
	 * @Description: 列出某一个楼宇下的所有店铺
	 * @param @param modelMap
	 * @param @return
	 * @return String
	 * @throws
	 */
	@RequestMapping("/toRestaurants/{alias}")
	public String toRestaurants(
			ModelMap modelMap,
			HttpServletRequest request,
			HttpServletResponse response,
			@PathVariable("alias") String alias) throws SQLException, IOException {
		List<RestaurantDto> restaurants = restaurantService.listRestaurants(alias,request.getSession());
		if(null == restaurants){
			return "redirect:/";
		}
		List<NewsEntity> newsList = newsService.listLatestNews(request.getSession());
		List<BroadCastEntity> broadCasts = restaurantService.listLatestBroadcasts(request.getSession());
		modelMap.put("restaurants", restaurants);
		modelMap.put("newsList", newsList);
		modelMap.put("broadCastList", broadCasts);
		BuildingEntity building = (BuildingEntity)request.getSession().getAttribute(SessionParams.BUILDING);
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_RESTAURANTS,null == building ? SystemGlobals.getPreference("company.name") : building.getName()));
		return "restaurant/restaurants";
	}
	
	/**
	 * @throws SQLException 
	* @Title: listRestCategory
	* @Description: 根据楼宇的id 获取楼宇下面店铺对应的所属分类信息
	* @param @param modelMap
	* @param @return 
	* @return List<Map<String,Integer>> 
	* @throws 
	*/ 
	@RequestMapping("listRestCategory")
	public @ResponseBody List<Map<String,String>> listRestCategory(
			HttpServletRequest request
			) throws SQLException{
		
		return restaurantService.listRestCategory(request.getSession());
	}
	
	/**
	 * @throws IOException 
	 * @throws SQLException 
	 * @Title: toRestDetail
	 * @Description: 列出某一个店铺的详情
	 * @param @param modelMap
	 * @param @return
	 * @return String
	 * @throws
	 */
	@RequestMapping("/toRestaurantDetail/{alias}")
	public String toRestaurantDetail(
		ModelMap modelMap,
		HttpServletRequest request,
		HttpServletResponse response,
		@PathVariable("alias") String alias,
		@RequestParam(value = "menu_id", defaultValue = "") Integer menu_id) throws SQLException, IOException {
		
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
		response.setDateHeader("Expires", 0);
		response.setHeader("Pragma", "no-cache");
		response.setHeader("pragma-directive", "no-cache");
		
		RestaurantDto restaurant = restaurantService.getRestDetail(alias,request.getSession());
		if(null == restaurant){
			return "forward:/";
		}
		Map<String,Map<String,Object>> menus = restaurantService.getMenuList(restaurant);
		modelMap.put("restaurant", restaurant);
		modelMap.put("menus", menus);
		modelMap.put("menus_recommend", restaurant.getMenus_recommend());
		//用户点击来一份的订单id
		modelMap.put("menu_id", menu_id);
		modelMap.put("service_tel", SystemGlobals.getPreference("service.tel"));
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_RESTAURANT,restaurant.getName()));
		return "restaurant/restaurant";
	}
	
	
	/**
	* @Title: checkStatus
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param restId
	* @param @param menuList
	* @param @param totalPrice
	* @param @param carriage
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/checkStatus")
	public String checkStatus(ModelMap modelMap,
			HttpServletRequest request,
			@RequestParam(value = "restId", defaultValue = "") Integer restId,
			@RequestParam(value = "menuList", defaultValue = "") String menuList,
			@RequestParam(value = "totalPrice", defaultValue = "0") Double totalPrice,
			@RequestParam(value = "carriage", defaultValue = "0") Double carriage) throws SQLException{
		String status = restaurantService.checkStatus(restId,menuList,totalPrice,carriage,request.getSession());
		modelMap.put("data", status);
		return "jsonView";
	}
	/**
	* @Title: addFavourite
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param restId
	* @param @param menuList
	* @param @param totalPrice
	* @param @param carriage
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/addFavourite")
	public String addFavourite(ModelMap modelMap,
			HttpServletRequest request,
			@RequestParam(value = "restId", defaultValue = "") Integer restId) throws SQLException{
		String success = restaurantService.addFavourite(restId,request.getSession());
		modelMap.put("success", success);
		return "jsonView";
	}
}
