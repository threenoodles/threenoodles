/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.SEOType;
import com.cta.constant.TopNavis;
import com.cta.entity.GiftEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.GiftService;

/**
 * @ClassName: GiftController
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-8 下午1:00:31
 */
@Controller
@RequestMapping("/gift")
public class GiftController {
	@Autowired
	private GiftService giftService;
	@Autowired
	private CommonService commonService;
	/**
	* @Title: toGifts
	* @Description: 
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toGifts")
	public String toGifts(ModelMap modelMap,HttpServletRequest request) throws SQLException {
		List<String> order_by = new LinkedList<String>();
		Map<String,List<GiftEntity>> gifts = giftService.listGifts(order_by,request.getSession());
				
		modelMap.put("order_by", order_by);
		modelMap.put("data", gifts);
		modelMap.put("service_tel", SystemGlobals.getPreference("service.tel"));
		modelMap.put("naviFlag", TopNavis.GIFT);
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_GIFTS,""));
		return "/gift/gifts";
	}
	
	
	/**
	* @Title: exchangeGift
	* @Description: 用户兑换礼品
	* @param @param modelMap
	* @param @param gift_id
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/exchangeGift")
	public String exchangeGift(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "gift_id", defaultValue = "") Integer gift_id) throws SQLException {
		
		String result = giftService.addUserGift(request.getSession(),gift_id);
		modelMap.put("success",result);
		return "jsonView";
	}
}
