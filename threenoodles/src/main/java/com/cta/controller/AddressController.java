/**
* @FileName: AddressController.java
* @Package com.cta.controller
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-18 下午12:49:34
* @version V1.0
*/
package com.cta.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.service.impl.AddressService;

/**
 * @ClassName: AddressController
 * @Description: 管理用户地址
 * @author chenwenpeng
 * @date 2013-5-18 下午12:49:34
 *
 */
@Controller
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressService addressService;
	/**
	 * @throws SQLException 
	* @Title: listUserAddress
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("listUserAddress")
	public String listUserAddress(
			ModelMap modelMap,
			HttpServletRequest request) throws SQLException{
	
		List<Map<String,Object>> addresses = addressService.listUserAddress(request.getSession());
		modelMap.put("dataList", addresses);
		return "jsonView";
	}
}
