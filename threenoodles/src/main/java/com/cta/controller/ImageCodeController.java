/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * @ClassName: ImageCodeController
 * @Description: 验证码
 * @author chenwenpeng
 * @date 2013-6-12 下午4:50:02
 */
@Controller
@RequestMapping("/admin/imagecode")
public class ImageCodeController {

	public static final String RANDOM_NUMBER_TITLE = "RANDOM_NUMBER_TITLE";
	static final private String CONTENT_TYPE = "image/jpeg; charset=UTF-8";
	static final private int imageWidth = 55;
	static final private int imageHeight = 30;
	static final private int image_Start_X = 5;
	static final private int image_Start_Y = 17;
	static final private String FONT_NAME = "TimesRoman";
	static final private int FONT_STYLE = Font.PLAIN;
	static final private int FONT_SIZE = 20;
	static private int randomNumber = 0;

	@RequestMapping("getImageCode")
	public void getImageCode(final HttpServletRequest request,
	        final HttpServletResponse response) throws ServletException,
	        IOException {
		response.setContentType(ImageCodeController.CONTENT_TYPE);
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
		response.setDateHeader("Expires", 0);
		response.setHeader("Pragma", "no-cache");
		response.setHeader("P3P", "CP=CAO PSA OUR");
		final ServletOutputStream out = response.getOutputStream();
		HttpSession session = null;
		session = request.getSession();

		String sessionRandomNumber = "";

		sessionRandomNumber = (String) session
		        .getAttribute(ImageCodeController.RANDOM_NUMBER_TITLE);
		sessionRandomNumber = getRandomNumber();
		session.setAttribute(ImageCodeController.RANDOM_NUMBER_TITLE,
		        sessionRandomNumber);

		final BufferedImage image = new BufferedImage(
		        ImageCodeController.imageWidth,
		        ImageCodeController.imageHeight,
		        BufferedImage.TYPE_INT_RGB);
		final Graphics g = image.getGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, ImageCodeController.imageWidth,
		        ImageCodeController.imageHeight);
		g.setColor(Color.black);
		final Font font = new Font(ImageCodeController.FONT_NAME,
		        ImageCodeController.FONT_STYLE,
		        ImageCodeController.FONT_SIZE);
		g.setFont(font);
		g.drawString(sessionRandomNumber,
		        ImageCodeController.image_Start_X,
		        ImageCodeController.image_Start_Y);
		final JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		encoder.encode(image);
	}
	
	private String getRandomNumber() {
		final Random rNumber = new Random(System.currentTimeMillis());
		do {
			ImageCodeController.randomNumber = rNumber.nextInt(9999);
		} while ( ( ImageCodeController.randomNumber <= 1000 )
		        || ( ImageCodeController.randomNumber >= 9999 ));
		return String.valueOf(ImageCodeController.randomNumber);
	}

}
