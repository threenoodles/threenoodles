/**
* @FileName: IndexController.java
* @Package com.cta.controller
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-18 下午04:07:12
* @version V1.0
*/
package com.cta.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.constant.SEOType;
import com.cta.entity.BuildingEntity;
import com.cta.entity.RegionEntity;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.IndexService;

/**
 * @ClassName: IndexController
 * @Description:
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {

	@Autowired
	private IndexService indexService;
	@Autowired
	private CommonService commonService;
	/**
	 * @throws IOException 
	 * @throws ServletException 
	 * @throws SQLException
	* @Title: toIndex
	* @Description: 列出前台所有的地址和热门区域
	* @param @return
	* @return String
	* @throws
	*/
	@RequestMapping("")
	public String toIndex(
			ServletRequest request, 
			ServletResponse response,
			ModelMap model) throws SQLException, ServletException, IOException{
		//热门楼宇
		List<BuildingEntity> buildings_hot = new LinkedList<BuildingEntity>();
		//所有普通楼宇
		Map<String, List<BuildingEntity>> buildings_all = new HashMap<String, List<BuildingEntity>>();
		//列出所有的区域
		List<RegionEntity> regions = new LinkedList<RegionEntity>();

		indexService.listBuildings(regions,buildings_all,buildings_hot);
		model.put("regions", regions);
		model.put("buildings_all", buildings_all);
		model.put("buildings_hot", buildings_hot);
		model.put("seo", commonService.getSEOInfo(SEOType.SEO_INDEX,""));
		return "index/index";
	}
}
