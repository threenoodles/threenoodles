package com.cta.controller.android;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.platform.config.SystemGlobals;

@Controller
@RequestMapping("/android")
public class UpdateController {

	@RequestMapping("checkUpdate")
	public void checkUpdate(HttpServletResponse response,
			@RequestParam(value = "localVersionCode", defaultValue = "") Integer localVersionCode) throws Exception {
		if(null == localVersionCode){
			response.setStatus(200);
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.write("null");
			return;
		}
		
		Integer serviceVersionCode = SystemGlobals.getIntPreference("android_service_version_code", 2);
		//说明本地需要更新了
		if(serviceVersionCode > localVersionCode){
			JSONObject updateMsg = new JSONObject();
			String versionName = SystemGlobals.getPreference("android_service_version_name");
			String versionDesc = SystemGlobals.getPreference("android_service_version_update_desc");
			String versionSize = SystemGlobals.getPreference("android_service_version_size");
			updateMsg.put("versionName", versionName);
			updateMsg.put("versionDesc", versionDesc);
			updateMsg.put("versionSize", versionSize);

			response.setStatus(200);
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.write(updateMsg.toString());
			
		}else {
			PrintWriter out = response.getWriter();
			out.write("null");
		}
	}
}
