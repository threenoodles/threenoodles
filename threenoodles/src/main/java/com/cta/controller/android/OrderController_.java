/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller.android;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.constant.OrderStatus;
import com.cta.constant.OrderType;
import com.cta.entity.OrderEntity;
import com.cta.service.impl.android.OrderService_;
import com.cta.utils.DateUtils;

/**
 * @ClassName: OrderController_
 * @Description:
 * @author chenwenpeng
 * @date 2013-9-1 下午2:07:12
 */
@Controller
@RequestMapping("")
public class OrderController_ {

	@Autowired
	private OrderService_ orderService;

	@RequestMapping("/android/getOrder_today")
	public @ResponseBody
	Map<String, Object> getOrder_today(
			@RequestParam(value = "userId", defaultValue = "") Integer userId)
			throws Exception {

		// List<Map<String, Object>> orderList = orderService
		// .getOrder_today(userId);
		Map<String, Object> result = new HashMap<String, Object>(2);
		Date endTime = DateUtils.getEndTime(new Date());
		Date startTime = DateUtils.getDateByDayInterval(
				DateUtils.getStartTime(endTime), -30);
		List<Map<String, Object>> otherOrderList = orderService
				.getOrderByTimeIntervalAndUserId(userId, startTime, endTime);

		result.put("today", DateUtils.formatDateTime(endTime, "yyyy-MM-dd"));
		result.put("dataList", otherOrderList);
		return result;
	}

	/**
	 * @Title: saveOrder
	 * @Description:
	 * @param @param request
	 * @param @param userId
	 * @param @param phone
	 * @param @param address
	 * @param @param buildingId
	 * @param @param menuList
	 * @param @param restId
	 * @param @param carriage
	 * @param @param remark
	 * @param @param totalPrice
	 * @param @param orderBy
	 * @param @throws Exception
	 * @return void
	 * @throws
	 */
	@RequestMapping("/order/saveOrder")
	public void saveOrder(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "userId", defaultValue = "") Integer userId,
			@RequestParam(value = "restId", defaultValue = "") Integer restId,
			@RequestParam(value = "buildingId", defaultValue = "") Integer buildingId,
			@RequestParam(value = "menuList", defaultValue = "") String menuList,
			@RequestParam(value = "totalPrice", defaultValue = "") Double totalPrice,
			@RequestParam(value = "orderTime", defaultValue = "") String orderTime,
			@RequestParam(value = "orderBy", defaultValue = "") String orderBy)throws Exception {

		OrderEntity order = new OrderEntity();
		order.setAddress("none");
		order.setBuilding_id(buildingId);
		order.setCarriage(0.0);
		order.setOrdered_time(new Date());
		order.setPhone(StringUtils.EMPTY);
		order.setRemark(orderTime);
		order.setRest_id(restId);
		order.setStatus(OrderStatus.COOKING);
		order.setTotalPrice(totalPrice);
		order.setType(OrderType.ORDER_ANDROID);
		order.setUser_id(userId);
		order.setOrdered_time(new Date());
		order.setStatus(OrderStatus.COOKING);
		order.setType(OrderType.ORDER_ANDROID);
		
		orderService.saveOrderWithNoNotice(order, menuList);
		response.setStatus(200);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.write("success");
	}
}
