/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller.android;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.service.impl.android.Login_RegistService;

/**
 * @ClassName: Login_RegistController
 * @Description: 登录和注册
 * @author chenwenpeng
 * @date 2013-9-1 上午10:54:48
 */
@Controller
@RequestMapping("/android")
public class Login_RegistController {
	@Autowired
	private Login_RegistService login_registService;
	@RequestMapping("/login")
	public void login(ModelMap modelMap,HttpServletResponse response,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "password", defaultValue = "") String password) throws Exception {

		JSONObject json = login_registService.login(username,password);
		response.setStatus(200);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.write(json.toString());
	}
	
	/**
	* @Title: regist
	* @Description: 
	* @param @param modelMap
	* @param @param response
	* @param @param username
	* @param @param password
	* @param @throws Exception 
	* @return void 
	* @throws 
	*/ 
	@RequestMapping("/regist")
	public void regist(ModelMap modelMap,HttpServletResponse response,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "phone", defaultValue = "") String phone,
			@RequestParam(value = "password", defaultValue = "") String password) throws Exception {
		
		JSONObject json = login_registService.regist(username,password,phone);
		response.setStatus(200);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.write(json.toString());
	}
}
