/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller.android;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.service.impl.android.AddressService_;

/**
 * @ClassName: Android_AddressController
 * @Description:
 * @author chenwenpeng
 * @date 2013-8-28 下午10:19:20
 */

@Controller
@RequestMapping("/android")
public class AddressController_ {

	@Autowired
	private AddressService_ addressService;

	/**
	* @Title: getAddresses
	* @Description:
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException
	* @return String
	* @throws
	*/
	@RequestMapping("getAddresses")
	public @ResponseBody List<Map<String,Object>> getAddresses(ModelMap modelMap,HttpServletRequest request) throws SQLException{

		List<Map<String,Object>> addresses = addressService.listAddress();
		return addresses;
	}
}
