/**
 *
 */
package com.cta.controller.android;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.service.impl.android.RestaurantService_;

/**
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/android")
public class RestaurantController_ {

	@Autowired
	private RestaurantService_ restaurantService;

	/**
	 * @param modelMap
	 * @param buildingId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("getRestaurants")
	public @ResponseBody List<Map<String,Object>> getRestaurants(ModelMap modelMap,
			@RequestParam(value = "buildingId", defaultValue = "") Integer buildingId) throws Exception {

		List<Map<String,Object>> restaurantList = restaurantService.getRestaurants(buildingId);
		return restaurantList;
	}

	/**获取店铺详情
	 * @param restId
	 * @param buildingId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("getRestInfo")
	public @ResponseBody Map<String,Object> getRestInfo(ModelMap modelMap,
			@RequestParam(value = "restId", defaultValue = "") Integer restId,
			@RequestParam(value = "buildingId", defaultValue = "") Integer buildingId) throws Exception {

		Map<String,Object> restInfo = restaurantService.getRestInfo(restId,buildingId);
		return restInfo;
	}
	/**
	* @Title: getCategoyr_menuList
	* @Description: 
	* @param @param modelMap
	* @param @param restId
	* @param @param cateId
	* @param @return
	* @param @throws Exception 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	@RequestMapping("getCategoyr_menuList")
	public @ResponseBody List<Map<String, Object>> getCategoyr_menuList(ModelMap modelMap,
			@RequestParam(value = "restId", defaultValue = "") Integer restId,
			@RequestParam(value = "cateId", defaultValue = "") Integer cateId) throws Exception {
		
		List<Map<String, Object>> menuList = restaurantService.getCategoyr_menuList(restId,cateId);
		return menuList;
	}
}
