/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.SEOType;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.ReistService;

/**
 * @ClassName: RegistController
 * @Description: 注册
 * @author chenwenpeng
 * @date 2013-7-21 上午6:37:55
 */
@Controller
@RequestMapping("/regist")
public class RegistController {
	@Autowired
	private ReistService reistService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * @throws SQLException 转到注册页
	* @Title: toRegist
	* @Description: 
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toRegist")
	public String toRegist(
			@RequestParam(value = "building_alias", defaultValue = "") String building_alias,
			@RequestParam(value = "rest_alias", defaultValue = "") String rest_alias,
			@RequestParam(value = "regist_flag", defaultValue = "") String regist_flag,
			ModelMap modelMap) throws SQLException{
		
		modelMap.put("regist_flag", regist_flag);
		modelMap.put("building_alias", building_alias);
		modelMap.put("rest_alias", rest_alias);
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ORDER_REGIST,""));
		return "regist/regist";
	}
	/**
	 * @throws SQLException 验证用户名
	 * @Title: toRegist
	 * @Description: 
	 * @param @return 
	 * @return String 
	 * @throws 
	 */ 
	@RequestMapping("/checkUsername")
	public String checkUsername(
			@RequestParam(value = "username", defaultValue = "") String username,
			ModelMap modelMap) throws SQLException{
		
		boolean result = reistService.checkUsername(username);
		modelMap.put("success", result);
		return "jsonView";
	}
	/**
	* @Title: checkPhone
	* @Description: 验证用户的电话是否唯一
	* @param @param phone
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/checkPhone")
	public String checkPhone(
			@RequestParam(value = "phone", defaultValue = "") String phone,
			ModelMap modelMap) throws SQLException{
		
		boolean result = reistService.checkPhone(phone);
		modelMap.put("success", result);
		return "jsonView";
	}
	
	/**
	 * @throws SQLException 验证用户验证码
	 * @Title: toRegist
	 * @Description: 
	 * @param @return 
	 * @return String 
	 * @throws 
	 */ 
	@RequestMapping("/checkImagecode")
	public String checkImagecode(
			HttpServletRequest request,
			@RequestParam(value = "imagecode", defaultValue = "") String imagecode,
			ModelMap modelMap) throws SQLException{
		
		boolean result = reistService.checkImagecode(imagecode,request);
		modelMap.put("success", result);
		return "jsonView";
	}
	
	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 注册
	 * @Title: toRegist
	 * @Description: 
	 * @param @return 
	 * @return String 
	 * @throws 
	 */ 
	@RequestMapping("/registUser")
	public String regist(
		HttpServletRequest request,
		@RequestParam(value = "phone", defaultValue = "") String phone,
		@RequestParam(value = "username", defaultValue = "") String username,
		@RequestParam(value = "password", defaultValue = "") String password,
		ModelMap modelMap) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
	
		List<String> message = reistService.addUser(username,password,phone,request);
		modelMap.put("success", null==message ? true : false);
		modelMap.put("messages", message);
		return "jsonView";
	}
}
