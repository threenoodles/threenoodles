/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.constant.AboutType;
import com.cta.constant.SEOType;
import com.cta.service.impl.AboutService;
import com.cta.service.impl.CommonService;

/**
 * @ClassName: AboutController
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午5:41:11
 */
@Controller
@RequestMapping("/about")
public class AboutController {
	@Autowired
	private AboutService aboutService;
	@Autowired
	private CommonService commonService;
	/**
	* @Title: toAboutUs
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toAboutUs")
	public String toAboutUs(ModelMap modelMap) throws SQLException {
		
		String content = aboutService.getContent(AboutType.ABOUT_US);
		modelMap.put("content", content);
		modelMap.put("flag", AboutType.ABOUT_US);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ABOUT_US,""));
		return "/about/about";
	}
	/**
	* @Title: toAreement
	* @Description: 服务协议
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toAreement")
	public String toAreement(ModelMap modelMap) throws SQLException {
		
		String content = aboutService.getContent(AboutType.ABOUT_AGREEMENT);
		modelMap.put("content", content);
		modelMap.put("flag", AboutType.ABOUT_AGREEMENT);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ABOUT_AGREEMENT,""));
		return "/about/about";
	}
	/**
	* @Title: toJoinUs
	* @Description: 
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toJoinUs")
	public String toJoinUs(ModelMap modelMap) throws SQLException {
		
		String content = aboutService.getContent(AboutType.ABOUT_JOINUS);
		modelMap.put("content", content);
		modelMap.put("flag", AboutType.ABOUT_JOINUS);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ABOUT_JOINUS,""));
		return "/about/about";
	}
	/**
	* @Title: toEmployee
	* @Description: 
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toEmployee")
	public String toEmployee(ModelMap modelMap) throws SQLException {
		
		String content = aboutService.getContent(AboutType.ABOUT_EMPLOYEE);
		modelMap.put("content", content);
		modelMap.put("flag", AboutType.ABOUT_EMPLOYEE);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ABOUT_EMPLOYEE,""));
		return "/about/about";
	}
	
	/**
	* @Title: toHelp
	* @Description: 
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toHelp")
	public String toHelp(ModelMap modelMap) throws SQLException {
		
		String content = aboutService.getContent(AboutType.ABOUT_HELP);
		modelMap.put("content", content);
		modelMap.put("flag", AboutType.ABOUT_HELP);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ABOUT_HELP,""));
		return "/about/about";
	}
}
