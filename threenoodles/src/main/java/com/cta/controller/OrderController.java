/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.OrderSubmitStatus;
import com.cta.constant.RestaurantStatus;
import com.cta.constant.SEOType;
import com.cta.constant.SessionParams;
import com.cta.dto.MenuDto;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.service.impl.CommonService;
import com.cta.service.impl.OrderService;
import com.cta.utils.DateUtils;

/**
 * @ClassName: OrderController
 * @Description: 订单控制器
 * @author chenwenpeng
 * @date 2013-6-24 下午11:05:15
 */
@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * @throws SQLException 
	 * @throws IOException 
	* @Title: toConfirmOrder
	* @Description: 转到订单确认
	* @param @param request
	* @param @param response
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toConfirmOrder")
	public String toConfirmOrder(
			HttpServletRequest request,
			HttpServletResponse response,
			ModelMap modelMap,
			@RequestParam(value = "flag", defaultValue="false") boolean flag) throws IOException, SQLException{
		HttpSession session = request.getSession();
		UserEntity user = (UserEntity) session.getAttribute(SessionParams.USER);
		RestaurantDto restaurant = (RestaurantDto) session.getAttribute(SessionParams.RESTAURANT);
		BuildingEntity building = (BuildingEntity) session.getAttribute(SessionParams.BUILDING);
		List<MenuDto> menus = (List<MenuDto>) session.getAttribute(SessionParams.ORDER_MENULIST);
		Double totalCount = (Double) session.getAttribute(SessionParams.ORDER_TOTALCOUNT);
		Double carriage = (Double) session.getAttribute(SessionParams.ORDER_CARRIAGE);
		if(null == user || null == restaurant || null == building || null == menus || null == totalCount || null == carriage){
			//请求失败，可能是用户的第二次提交,或者是用户直接输入地址跳转过来的
			 return "redirect:/";
		}
		//预定
		if(flag && RestaurantStatus.CLOSED_RESERVATION == restaurant.getStatus()){
			
			String begin_am = DateUtils.formatTime(restaurant.getOpen_time_am());
			String end_am = DateUtils.formatTime(restaurant.getClose_time_am());
			String begin_pm = DateUtils.formatTime(restaurant.getOpen_time_pm());
			String end_pm = DateUtils.formatTime(restaurant.getClose_time_pm());
			
			if(null == begin_am || null == end_am || null == begin_pm || null == end_pm){
				return "redirect:/";
			}
			modelMap.put("begin_am",begin_am);
			modelMap.put("end_am",end_am);
			modelMap.put("begin_pm",begin_pm);
			modelMap.put("end_pm",end_pm);
			
			modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_NEWS,null == building? SystemGlobals.getPreference("company.name") : building.getName()));
			return "/order/order_reserve_confirm";
		}
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ORDER_CONFIRM,null == building? SystemGlobals.getPreference("company.name") : building.getName()));
		return "/order/order_general_confirm";
	}
	
	/**
	 * @throws ParseException 
	 * @throws UnsupportedEncodingException 
	 * @throws SQLException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @Title: submitOrder
	* @Description: 提交订单
	* @param @param request
	* @param @param address
	* @param @param phone
	* @param @param remarks
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("submitOrder")
	public String submitOrder(HttpServletRequest request,
			ModelMap modelMap,
			@RequestParam(value = "address", defaultValue = "") String address,
			@RequestParam(value = "phone", defaultValue = "") String phone,
			@RequestParam(value = "remarks", defaultValue = "") String remarks,
			@RequestParam(value = "orderType", defaultValue = "1") Integer orderType,
			@RequestParam(value = "reserveTime", defaultValue = "") String reserveTime) throws Exception{
		
		String result = orderService.addOrder(request.getSession(),address,phone,remarks,orderType,reserveTime);
		modelMap.put("success", OrderSubmitStatus.SUCCEESS.equals(result) ? true : false);
		return "jsonView";
	}
	
	/**
	 * @throws SQLException 
	* @Title: toSuccess
	* @Description: 转到订单成功
	* @param @param request
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("toSuccess")
	public String toSuccess(HttpServletRequest request,ModelMap modelMap) throws SQLException{
		
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_ORDER_SUCCESS,""));
		return "order/order_success";
	}
	
	
	/**
	 * @throws ParseException 
	* @Title: checkReserve
	* @Description: 验证用户的订餐时间
	* @param @param modelMap
	* @param @param reserveTime
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("checkReserveTime")
	public String checkReserveTime(
			ModelMap modelMap,
			HttpServletRequest request,
			@RequestParam(value = "reserveTime", defaultValue="") String reserveTime) throws ParseException{
		
		String result = orderService.checkReserveTime(reserveTime,(RestaurantDto)request.getSession().getAttribute(SessionParams.RESTAURANT));
		modelMap.put("success", "success".equals(result) ? true : false);
		modelMap.put("message", "success".equals(result) ? "" : result);
		return "jsonView";
	}
}
