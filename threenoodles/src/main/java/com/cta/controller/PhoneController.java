/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.service.impl.PhoneService;

/**
 * @ClassName: PhoneController
 * @Description: 系统电话操作
 * @author chenwenpeng
 * @date 2013-6-29 下午8:49:05
 */
@Controller()
@RequestMapping("/phone")
public class PhoneController {
	@Autowired
	private PhoneService phoneService;
	/**
	 * @throws SQLException 
	* @Title: listUserPhone
	* @Description: 
	* @param @param modelMap
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("listUserPhone")
	public String listUserPhone(ModelMap modelMap,HttpServletRequest request) throws SQLException{
		
		List<Map<String,Object>> phoneList = phoneService.listUserPhone(request.getSession());
		modelMap.put("dataList",phoneList);
		return "jsonView";
	}
	
	/**
	* @Title: checkPhone
	* @Description: 电话
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("checkPhone")
	public String checkPhone(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "phone", defaultValue = "") String phone) throws SQLException{
		
		boolean result = phoneService.checkPhone(request.getSession(),phone);
		modelMap.put("data", result);
		return "jsonView";
	}
	
	/**
	 * @throws UnsupportedEncodingException 
	* @Title: sendPhoneCode
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param phone
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("sendPhoneCode")
	public String sendPhoneCode(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "phone", defaultValue = "") String phone) throws SQLException, UnsupportedEncodingException{
		
		boolean result = phoneService.sendPhoneCode(request.getSession(),phone);
		modelMap.put("data", result);
		return "jsonView";
	}
	/**
	* @Title: sendPhoneCode_resetpassword
	* @Description: 发送充值密码的验证码
	* @param @param modelMap
	* @param @param request
	* @param @param phone
	* @param @return
	* @param @throws SQLException
	* @param @throws UnsupportedEncodingException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("sendPhoneCode_resetpassword")
	public String sendPhoneCode_resetpassword(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "phone", defaultValue = "") String phone) throws SQLException, UnsupportedEncodingException{
		
		boolean result = phoneService.sendPhoneCode_resetpassword(request.getSession(),phone);
		modelMap.put("success", result);
		return "jsonView";
	}
	
	/**
	* @Title: checkPhoneCode_resetPassword
	* @Description: 验证用户的验证码
	* @param @param modelMap
	* @param @param request
	* @param @param code
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("checkPhoneCode_resetPassword")
	public String checkPhoneCode_resetPassword(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "code", defaultValue = "") String code) throws SQLException{
		
		boolean result = phoneService.checkPhoneCode_resetPassword(request.getSession(),code);
		modelMap.put("success", result);
		return "jsonView";
	}
	
	/**
	* @Title: checkPhoneCode
	* @Description: 验证手机验证码
	* @param @param modelMap
	* @param @param request
	* @param @param code
	* @param @return
	* @param @throws SQLException
	* @param @throws UnsupportedEncodingException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("checkPhoneCode")
	public String checkPhoneCode(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "code", defaultValue = "") String code,
			@RequestParam(value = "phone", defaultValue = "") String phone) throws SQLException, UnsupportedEncodingException{
		
		boolean result = phoneService.checkPhoneCode(request.getSession(),code,phone);
		modelMap.put("success", result);
		return "jsonView";
	}
}
