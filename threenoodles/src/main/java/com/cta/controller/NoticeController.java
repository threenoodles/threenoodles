/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.service.impl.NoticeService;

/**
 * @ClassName: NoticeController
 * @Description: 公告新闻 的控制器
 * @author chenwenpeng
 * @date 2013-5-26 下午4:09:41
 */
@Controller
@RequestMapping("/notice")
public class NoticeController {
	@Autowired
	private NoticeService noticeService;
	
	/**
	 * @throws SQLException 
	* @Title: listNotices
	* @Description: 列出所有的公告新闻
	* @param @param model
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("listNotice")
	public String listNotices(ModelMap model) throws SQLException{
		
		List<Map<String,Object>> orderList = noticeService.listOrders();
		model.put("orderList", orderList);
		return "jsonView";
	}
}
