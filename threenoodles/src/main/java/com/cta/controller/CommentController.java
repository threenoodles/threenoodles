/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.DataStatus;
import com.cta.constant.SEOType;
import com.cta.constant.SessionParams;
import com.cta.constant.TopNavis;
import com.cta.dto.CommentDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.CommentEntity;
import com.cta.entity.Reply_CommentEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ListPager;
import com.cta.service.impl.CommentService;
import com.cta.service.impl.CommonService;

/**
 * @ClassName: CommentController
 * @Description: 评论的控制器
 * @author chenwenpeng
 * @date 2013-6-11 上午11:35:32
 */
@Controller
@RequestMapping("/comment")
public class CommentController {
	@Autowired
	private CommentService commentService;
	@Autowired
	private CommonService commonService;
	
	/**
	* @Title: listRestComments
	* @Description: 列出店铺的评论信息
	* @param @return 
	* @return List<CommentEntity> 
	* @throws 
	*/ 
	@RequestMapping("/listRestComments")
	public String listRestComments(
		ModelMap modelMap,
		HttpServletRequest request,
		HttpServletResponse response,
		@RequestParam(value = "restId", defaultValue = "") Integer restId,
		@RequestParam(value = "start", defaultValue = "0") Integer start,
		@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws SQLException {
		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);
		
		commentService.listRestComments(pager,restId);
		modelMap.put("dataList", pager.getPageData());
		modelMap.put("totalCount", pager.getTotalRows());
		return "jsonView";
	}
	
	/**
	* @Title: listBuildingComments
	* @Description: 转到楼宇评价
	* @param @param modelMap
	* @param @param request
	* @param @param response
	* @param @param start
	* @param @param limit
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toBuildingComments")
	public String toBuildingComments(
			ModelMap modelMap,
			HttpServletRequest request) throws SQLException {
		ListPager pager = new ListPager();
		pager.setPageNo(0);
		pager.setRowsPerPage(5);
		
		List<CommentDto> comments = commentService.listBuildingComments(pager,request.getSession());
		modelMap.put("dataList", comments);
		modelMap.put("naviFlag", TopNavis.COMMENTS);
		
		BuildingEntity building = (BuildingEntity)request.getSession().getAttribute(SessionParams.BUILDING);
		modelMap.put("seo", commonService.getSEOInfo(SEOType.SEO_COMMENTS,null == building ? SystemGlobals.getPreference("company.name") : building.getName()));
		return "/comment/comments";
	}
	
	/**
	* @Title: listBuildingComments
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @param start
	* @param @param limit
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/listBuildingComments")
	public String listBuildingComments(
			ModelMap modelMap,
			HttpServletRequest request,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit) throws SQLException {
		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);
		
		List<CommentDto> comments = commentService.listBuildingComments(pager,request.getSession());
		modelMap.put("dataList", comments);
		return "jsonView";
	}
	
	
	/**
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @Title: submitComment
	* @Description: 提交评论
	* @param @param modelMap
	* @param @param order_id
	* @param @param taste
	* @param @param attitude
	* @param @param speed
	* @param @param content
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/submitComment")
	public String submitComment(
		ModelMap modelMap,HttpServletRequest request,
		@RequestParam(value = "order_id", defaultValue = "") Integer order_id,
		@RequestParam(value = "taste", defaultValue = "") Integer taste,
		@RequestParam(value = "attitude", defaultValue = "") Integer attitude,
		@RequestParam(value = "speed", defaultValue = "") Integer speed,
		@RequestParam(value = "content", defaultValue = "") String content) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		
		CommentEntity comment = new CommentEntity();
		comment.setOrder_id(order_id);
		comment.setTaste(taste);
		comment.setAttitude(attitude);
		comment.setSpeed(speed);
		comment.setContent(content);
		comment.setComment_time(new Date());
		comment.setStatus(DataStatus.ENABLED_GENERAL);
		
		List<String> messages = commentService.addComment(comment,request.getSession());
		modelMap.put("success", null == messages?true:false);
		modelMap.put("messages", messages);
		return "jsonView";
	}
	
	
	@RequestMapping("/submitReply")
	public String submitReply(
			ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "commentId", defaultValue = "") Integer commentId,
			@RequestParam(value = "content", defaultValue = "") String content,
			@RequestParam(value = "username", defaultValue = "") String username,
			@RequestParam(value = "password", defaultValue = "") String password) throws SQLException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException {
		
		Reply_CommentEntity reply = new Reply_CommentEntity();
		reply.setComment_id(commentId);
		reply.setContent(content);
		reply.setReply_time(new Date());
		reply.setStatus(DataStatus.ENABLED_GENERAL);
		
		String data = commentService.addReply(reply,username,password,request.getSession());
		modelMap.put("data", data);
		return "jsonView";
	}
	
	
	/**
	* @Title: getMenuPic
	* @Description: 
	* @param @param modelMap
	* @param @param menu_id
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/getMenuPic")
	public String getMenuPic(ModelMap modelMap,
			@RequestParam(value="menu_id", defaultValue="") Integer menu_id) throws SQLException {
		String pic_name = commentService.getMenuPic(menu_id);
		modelMap.put("data", pic_name);
		return "jsonView";
	}
}
