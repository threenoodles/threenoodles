/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.controller;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cta.constant.DataStatus;
import com.cta.constant.SessionParams;
import com.cta.entity.UserEntity;
import com.cta.service.impl.CommonService;

/**
 * @ClassName: CommonController
 * @Description: 
 * @author chenwenpeng
 * @date 2013-5-23 下午9:35:53
 */
@Controller
@RequestMapping("/common")
public class CommonController {

	@Autowired
	private CommonService commonService;
	
	/**
	 * @throws UnsupportedEncodingException 
	* @Title: toSearchResult
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/toSearchResult")
	public String toSearchResult(ModelMap modelMap,HttpServletRequest request,
			@RequestParam(value = "menu_name", defaultValue = "") String menu_name) throws SQLException, UnsupportedEncodingException {
		//检索菜单包含的店铺id
		List<String> restaurants = new LinkedList<String>();
		Map<String, List<Map<String,Object>>> menus = commonService.listSearchResult(menu_name,request.getSession(),restaurants);
		modelMap.put("menu_name", menu_name);
		modelMap.put("restaurants", restaurants);
		modelMap.put("menus", menus);
		return "/common/searchMenu";
	}
	
	
	/**
	* @Title: testLogin
	* @Description: 验证用户是否登录
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException
	* @param @throws UnsupportedEncodingException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/testLogin")
	public String testLogin(ModelMap modelMap,HttpServletRequest request) throws SQLException, UnsupportedEncodingException {
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionParams.USER);
		if(null == user || null == user.getId()){
			modelMap.put("success", DataStatus.NO_LOGIN);
		}else{
			modelMap.put("success", DataStatus.SUCCESS);
		}
		return "jsonView";
	}
	

	/**
	* @Title: listMyMessage
	* @Description: 列出我的未读回复（评论回复和反馈回复）
	* @param @param modelMap
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/listMyMessage")
	public String listMyMessage(ModelMap modelMap,HttpServletRequest request) throws SQLException{
		Map<String,Object> user_info = commonService.listMyMessage(request.getSession());
		modelMap.put("data", user_info);
		return "jsonView";
	}
	
	/**
	* @Title: getFavoriteRest
	* @Description: 
	* @param @param modelMap
	* @param @param request
	* @param @return
	* @param @throws SQLException 
	* @return String 
	* @throws 
	*/ 
	@RequestMapping("/getFavoriteRest")
	public String getFavoriteRest(ModelMap modelMap,HttpServletRequest request) throws SQLException{
		List<Map<String,Object>> rests = commonService.getFavoriteRest(request.getSession());
		modelMap.put("dataList", rests);
		return "jsonView";
	}
}
