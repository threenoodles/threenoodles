/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import com.cta.entity.MenuEntity;

/**
 * @ClassName: MenuDto
 * @Description:
 * @author chenwenpeng
 * @date 2013-6-21 下午11:29:42
 */
public class MenuDto extends MenuEntity {
	/** The long serialVersionUID */
	private static final long serialVersionUID = 4511753438854337784L;
	private Integer num;// 菜单数量
	private String isPackage;// 打包
	private String taste;// 口味
	private String message;// 留言

	public String getIsPackage() {
		return isPackage;
	}

	public void setIsPackage(String isPackage) {
		this.isPackage = isPackage;
	}

	public String getTaste() {
		return taste;
	}

	public void setTaste(String taste) {
		this.taste = taste;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
}
