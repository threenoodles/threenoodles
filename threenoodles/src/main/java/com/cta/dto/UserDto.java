/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import com.cta.entity.UserEntity;
import com.cta.platform.persistence.annotation.Column;
import com.cta.platform.persistence.annotation.NotEmpty;

/**
 * @ClassName: UserDto
 * @Description: 
 * @author chenwenpeng
 * @date 2013-7-21 上午8:01:57
 */
public class UserDto extends UserEntity{
	
	@Column(name="gender")
	@NotEmpty()
	private Integer gender;
	@Column(name="icon")
	@NotEmpty()
	private String icon;
	@Column(name="email")
	@NotEmpty()
	private String email;
	@Column(name="qq")
	@NotEmpty()
	private String qq;
	
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
}
