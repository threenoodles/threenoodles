/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import java.util.List;
import java.util.Map;

import com.cta.entity.CommentEntity;
import com.cta.entity.OrderEntity;
import com.cta.platform.persistence.annotation.Column;

/**
 * @ClassName: OrderDto
 * @Description: 
 * @author chenwenpeng
 * @date 2013-7-23 下午11:17:36
 */
public class OrderDto extends OrderEntity{

	@Column(name="rest_name")//订单对应的店铺名字
	private String rest_name;
	@Column(name="recommend_count")//订单对应的店铺的评论次数
	private String recommend_count;
	
	@Column(name="rest_telephone")//店铺的收餐电话
	private String rest_telephone;
	private List<Map<String,Object>> menus;//订单对应的菜单
	private CommentEntity comment;//订单的评论

	
	
	public String getRecommend_count() {
		return recommend_count;
	}

	public void setRecommend_count(String recommend_count) {
		this.recommend_count = recommend_count;
	}

	public CommentEntity getComment() {
		return comment;
	}

	public void setComment(CommentEntity comment) {
		this.comment = comment;
	}

	public String getRest_name() {
		return rest_name;
	}

	public void setRest_name(String rest_name) {
		this.rest_name = rest_name;
	}

	public List<Map<String, Object>> getMenus() {
		return menus;
	}

	public void setMenus(List<Map<String, Object>> menus) {
		this.menus = menus;
	}

	public String getRest_telephone() {
		return rest_telephone;
	}

	public void setRest_telephone(String rest_telephone) {
		this.rest_telephone = rest_telephone;
	}
}
