/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import java.util.List;
import java.util.Map;

import com.cta.entity.CommentEntity;
import com.cta.platform.persistence.annotation.Column;

/**
 * @ClassName: CommentDto
 * @Description: 评论的dto
 * @author chenwenpeng
 * @date 2013-6-11 下午3:21:05
 */
public class CommentDto extends CommentEntity{

	/** The long serialVersionUID*/
	private static final long serialVersionUID = 660096035127152449L;
	
	@Column(name="rest_name")
	private String rest_name;//评论的店铺名称
	
	@Column(name="user_name")
	private String user_name;//评论人的名字
	private List<Reply_CommentDto> replyList;//评论对应的回复
	private List<Map<String,Object>> replyList_s;//评论对应的回复（简单信息：replier_name,reply_content）
	private Integer score_avg;//本条评论的平均得分

	private List<Map<String,Object>> menus;//评论的菜单详情
	
	@Column(name="rest_id")//店铺的ID
	private Integer rest_id;
	@Column(name="recommend_count")//店铺的评论总数
	private Integer recommend_count;
	@Column(name="rest_pic")//店铺的图片
	private String rest_pic;
	@Column(name="alias")//店铺的别名
	private String alias;
	

	
	public Integer getRest_id() {
		return rest_id;
	}
	public void setRest_id(Integer rest_id) {
		this.rest_id = rest_id;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getRest_pic() {
		return rest_pic;
	}
	public void setRest_pic(String rest_pic) {
		this.rest_pic = rest_pic;
	}
	public List<Map<String, Object>> getMenus() {
		return menus;
	}
	public void setMenus(List<Map<String, Object>> menus) {
		this.menus = menus;
	}
	public String getRest_name() {
		return rest_name;
	}
	public void setRest_name(String rest_name) {
		this.rest_name = rest_name;
	}
	public List<Map<String, Object>> getReplyList_s() {
		return replyList_s;
	}
	public void setReplyList_s(List<Map<String, Object>> replyList_s) {
		this.replyList_s = replyList_s;
	}
	public Integer getScore_avg() {
		return score_avg;
	}
	public void setScore_avg(Integer score_avg) {
		this.score_avg = score_avg;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public List<Reply_CommentDto> getReplyList() {
		return replyList;
	}
	public void setReplyList(List<Reply_CommentDto> replyList) {
		this.replyList = replyList;
	}
	public Integer getRecommend_count() {
		return recommend_count;
	}
	public void setRecommend_count(Integer recommend_count) {
		this.recommend_count = recommend_count;
	}
	
}
