/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import java.util.List;
import java.util.Map;

import com.cta.entity.FeedbackEntity;
import com.cta.platform.persistence.annotation.Column;

/**
 * @ClassName: Reply_FeedbackDto
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-2 上午10:15:02
 */
public class FeedbackDto extends FeedbackEntity{

	@Column(name="responder_name")
	private String responder_name;//反馈者的名字
	
	private List<Map<String,Object>> replyList_s;//回复的列表

	public String getResponder_name() {
		return responder_name;
	}

	public void setResponder_name(String responder_name) {
		this.responder_name = responder_name;
	}

	public List<Map<String, Object>> getReplyList_s() {
		return replyList_s;
	}

	public void setReplyList_s(List<Map<String, Object>> replyList_s) {
		this.replyList_s = replyList_s;
	}
}
