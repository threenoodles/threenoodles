/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import com.cta.entity.Reply_CommentEntity;
import com.cta.platform.persistence.annotation.Column;

/**
 * @ClassName: Reply_CommentDto
 * @Description: 评论的dto
 * @author chenwenpeng
 * @date 2013-6-11 下午5:07:49
 */

public class Reply_CommentDto extends Reply_CommentEntity{
	/** The long serialVersionUID*/
	private static final long serialVersionUID = 580372550827731767L;
	
	@Column(name="replier_name")
	private String replier_name;//回复者的name

	public String getReplier_name() {
		return replier_name;
	}

	public void setReplier_name(String replier_name) {
		this.replier_name = replier_name;
	}
}
