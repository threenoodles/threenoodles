/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dto;

import java.util.Date;
import java.util.List;

import com.cta.entity.MenuEntity;
import com.cta.entity.RestaurantEntity;
import com.cta.platform.persistence.annotation.Column;

/**
 * @ClassName: RestaurantDto
 * @Description: 
 * @author chenwenpeng
 * @date 2013-6-7 下午10:45:40
 */
public class RestaurantDto extends RestaurantEntity{
	/** The long serialVersionUID*/
	private static final long serialVersionUID = 835591198073053L;
	@Column(name="deli_interval")
	private Integer deli_interval;
	//
	@Column(name="food_count")
	private Integer food_count;
	//
	@Column(name="order_count")
	private Integer order_count;
	//
	@Column(name="recommend_count")
	private Integer recommend_count;
	//
	@Column(name="taste")
	private Double taste;
	//
	@Column(name="speed")
	private Double speed;
	//
	@Column(name="attitude")
	private Double attitude;
	
	@Column(name="open_time_am")
	private Date open_time_am;
	
	@Column(name="close_time_am")
	private Date close_time_am; 
	
	@Column(name="open_time_pm")
	private Date open_time_pm;
	
	@Column(name="close_time_pm")
	private Date close_time_pm;
	
	private Integer score_avg = 3;//店铺平均得分
	private List<MenuEntity> menus_recommend;//店长推荐
	
	
	
	public Date getOpen_time_am() {
		return open_time_am;
	}
	public void setOpen_time_am(Date open_time_am) {
		this.open_time_am = open_time_am;
	}
	public Date getOpen_time_pm() {
		return open_time_pm;
	}
	public void setOpen_time_pm(Date open_time_pm) {
		this.open_time_pm = open_time_pm;
	}
	public Date getClose_time_am() {
		return close_time_am;
	}
	public void setClose_time_am(Date close_time_am) {
		this.close_time_am = close_time_am;
	}
	public Date getClose_time_pm() {
		return close_time_pm;
	}
	public void setClose_time_pm(Date close_time_pm) {
		this.close_time_pm = close_time_pm;
	}
	public Integer getDeli_interval() {
		return deli_interval;
	}
	public void setDeli_interval(Integer deli_interval) {
		this.deli_interval = deli_interval;
	}
	public Integer getFood_count() {
		return food_count;
	}
	public void setFood_count(Integer food_count) {
		this.food_count = food_count;
	}
	public Integer getOrder_count() {
		return order_count;
	}
	public void setOrder_count(Integer order_count) {
		this.order_count = order_count;
	}
	public Integer getRecommend_count() {
		return recommend_count;
	}
	public void setRecommend_count(Integer recommend_count) {
		this.recommend_count = recommend_count;
	}
	public Double getTaste() {
		return taste;
	}
	public void setTaste(Double taste) {
		this.taste = taste;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Double getAttitude() {
		return attitude;
	}
	public void setAttitude(Double attitude) {
		this.attitude = attitude;
	}
	public Integer getScore_avg() {
		return score_avg;
	}
	public void setScore_avg(Integer score_avg) {
		this.score_avg = score_avg;
	}
	public List<MenuEntity> getMenus_recommend() {
		return menus_recommend;
	}
	public void setMenus_recommend(List<MenuEntity> menus_recommend) {
		this.menus_recommend = menus_recommend;
	}
}
