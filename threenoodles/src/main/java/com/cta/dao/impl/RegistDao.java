/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.entity.UserEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: RegistDao
 * @Description: 注册
 * @author chenwenpeng
 * @date 2013-7-21 下午3:04:12
 */
@Repository
public class RegistDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: checkUsername
	* @Description: 
	* @param @param username
	* @param @return 
	* @return UserEntity 
	* @throws 
	*/ 
	public Map<String,Object> checkUsername(String username) throws SQLException {
		
		String sql = "select id from mf_user where nick_name = ?";
		return this.executeQueryOne(sql, new String[]{username});
	}

	/**
	 * @throws SQLException 
	* @Title: insertUser
	* @Description: 插入一个用户
	* @param @param user
	* @param @return 
	* @return Integer 
	* @throws 
	*/ 
	public Integer insertUser(UserEntity user) throws SQLException {
		
		return this.insert_pk(user);
	}

	/**
	 * @throws SQLException 
	* @Title: insertUserDetail
	* @Description: 插入用户详情
	* @param @param user_id
	* @param @param qq 
	* @return void 
	* @throws 
	*/ 
	public void insertUserDetail(Integer user_id) throws SQLException {
		
		String sql = "insert into mf_user_detail(user_id) values(?)";
		this.executeUpdate(sql,new Object[]{user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: insertUserTelephone
	* @Description: 向用户收餐电话表添加一条待验证数据
	* @param @param user_id
	* @param @param phone 
	* @return void 
	* @throws 
	*/ 
	public void insertUserTelephone(Integer user_id, String phone) throws SQLException {
		String sql = "insert into mf_user_telephone values(null,?,?,?)";
		this.executeUpdate(sql, new Object[]{user_id,phone,DataStatus.ENABLED_GENERAL});
	}

	/**
	* @Title: checkPhone
	* @Description: 
	* @param @param phone
	* @param @return
	* @param @throws SQLException 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> checkPhone(String phone) throws SQLException {
		
		String sql = "select id from mf_user where telephone = ?";
		return this.executeQueryOne(sql, new String[]{phone});
	}
}
