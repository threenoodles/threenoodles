/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl.android;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BuildingEntity;
import com.cta.entity.OrderEntity;
import com.cta.entity.UserEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;
import com.cta.utils.DateUtils;

/**
 * @ClassName: OrderDao_
 * @Description:
 * @author chenwenpeng
 * @date 2013-9-1 下午2:08:46
 */
@Repository
public class OrderDao_ extends BaseDaoImpl {

	/**
	 * @Title: getOrder_today
	 * @Description:
	 * @param @param userId
	 * @param @return
	 * @param @throws SQLException
	 * @return List<Map<String,Object>>
	 * @throws
	 */
	public List<Map<String, Object>> getOrder_today(Integer userId)
			throws SQLException {

		String sql = "select o.id order_id,o.user_id,o.rest_id,o.phone user_phone,o.address,o.remark,o.carriage,o.totalPrice,date_format(o.ordered_time,'%Y-%m-%d %H:%k:%i') ordered_time,o.status,r.name rest_name,r.telephone rest_phone from mf_order o left join mf_restaurant r on o.rest_id = r.id where o.user_id = ? and o.status <> ? and o.ordered_time>='"
				+ DateUtils.formatDateTime(new Date(), "yyyy-MM-dd 00:00:00")
				+ "'";
		return this.executeQuery(sql, new Integer[] { userId,
				DataStatus.UNENABLE_GENERAL });
	}

	/**
	 * @throws SQLException
	 * @Title: listOrderMenus
	 * @Description:
	 * @param @param order_id
	 * @param @return
	 * @return List<Map<String,Object>>
	 * @throws
	 */
	public List<Map<String, Object>> listOrderMenus(Integer order_id)
			throws SQLException {
		String sql = "select menu_id,name menu_name,count menu_count,price menu_price from mf_order_menu where order_id = ?";
		return this.executeQuery(sql, new Integer[] { order_id });
	}

	/**
	 * @Title: addOrder
	 * @Description:
	 * @param @param order
	 * @param @return
	 * @param @throws SQLException
	 * @return Integer
	 * @throws
	 */
	public Integer addOrder(OrderEntity order) throws SQLException {

		return this.insert_pk(order);
	}

	/**
	 * @throws SQLException
	 * @Title: getRestaurantDetail
	 * @Description:
	 * @param @param rest_id
	 * @param @return
	 * @return RestaurantDto
	 * @throws
	 */
	public RestaurantDto getRestaurantDetail(Integer rest_id)
			throws SQLException {

		String sql = "select r.id,r.name,r.alias,r.pic,r.address,r.notice,r.deli_price,d.deli_interval,r.telephone,r.carriage,d.food_count,d.order_count,d.recommend_count,d.taste,d.speed,d.attitude,r.deli_type,r.send_type,r.status from mf_restaurant r,mf_rest_detail d where r.id = ? and r.id = d.rest_id";
		return this.selectOne(sql, new Object[] { rest_id },
				RestaurantDto.class);
	}

	/**
	 * @throws SQLException
	 * @Title: getBuildingDetail
	 * @Description:
	 * @param @param building_id
	 * @param @return
	 * @return BuildingEntity
	 * @throws
	 */
	public BuildingEntity getBuildingDetail(Integer building_id)
			throws SQLException {
		String sql = "select id,name from mf_building where id = ?";
		return this.selectOne(sql, new Object[] { building_id },
				BuildingEntity.class);
	}

	/**
	 * @throws SQLException
	 * @Title: updateOrderStatus
	 * @Description: 更新订单状态
	 * @param @param id
	 * @param @param faild
	 * @return void
	 * @throws
	 */
	public void updateOrderStatus(int orderId, int status) throws SQLException {
		String sql = "update mf_order set status = ? where id = ?";
		this.executeUpdate(sql, new Integer[] { status, orderId });
	}

	/**
	 * @param score
	 * @throws SQLException
	 * @Title: updateUserMsg
	 * @Description: 更新用户信息
	 * @param @param user_id
	 * @return void
	 * @throws
	 */
	public void updateUserMsg(Integer user_id, Double score)
			throws SQLException {
		String sql = "update mf_user set order_count=(order_count+1),gold=(gold+"
				+ score + ") where id = ?";
		this.executeUpdate(sql, new Integer[] { user_id });
	}

	/**
	 * @throws SQLException
	 * @Title: updateRestMsg
	 * @Description:
	 * @param @param rest_id
	 * @return void
	 * @throws
	 */
	public void updateRestMsg(Integer rest_id) throws SQLException {
		String sql = "update mf_rest_detail set order_count=(order_count+1) where rest_id = ?";
		this.executeUpdate(sql, new Integer[] { rest_id });
	}

	/**
	 * @throws SQLException
	 * @Title: updateBuildingMsg
	 * @Description:
	 * @param @param building_id
	 * @return void
	 * @throws
	 */
	public void updateBuildingMsg(Integer building_id) throws SQLException {

		String sql = "update mf_building set order_count=(order_count+1) where id = ?";
		this.executeUpdate(sql, new Integer[] { building_id });
	}

	/**
	 * @Title: updateUserAddressMsg
	 * @Description: 更新用户的地址信息
	 * @param @param user_id
	 * @param @param building_id
	 * @param @param address
	 * @param @throws SQLException
	 * @return void
	 * @throws
	 */
	public void updateUserAddressMsg(Integer user_id, Integer building_id,
			String address) throws SQLException {

		String sql = "insert into mf_address values(null,?,?,?,?)";
		this.executeUpdate(sql,
				new Object[] { user_id, building_id, address, 1 });
	}

	/**
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getOrderByTimeIntervalAndUserId(
			Integer userId, Date startTime, Date endTime) throws SQLException {

		String sql = "select o.id order_id,o.user_id,o.rest_id,o.phone user_phone,o.address,o.remark,o.carriage,o.totalPrice,date_format(o.ordered_time,'%Y-%m-%d %H:%i:%s') ordered_time,o.status,r.name rest_name,r.telephone rest_phone ,o.secret secret from mf_order o left join mf_restaurant r on o.rest_id = r.id where o.user_id = ? and o.status <> ? and o.ordered_time>='"
				+ DateUtils.formatDateTime(startTime, "yyyy-MM-dd HH:mm:ss")
				+ "' and o.ordered_time<='"
				+ DateUtils.formatDateTime(endTime, "yyyy-MM-dd HH:mm:ss")
				+ "' order by o.ordered_time desc";
		return this.executeQuery(sql, new Integer[] { userId,
				DataStatus.UNENABLE_GENERAL });
	}

	public UserEntity getUserByOrderId(Integer userId) throws SQLException {

		String sql = "select * from mf_user where id = " + userId;

		return this.selectOne(sql, UserEntity.class);
	}
}
