/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.OrderStatus;
import com.cta.dto.CommentDto;
import com.cta.dto.Reply_CommentDto;
import com.cta.entity.CommentEntity;
import com.cta.entity.Reply_CommentEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;
import com.cta.platform.util.ListPager;

/**
 * @ClassName: CommentDao
 * @Description: 评论的dao
 * @author chenwenpeng
 * @date 2013-6-11 下午3:04:23
 */
@Repository
public class CommentDao extends BaseDaoImpl{

	/**
	* @Title: listRestComments
	* @Description: 获取店铺的评论信息
	* @param @param pager
	* @param @param restId
	* @param @return
	* @param @throws SQLException 
	* @return List<CommentDto> 
	* @throws 
	*/ 
	public List<CommentDto> listRestComments(ListPager pager,Integer restId) throws SQLException {
		String sql = "select c.id,c.content,c.user_id,c.taste,c.speed,c.attitude,c.comment_time,u.nick_name as user_name from mf_comment c,mf_user u where c.user_id = u.id and c.rest_id = ? and c.status = 1 order by c.comment_time desc";
		return this.selectPage(sql, new Integer[]{restId},CommentDto.class, pager);
	}

	/**
	 * @throws SQLException 
	* @Title: listReplyByCommentId
	* @Description: 根据评论的id 获取所有的回复信息
	* @param @param commetId
	* @param @return 
	* @return List<Reply_CommentEntity> 
	* @throws 
	*/ 
	public List<Reply_CommentDto> listReplyByCommentId(Integer commetId) throws SQLException {
		String sql = "select r.id,r.content,r.reply_time,u.nick_name replier_name from mf_reply_comment r ,mf_user u where r.replier_id = u.id and r.comment_id = ? and status <> 3";
		return this.selectAll(sql,new Integer[]{commetId},Reply_CommentDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getRestIdByOrderId
	* @Description: 根据订单的id 获取订单所属店铺的id
	* @param @param order_id
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public Integer getRestIdByOrderId(Integer order_id) throws SQLException {
		
		String sql = "select rest_id from mf_order where id = ? and status <> ?";
		List<Integer> ids = this.executeQueryForInt(sql, new Integer[]{order_id,OrderStatus.COMMENT});
		return ids.size()>0? ids.get(0) : null;
	}

	/**
	 * @throws SQLException 
	* @Title: addComment
	* @Description: 
	* @param @param comment 
	* @return void 
	* @throws 
	*/ 
	public void addComment(CommentEntity comment) throws SQLException {
		
		this.insert(comment);
	}

	
	/**
	 * @throws SQLException 
	* @Title: listBuildingComment
	* @Description: 
	* @param @param pager
	* @param @param buildingId
	* @param @return 
	* @return List<CommentDto> 
	* @throws 
	*/ 
	public List<CommentDto> listBuildingComment(ListPager pager,Integer buildingId) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("select c.id,c.order_id,c.rest_id,c.content,c.taste,c.speed,c.attitude,c.user_id,u.nick_name user_name,c.status,c.comment_time,r.name rest_name,rd.recommend_count,r.pic rest_pic,r.alias from mf_comment c");
		sql.append(" left join mf_restaurant r on c.rest_id = r.id");
		sql.append(" left join mf_rest_detail rd on r.id = rd.rest_id");
		sql.append(" left join mf_order o on o.id = c.order_id");
		sql.append(" left join mf_user u on o.user_id = u.id");
		sql.append(" where o.building_id = ?");
		
		sql.append(" and c.status = 1");
		sql.append(" order by c.id desc");
		
		return this.selectPage(sql.toString(), new Integer[]{buildingId}, CommentDto.class, pager);
	}

	/**
	 * @throws SQLException 
	* @Title: ListCommentReply_s
	* @Description: 获取评论的回复信息
	* @param @param id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> ListCommentReply_s(Integer comment_id) throws SQLException {
		
		String sql = "select rc.content,rc.reply_time,u.nick_name from mf_reply_comment rc left join mf_user u on rc.replier_id = u.id where rc.comment_id = ?";
		return this.executeQuery(sql, new Integer[]{comment_id});
	}

	/**
	* @Title: getMenuPic
	* @Description: 获取菜单图片和类型
	* @param @param menu_id
	* @param @return
	* @param @throws SQLException 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getMenuPic(Integer menu_id) throws SQLException {
		String sql = "select pic,recommend from mf_menu_qd where id = ?";
		return this.executeQueryOne(sql, new Integer[]{menu_id});
	}

	/**
	 * @throws SQLException 
	* @Title: getComment_s
	* @Description: 
	* @param @param comment_id
	* @param @return 
	* @return CommentEntity 
	* @throws 
	*/ 
	public CommentEntity getComment_s(Integer comment_id) throws SQLException {
		String sql = "select user_id from mf_comment where id = ?";
		
		return this.selectOne(sql, new Integer[]{comment_id},CommentEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: addReply
	* @Description: 添加回复
	* @param @param reply 
	* @return void 
	* @throws 
	*/ 
	public void addReply(Reply_CommentEntity reply) throws SQLException {
		
		this.insert(reply);
	}
}
