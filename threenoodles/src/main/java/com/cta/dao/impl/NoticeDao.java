/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import org.springframework.stereotype.Repository;

import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: NoticeDao
 * @Description: 公告dao
 * @author chenwenpeng
 * @date 2013-5-26 下午4:12:01
 */
@Repository
public class NoticeDao extends BaseDaoImpl{

	
}
