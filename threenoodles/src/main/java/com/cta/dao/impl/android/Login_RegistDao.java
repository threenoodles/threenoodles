/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl.android;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: Login_RegistDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-9-1 上午11:02:29
 */
@Repository
public class Login_RegistDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: login
	* @Description: 
	* @param @param username
	* @param @return 
	* @return HashMap<String,String> 
	* @throws 
	*/ 
	public Map<String, Object> login(String username,String password) throws SQLException {
		String sql = "select id,nick_name name,password from mf_user where nick_name = ? and password = ?";
		return this.executeQueryOne(sql, new String[]{username,password});
	}

	/**
	 * @throws SQLException 
	* @Title: checkUserName
	* @Description: 
	* @param @param username
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> checkUserName(String username) throws SQLException {
		String sql = "select id from mf_user where nick_name = ? limit 1";
		return this.executeQueryOne(sql, new String[]{username});
	}

	/**
	 * @throws SQLException 
	* @Title: checkPhone
	* @Description: 
	* @param @param phone
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> checkPhone(String phone) throws SQLException {
		
		String sql = "select id from mf_user where telephone = ? limit 1";
		return this.executeQueryOne(sql, new String[]{phone});
	}
}
