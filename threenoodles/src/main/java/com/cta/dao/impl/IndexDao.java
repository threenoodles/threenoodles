/**
* @FileName: IndexDao.java
* @Package com.cta.dao
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-18 下午04:11:09
* @version V1.0
*/
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cta.entity.BuildingEntity;
import com.cta.entity.RegionEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: IndexDao
 * @Description: 首页dao层
 * @author chenwenpeng
 * @date 2013-5-18 下午04:11:09
 *
 */
@Repository
public class IndexDao extends BaseDaoImpl{

	/**
	 * @throws SQLException
	* @Title: listBuildings
	* @Description: 列出所有楼宇
	* @param @param sql
	* @param @return
	* @return List<BuildingEntity>
	* @throws
	*/
	public List<BuildingEntity> listBuildings(String sql) throws SQLException {

		return this.selectAll(sql, BuildingEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listRegions
	* @Description: 列出所有区域
	* @param @param sql
	* @param @return 
	* @return List<RegionEntity> 
	* @throws 
	*/ 
	public List<RegionEntity> listRegions(String sql) throws SQLException {
		return this.selectAll(sql, RegionEntity.class);
	}
}
