/**
 *
 */
package com.cta.dao.impl.android;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.constant.MenuType;
import com.cta.constant.RestaurantStatus;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @author Administrator
 *
 */
@Repository
public class RestaurantDao_ extends BaseDaoImpl{

	/**
	 * @param buildingId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getRestaurants(Integer buildingId) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("select r.id,r.name,r.notice,r.deli_price deliveryValue,r.status isClosed,r.sort,round((rd.taste+rd.attitude)/2) score,rd.deli_interval deliveryTime");
		sql.append(" from mf_restaurant r left join mf_rest_detail rd on r.id = rd.rest_id where r.id in");
		sql.append(" (select rest_id from mf_build_restaurant where build_id = ?) and r.status in (?,?)");

		return this.executeQuery(sql.toString(), new Integer[]{buildingId,RestaurantStatus.OPENED,RestaurantStatus.CLOSED});
	}

	/**
	 * @param restId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getRecommendMenuList(
			Integer restId) throws SQLException {

		String sql = "select id,name,price from mf_menu_qd where recommend = ? and rest_id = ? and status = ?";

		return this.executeQuery(sql, new Integer[]{MenuType.RECOMMENDED,restId,DataStatus.ENABLED_GENERAL});
	}

	/**
	 * @param restId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String,Object>> getCategoryList(Integer restId) throws SQLException {

		String sql = "select id,name from mf_cate_menu where id in (select distinct cate_id from mf_menu_qd where rest_id = ?)";
		return this.executeQuery(sql, new Integer[]{restId});
	}


	/**
	 * @param restId
	 * @param buildingId
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> getRestCarriage(Integer restId,
			Integer buildingId) throws SQLException {
		String sql = "select carriage from mf_rest_carriage_building where rest_id = ? and building_id = ? limit 1";

		return this.executeQueryOne(sql, new Integer[]{restId,buildingId});
	}

	/**
	 * @param restId
	 * @return
	 * @throws SQLException 
	 */
	public Map<String, Object> getRestInfo(Integer restId) throws SQLException {

		String sql = "select deli_price deliPrice,status isClosed,carriage from mf_restaurant where id = ? and status in(?,?)";
		return this.executeQueryOne(sql, new Integer[]{restId,RestaurantStatus.OPENED,RestaurantStatus.CLOSED});
	}

	/**
	 * @throws SQLException 
	* @Title: getCategoyr_menuList
	* @Description: 
	* @param @param restId
	* @param @param cateId
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public List<Map<String, Object>> getCategoyr_menuList(Integer restId,
			Integer cateId) throws SQLException {
		String sql = "select id,name,price from mf_menu_qd where rest_id = ? and cate_id = ? and status = ?";
		
		return this.executeQuery(sql, new Integer[]{restId,cateId,DataStatus.ENABLED_GENERAL});
	}
}
