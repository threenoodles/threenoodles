/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.constant.ReplyStatus;
import com.cta.dto.CommentDto;
import com.cta.dto.FeedbackDto;
import com.cta.dto.OrderDto;
import com.cta.entity.CommentEntity;
import com.cta.entity.FeedbackEntity;
import com.cta.entity.Reply_CommentEntity;
import com.cta.entity.Reply_FeedbackEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;
import com.cta.platform.util.ListPager;
import com.cta.utils.DateUtils;

/**
 * @ClassName: MycenterDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-7-23 下午10:57:02
 */
@Repository
public class MycenterDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listOrder_today
	* @Description: 
	* @param @param flag
	* @param @param id
	* @param @return 
	* @return List<OrderDto> 
	* @throws 
	*/ 
	public List<OrderDto> listOrder_today(Integer id) throws SQLException {
		String sql = "select o.id,o.user_id,o.rest_id,o.phone,o.address,o.remark,o.carriage,o.totalPrice,o.ordered_time,o.status,r.name rest_name,r.telephone rest_telephone from mf_order o left join mf_restaurant r on o.rest_id = r.id where o.user_id = ? and o.status <> -1 and o.ordered_time>='"+DateUtils.formatDateTime(new Date(), "yyyy-MM-dd 00:00:00")+"'";
		return this.selectAll(sql,new Integer[]{id},OrderDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listOrderMenus
	* @Description: 
	* @param @param order_id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listOrderMenus(Integer order_id) throws SQLException {
		String sql = "select menu_id,name,count,price from mf_order_menu where order_id = ?";
		return this.executeQuery(sql, new Integer[]{order_id});
	}

	/**
	 * @throws SQLException 
	* @Title: getUserDetail
	* @Description:
	* @param @param user_id
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getUserDetail(Integer user_id) throws SQLException {
		String sql = "select order_count,un_comment,gold from mf_user where id = ?";
		return this.executeQueryOne(sql, new Integer[]{user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: getOrderComment
	* @Description: 
	* @param @param order_id
	* @param @return 
	* @return CommentEntity 
	* @throws 
	*/ 
	public CommentEntity getOrderComment(Integer order_id) throws SQLException {
		
		String sql = "select content,taste,speed,attitude from mf_comment where order_id= ? and status = ? order by id desc limit 1";
		return this.selectOne(sql, new Integer[]{order_id,DataStatus.ENABLED_GENERAL},CommentEntity.class);
	}
	/**
	 * @throws SQLException 
	 * @param pager 
	* @Title: listOrder_history
	* @Description: 
	* @param @param id
	* @param @return 
	* @return List<OrderDto> 
	* @throws 
	*/ 
	public List<OrderDto> listOrder_history(Integer id) throws SQLException {
		
		String sql = "select o.id,o.user_id,o.rest_id,o.phone,o.address,o.remark,o.carriage,o.totalPrice,o.ordered_time,o.status,r.name rest_name,r.telephone rest_telephone from mf_order o left join mf_restaurant r on o.rest_id = r.id where o.user_id = ? and o.status <> -1 order by id desc limit 5";
		return this.selectPage(sql, new Integer[]{id}, OrderDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listOrder_history
	* @Description: 列出我的更多历史订单
	* @param @param id
	* @param @param pager
	* @param @return 
	* @return List<OrderDto> 
	* @throws 
	*/ 
	public List<OrderDto> listOrder_history(Integer id, ListPager pager) throws SQLException {
		
		String sql = "select o.id,o.user_id,o.rest_id,o.phone,o.address,o.remark,o.carriage,o.totalPrice,o.ordered_time,o.status,r.name rest_name,r.telephone rest_telephone from mf_order o left join mf_restaurant r on o.rest_id = r.id where o.user_id = ? and o.status <> -1 order by id desc";
		return this.selectPage(sql, new Integer[]{id}, OrderDto.class,pager);
	}

	/**
	 * @throws SQLException 
	* @Title: listComments
	* @Description: 获取 有回复 或没有回复的评论信息
	* @param @param user_id
	* @param @return 
	* @return List<CommentDto> 
	* @throws 
	*/ 
	public List<CommentDto> listComments(Integer status,Integer user_id,ListPager pager) throws SQLException {
		
		String sql = "select c.id,c.comment_time,c.content,r.name rest_name ,u.nick_name user_name from mf_comment c left join mf_restaurant r on c.rest_id = r.id left join mf_user u on c.user_id = u.id where c.id in (select comment_id from mf_reply_comment where status = ? and commenter_id = ?) and c.status = ? order by id desc";
		return this.selectPage(sql, new Integer[]{status,user_id,DataStatus.ENABLED_GENERAL}, CommentDto.class,pager);
	}

	/**
	 * @throws SQLException 
	* @Title: listReplies
	* @Description: 获取评论的回复
	* @param @param id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listReplies(Integer comment_id) throws SQLException {
		
		String sql = "select rc.content reply_content,u.nick_name replier_name from mf_reply_comment rc left join mf_user u on rc.replier_id = u.id where rc.comment_id = ?";
		return this.executeQuery(sql, new Integer[]{comment_id});
	}

	/**
	 * @throws SQLException 
	* @Title: submitComment
	* @Description: 
	* @param @param reply_comment 
	* @return void 
	* @throws 
	*/ 
	public void submitComment(Reply_CommentEntity reply_comment) throws SQLException {
		
		this.insert(reply_comment);
	}

	/**
	 * @throws SQLException 
	* @Title: listComments
	* @Description: 列出评论信息
	* @param @param unread
	* @param @param user_id
	* @param @return 
	* @return List<CommentDto> 
	* @throws 
	*/ 
	public List<CommentDto> listComments(int status, Integer user_id) throws SQLException {
		String sql = "select c.id,c.comment_time,c.content,r.name rest_name,u.nick_name user_name from mf_comment c left join mf_restaurant r on c.rest_id = r.id left join mf_user u on c.user_id = u.id where c.id in (select comment_id from mf_reply_comment where status = ? and commenter_id = ? and replier_id <> ?) order by c.id desc";
		return this.selectPage(sql, new Integer[]{status,user_id,user_id}, CommentDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listUnReadFeedbacks
	* @Description: 未删除的未读的反馈信息
	* @param @param user_id
	* @param @return 
	* @return List<Reply_FeedbackDto> 
	* @throws 
	*/ 
	public List<FeedbackDto> listUnReadFeedbacks(Integer user_id) throws SQLException {
		String sql = "select f.id, f.content, f.time, f.type,u.nick_name responder_name from mf_feedback f left join mf_user u on f.user_id = u.id where f.status = ? and f.id in (select feedback_id from mf_reply_feedback where status = ? and responder_id = ? and replier_id <> ?) order by f.id desc";
		
		return this.selectAll(sql, new Integer []{DataStatus.ENABLED_GENERAL,ReplyStatus.UNREAD,user_id,user_id},FeedbackDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listFeedbackReplies
	* @Description: 
	* @param @param feedback_id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listFeedbackReplies(Integer feedback_id) throws SQLException {
		
		String sql = "select rf.content,u.nick_name replier_name from mf_reply_feedback rf left join mf_user u on rf.replier_id = u.id where rf.feedback_id = ?";
		return this.executeQuery(sql, new Integer[]{feedback_id});
	}

	/**
	 * @throws SQLException 
	* @Title: listFeedbacks
	* @Description: 列出所有的反馈信息
	* @param @param user_id
	* @param @param read
	* @param @return 
	* @return List<Reply_FeedbackDto> 
	* @throws 
	*/ 
	public List<FeedbackDto> listFeedbacks(Integer user_id, int replyStatus,ListPager pager) throws SQLException {
		
		String sql = "select f.id, f.content, f.time, f.type,u.nick_name responder_name from mf_feedback f left join mf_user u on f.user_id = u.id where f.status = ? and f.user_id = ?";
		return this.selectPage(sql, new Integer[]{replyStatus,user_id}, FeedbackDto.class, pager);
	}

	/**
	 * @throws SQLException 
	* @Title: submitFeedback
	* @Description: 
	* @param @param reply_feedback 
	* @return void 
	* @throws 
	*/ 
	public void submitFeedbackReply(Reply_FeedbackEntity reply_feedback) throws SQLException {
		
		this.insert(reply_feedback);
	}

	/**
	* @Title: listMygifts
	* @Description: 
	* @param @param user_id
	* @param @return
	* @param @throws SQLException 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String,Object>> listMygifts(Integer user_id) throws SQLException {
		
		String sql = "select gu.id,gu.time_exchange,gu.status,g.name gift_name,g.points,u.telephone,ud.qq from mf_gift_user gu left join mf_gift g on gu.gift_id = g.id left join mf_user u on gu.user_id = u.id left join mf_user_detail ud on u.id = ud.user_id where gu.user_id = ?";
		return this.executeQuery(sql, new Integer[]{user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: resetPassworde
	* @Description: 修改密码
	* @param @param user_id
	* @param @param trim 
	* @return void 
	* @throws 
	*/ 
	public void resetPassworde(Integer user_id, String password) throws SQLException {
		
		String sql = "update mf_user set password = ? where id = ?";
		this.executeUpdate(sql, new Object[]{password,user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: queryUserInfo
	* @Description: 
	* @param @param username
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> queryUserInfoByName(String username) throws SQLException {
		String sql = "select id,nick_name,telephone from mf_user where nick_name = ? and enabled = ?";
		return this.executeQueryOne(sql, new Object[]{username,DataStatus.ENABLED_GENERAL});
	}

	/**
	* @Title: queryUserInfoByPhone
	* @Description: 
	* @param @param phone
	* @param @return
	* @param @throws SQLException 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> queryUserInfoByPhone(String phone) throws SQLException {
		
		String sql = "select id,nick_name,telephone from mf_user where telephone = ? and enabled = ?";
		return this.executeQueryOne(sql, new Object[]{phone,DataStatus.ENABLED_GENERAL});
	}

	/**
	 * @throws SQLException 
	* @Title: resetPassword
	* @Description: 
	* @param @param username
	* @param @param password 
	* @return void 
	* @throws 
	*/ 
	public void resetPassword(String username, String password) throws SQLException {
		
		String sql = "update mf_user set password = ? where nick_name = ?";
		this.executeUpdate(sql, new String[]{password,username});
	}

	/**
	 * @throws SQLException 
	* @Title: modifyFeedbackStatus
	* @Description: 
	* @param @param user_id 
	* @return void 
	* @throws 
	*/ 
	public void modifyFeedbackReplyStatus(Integer user_id,Integer status) throws SQLException {
		String sql = "update mf_reply_feedback set status = ? where responder_id = ?";
		this.executeUpdate(sql, new Integer[]{status,user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: modifyCommentReplyStatus
	* @Description: 
	* @param @param user_id
	* @param @param read 
	* @return void 
	* @throws 
	*/ 
	public void modifyCommentReplyStatus(Integer user_id, int status) throws SQLException {
		
		String sql = "update mf_reply_comment set status = ? where commenter_id = ?";
		this.executeUpdate(sql, new Integer[]{status,user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: submitFeedbackReply
	* @Description: 
	* @param @param feedback 
	* @return void 
	* @throws 
	*/ 
	public void submitFeedbackReply(FeedbackEntity feedback) throws SQLException {
		
		this.insert(feedback);
	}
}
