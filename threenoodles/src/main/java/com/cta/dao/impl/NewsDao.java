/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.entity.NewsEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: NewsDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午2:54:55
 */
@Repository
public class NewsDao extends BaseDaoImpl{
	/**
	 * @param objects 
	 * @param sql2 
	* @Title: listNews
	* @Description: 
	* @param @return
	* @param @throws SQLException 
	* @return List<NewsEntity> 
	* @throws 
	*/ 
	public List<NewsEntity> listLatestNews(Integer buildingId) throws SQLException {
		String sql = "select id,title from mf_news where status = ? and building_id = ? order by send_time desc limit 6";
		return this.selectAll(sql, new Integer[]{DataStatus.ENABLED_GENERAL,buildingId},NewsEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listNewsTile
	* @Description: 列出所有的新闻列表
	* @param @param sql
	* @param @param array
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listNewsTile(String sql, Object[] params) throws SQLException {
		
		return this.executeQuery(sql, params);
	}

	/**
	 * @throws SQLException 
	* @Title: listNewsDetail
	* @Description: 
	* @param @param news_id
	* @param @return 
	* @return NewsEntity 
	* @throws 
	*/ 
	public NewsEntity listNewsDetail(Integer news_id) throws SQLException {
		
		String sql = "select id,title,content,type,send_time from mf_news where id = ? and status = ?";
		return this.selectOne(sql, new Integer[]{news_id,DataStatus.ENABLED_GENERAL}, NewsEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: listDefaultNews
	* @Description: 
	* @param @return 
	* @return List<NewsEntity> 
	* @throws 
	*/ 
	public List<NewsEntity> listDefaultNews() throws SQLException {
		
		String sql = "select id,title,content,type,send_time from mf_news where status = ? order by send_time desc";
		return this.selectAll(sql, new Integer[]{DataStatus.DEFAULT_GENERAL}, NewsEntity.class);
	}

}
