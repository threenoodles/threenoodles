/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: AboutDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 下午5:45:58
 */
@Repository
public class AboutDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: getContent
	* @Description: 
	* @param @param type
	* @param @return 
	* @return String 
	* @throws 
	*/ 
	public Map<String,Object> getContent(Integer type) throws SQLException {
		String sql = "select content from mf_about where type = ?";
		return  this.executeQueryOne(sql, new Integer[]{type});
	}

}
