/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.cta.entity.UserEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: LoginDao
 * @Description: 登录
 * @author chenwenpeng
 * @date 2013-6-23 下午8:29:19
 */
@Repository
public class LoginDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: getUserByName
	* @Description: 
	* @param @param username
	* @param @return 
	* @return UserEntity 
	* @throws 
	*/ 
	public UserEntity getUserByName(String username) throws SQLException {
		
		String sql = "select id,nick_name,password from mf_user where nick_name = ?";
		return this.selectOne(sql, new String[]{username}, UserEntity.class);
	}
	
	/**
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public UserEntity getUserByTelephone(String telephone) throws SQLException {
		
		String sql = "select id,nick_name,password from mf_user where telephone = ? limit 1";
		return this.selectOne(sql, new String[]{telephone}, UserEntity.class);
	}
}
