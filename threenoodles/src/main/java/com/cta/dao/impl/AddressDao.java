/**
* @FileName: AddressDaoImpl.java
* @Package com.cta.dao.impl
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-18 下午03:47:02
* @version V1.0
*/
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: AddressDaoImpl
 * @Description: 地址操作
 * @author chenwenpeng
 * @date 2013-5-18 下午03:47:02
 *
 */
@Repository
public class AddressDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	 * @param buildingId 
	 * @param userId 
	* @Title: listUserAddress
	* @Description: 
	* @param @return 
	* @return List<String> 
	* @throws 
	*/ 
	public List<Map<String,Object>> listUserAddress(Integer userId, Integer buildingId) throws SQLException {
		String sql = "select address from mf_address where user_id = ? and building_id = ? and status = 1 order by id desc limit 3";
		return this.executeQuery(sql, new Integer[]{userId,buildingId});
	}
}
