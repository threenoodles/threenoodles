package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.dto.MenuDto;
import com.cta.entity.DelivererEntity;
import com.cta.entity.OrderEntity;
import com.cta.entity.RestaurantEntity;
import com.cta.entity.TaskEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
* @ClassName: OrderDao
* @Description: 订单的dao层
* @author chenwenpeng
* @date 2013-5-26 下午4:55:42
*/ 
@Repository
public class OrderDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listLatestOrders
	* @Description: 列出最新的前8条订单
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listLatestOrders() throws SQLException {
		String sql = "select rest_id,address,ordered_time from mf_order order by id desc limit 8";
		List<Map<String,Object>> orderList = this.executeQuery(sql, null);
		for(Map<String,Object> order : orderList){
			sql = "select name from mf_restaurant where id = "+order.get("rest_id");
			RestaurantEntity rest = this.selectOne(sql, RestaurantEntity.class);
			if(null == rest){
				order.put("rest_name", " ");
				continue;
			}
			order.put("rest_name", rest.getName());
		}
		return orderList;
	}

	/**
	 * @throws SQLException 
	* @Title: saveOrder
	* @Description: 保存订单
	* @param @param order
	* @param @return 
	* @return Integer 
	* @throws 
	*/ 
	public Integer addOrder(OrderEntity order) throws SQLException {
		
		return this.insert_pk(order);
	}
	/**
	 * @throws SQLException 
	* @Title: addOrderMenu
	* @Description: 添加订单menu
	* @param @param menu 
	* @return void 
	* @throws 
	*/ 
	public void addOrderMenu(Integer orderId,MenuDto menu) throws SQLException {
		
		String sql = "insert into mf_order_menu (id, order_id, menu_id, count, price, name) values (null, ?, ?, ?, ?, ?)";
		this.executeUpdate(sql, new Object[]{orderId,menu.getId(),menu.getNum(),menu.getPrice(),menu.getName()});
	}

	/**
	 * @throws SQLException 
	* @Title: getRestDeliverer
	* @Description: 
	* @param @param id
	* @param @param regionType
	* @param @return 
	* @return DelivererEntity 
	* @throws 
	*/ 
	public DelivererEntity getRestDeliverer(Integer building_id, Integer rest_id) throws SQLException {
		String sql = "select id,telephone,send_type from mf_deliverer where status = 1 and id = (select deliverer_id from mf_rest_deliverer_building where rest_id = ? or (rest_id = ? and building_id = ?) order by sort desc limit 1)";
		return this.selectOne(sql, new Integer[]{rest_id,rest_id,building_id},DelivererEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: updateOrderStatus
	* @Description: 更新订单状态
	* @param @param id
	* @param @param faild 
	* @return void 
	* @throws 
	*/ 
	public void updateOrderStatus(int orderId, int status) throws SQLException {
		String sql = "update mf_order set status = ? where id = ?";
		this.executeUpdate(sql, new Integer[]{status,orderId});
	}

	/**
	 * @param score 
	 * @throws SQLException 
	* @Title: updateUserMsg
	* @Description: 更新用户信息
	* @param @param user_id 
	* @return void 
	* @throws 
	*/ 
	public void updateUserMsg(Integer user_id, Double score) throws SQLException {
		String sql = "update mf_user set order_count=(order_count+1),gold=(gold+"+score+") where id = ?";
		this.executeUpdate(sql, new Integer[]{user_id});
	}

	/**
	 * @throws SQLException 
	* @Title: updateRestMsg
	* @Description: 
	* @param @param rest_id 
	* @return void 
	* @throws 
	*/ 
	public void updateRestMsg(Integer rest_id) throws SQLException {
		String sql = "update mf_rest_detail set order_count=(order_count+1) where rest_id = ?";
		this.executeUpdate(sql, new Integer[]{rest_id});
	}

	/**
	 * @throws SQLException 
	* @Title: updateBuildingMsg
	* @Description: 
	* @param @param building_id 
	* @return void 
	* @throws 
	*/ 
	public void updateBuildingMsg(Integer building_id) throws SQLException {

		String sql = "update mf_building set order_count=(order_count+1) where id = ?";
		this.executeUpdate(sql, new Integer[]{building_id});
	}

	/**
	* @Title: updateUserAddressMsg
	* @Description: 更新用户的地址信息
	* @param @param user_id
	* @param @param building_id
	* @param @param address
	* @param @throws SQLException 
	* @return void 
	* @throws 
	*/ 
	public void updateUserAddressMsg(Integer user_id, Integer building_id, String address) throws SQLException {

		String sql = "insert into mf_address values(null,?,?,?,?)";
		this.executeUpdate(sql, new Object[]{user_id,building_id,address,1});
	}

	/**
	 * @throws SQLException 
	* @Title: listOrderMenus
	* @Description: 
	* @param @param order_id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listOrderMenus(Integer order_id) throws SQLException {
		
		String sql = "select menu_id,name,price,count from mf_order_menu where order_id = ?";
		return this.executeQuery(sql, new Integer[]{order_id});
	}

	/**
	 * @throws SQLException 
	* @Title: addTask
	* @Description: 
	* @param @param task 
	* @return void 
	* @throws 
	*/ 
	public void addTask(TaskEntity task) throws SQLException {
		
		this.insert(task);
	}
}
