/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: PhoneDao
 * @Description: phone
 * @author chenwenpeng
 * @date 2013-6-29 下午8:51:33
 */
@Repository
public class PhoneDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listUserPhone
	* @Description: 
	* @param @param userId
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listUserPhone(Integer userId) throws SQLException {
		String sql = "select telephone from mf_user_telephone where user_id = ? order by id desc limit 3";
		return this.executeQuery(sql, new Integer[]{userId});
	}

	/**
	 * @throws SQLException 
	* @Title: getUserPhone
	* @Description: 
	* @param @param userId
	* @param @param phone
	* @param @return 
	* @return Object 
	* @throws 
	*/ 
	public Map<String,Object> getUserPhone(String phone) throws SQLException {
		
		String sql = "select id from mf_user_telephone where telephone = ? and status = 1";
		return this.executeQueryOne(sql, new Object[]{phone});
	}

	
	/**
	 * @throws SQLException 
	 * @param userId 
	* @Title: clearUnCheckPhone
	* @Description: 清除这个phone对应的未验证信息
	* @param @param phone 
	* @return void 
	* @throws 
	*/ 
	public void clearUnCheckPhone(Integer userId, String phone) throws SQLException {
		
		String sql = "delete from mf_user_telephone where user_id = ? and telephone = ? and status = ?";
		this.executeUpdate(sql, new Object[]{userId,phone,DataStatus.UNENABLE_GENERAL});
	}
	/**
	 * @throws SQLException 
	* @Title: addUserPhone
	* @Description: 用户电话存库
	* @param @param userId
	* @param @param phone 
	* @return void 
	* @throws 
	*/ 
	public void addUserPhone(Integer userId, String phone) throws SQLException {
		
		String sql = "insert into mf_user_telephone values(null,?,?,?)";
		this.executeUpdate(sql, new Object[]{userId,phone,DataStatus.ENABLED_GENERAL});
	}
}
