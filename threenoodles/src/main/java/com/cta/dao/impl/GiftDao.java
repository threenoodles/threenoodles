/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.entity.GiftEntity;
import com.cta.entity.Gift_UserEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: GiftDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-8 下午2:12:42
 */
@Repository
public class GiftDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listGifts
	* @Description: 
	* @param @return 
	* @return List<GiftEntity> 
	* @throws 
	*/ 
	public List<GiftEntity> listGifts() throws SQLException {
		
		String sql ="select id, name, pic, descr, points, status from mf_gift where status = 1 order by points";
		return this.selectAll(sql, GiftEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getUserPoints
	* @Description: 
	* @param @param id
	* @param @return 
	* @return Integer 
	* @throws 
	*/ 
	public Map<String,Object> getUserPoints(Integer user_id) throws SQLException {
		
		String sql = "select gold from mf_user where id = ?";
		return this.executeQueryOne(sql, new Integer[]{user_id});
	}
	/**
	 * @throws SQLException 
	* @Title: addUserGift
	* @Description: 
	* @param @param gift_user 
	* @return void 
	* @throws 
	*/ 
	public void addUserGift(Gift_UserEntity gift_user) throws SQLException {
		
		this.insert(gift_user);
	}

	/**
	 * @throws SQLException 
	* @Title: getGiftPoints
	* @Description: 
	* @param @param gift_id
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getGiftPoints(Integer gift_id) throws SQLException {
		String sql = "select points from mf_gift where id = ?";
		return this.executeQueryOne(sql, new Integer[]{gift_id});
	}

	/**
	 * @throws SQLException 
	* @Title: updateUserPoints
	* @Description: 更新用户的积分
	* @param @param points 
	* @return void 
	* @throws 
	*/ 
	public void updateUserPoints(Integer user_id,int points) throws SQLException {
		
		String sql = "update mf_user set gold = ? where id = ?";
		this.executeUpdate(sql, new Integer[]{points,user_id});
	}
}
