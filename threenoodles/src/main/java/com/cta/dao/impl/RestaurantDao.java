/**
* @FileName: RestaurantDao.java
* @Package com.cta.dao.impl
* @Description: TODO
* @author chenwenpeng
* @date 2013-5-20 下午01:38:00
* @version V1.0
*/
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.dto.MenuDto;
import com.cta.dto.RestaurantDto;
import com.cta.entity.BroadCastEntity;
import com.cta.entity.BuildingEntity;
import com.cta.entity.MenuEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: RestaurantDao
 * @Description: 店铺模块数据层
 * @author chenwenpeng
 * @date 2013-5-20 下午01:38:00
 *
 */
@Repository
public class RestaurantDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listRestaurants
	* @Description: 
	* @param @param buildingId
	* @param @return 
	* @return List<RestaurantEntity> 
	* @throws 
	*/ 
	public List<RestaurantDto> listRestaurants(Integer buildingId) throws SQLException {
		
		String sql = "select r.id,r.name,r.alias,r.pic,d.taste,d.speed,d.attitude,r.status from mf_restaurant r,mf_rest_detail d where r.id in (select rest_id from mf_build_restaurant where build_id = ?) and r.id = d.rest_id order by sort desc";
		return this.selectAll(sql, new Object[]{buildingId}, RestaurantDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getBuilding
	* @Description: 
	* @param @param buildingId
	* @param @return 
	* @return BuildingEntity 
	* @throws 
	*/ 
	public BuildingEntity getBuilding(Integer buildingId) throws SQLException {
		String sql = "select id,name from mf_building where id = "+buildingId;
		return this.selectOne(sql, BuildingEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getCategoryList
	* @Description: 
	* @param @param id
	* @param @return 
	* @return List<Integer> 
	* @throws 
	*/ 
	public List<Integer> getCategoryList(Integer id) throws SQLException {
		String sql = "select cate_id from mf_rest_cate where rest_id = ?";
		return this.executeQueryForInt(sql, new Object[]{id});
	}

	/**
	 * @throws SQLException 
	* @Title: getBuilding
	* @Description: 根据楼宇的别名，获取楼宇的摘要信息
	* @param @param alia
	* @param @return 
	* @return BuildingEntity 
	* @throws 
	*/ 
	public BuildingEntity getBuilding(String alia) throws SQLException {
		String sql = "select id,name,alias,region_id from mf_building where alias = ? and status = 1";
		return this.selectOne(sql,new Object[]{alia}, BuildingEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getRestaurantDetail
	* @Description: 
	* @param @param alias
	* @param @return 
	* @return RestaurantEntity 
	* @throws 
	*/ 
	public RestaurantDto getRestaurantDetail(String alias) throws SQLException {
		String sql = "select r.id,r.name,r.alias,r.pic,r.address,r.notice,r.deli_price,d.deli_interval,r.telephone,r.carriage,d.food_count,d.order_count,d.recommend_count,d.taste,d.speed,d.attitude,r.deli_type,r.send_type,r.status from mf_restaurant r,mf_rest_detail d where r.alias = ? and r.id = d.rest_id";
		return this.selectOne(sql, new Object[]{alias}, RestaurantDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getMenuCateList
	* @Description: 获取菜单的大类
	* @param @param id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> getMenuCateList(Integer id) throws SQLException {
		String sql = "select id,name from mf_cate_menu where id in (select distinct cate_id from mf_menu_qd  where rest_id = ? and status = 1)";
		return this.executeQuery(sql, new Object[]{id});
	}

	/**
	 * @throws SQLException 
	* @Title: getMenuList
	* @Description: 获取店铺所有的menulsit
	* @param @param id
	* @param @return 
	* @return List<MenuEntity> 
	* @throws 
	*/ 
	public List<MenuEntity> getMenuList(Integer restId) throws SQLException {
		String sql = "select id,name,price,pic,recommend,cate_id from mf_menu_qd where rest_id = ? and status = 1";
		return this.selectAll(sql, new Object[]{restId},MenuEntity.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getRestaurantStatus
	* @Description: 获取店铺的状态
	* @param @param restId
	* @param @return 
	* @return RestaurantEntity 
	* @throws 
	*/ 
	public RestaurantDto getRestaurantDetail(Integer restId) throws SQLException {
		String sql = "select r.id,r.name,r.alias,r.pic,r.address,r.notice,r.deli_price,d.deli_interval,r.telephone,r.carriage,d.food_count,d.order_count,d.recommend_count,d.taste,d.speed,d.attitude,d.open_time_am,d.open_time_pm,d.close_time_am,d.close_time_pm,r.deli_type,r.send_type,r.status from mf_restaurant r,mf_rest_detail d where r.id = ? and d.rest_id = ?";
		return this.selectOne(sql, new Integer[]{restId,restId},RestaurantDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getMenuList
	* @Description: 获取菜单列表
	* @param @param menuIds
	* @param @return 
	* @return List<MenuEntity> 
	* @throws 
	*/ 
	public List<MenuDto> getMenuList(String menuIds) throws SQLException {
		String sql = "select id,name,price from mf_menu_qd where id in ("+menuIds+")";;
		return this.selectAll(sql, MenuDto.class);
	}

	/**
	 * @throws SQLException 
	* @Title: getCarriage
	* @Description: 
	* @param @param building_id
	* @param @param rest_id
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getCarriage(Integer building_id, Integer rest_id) throws SQLException {
		
		String sql = "select carriage from mf_rest_carriage_building where rest_id = ? and building_id = ?";
		return this.executeQueryOne(sql, new Integer[]{building_id,rest_id});
	}

	/**
	 * @throws SQLException 
	* @Title: clearFavourite
	* @Description: 
	* @param @param restId
	* @param @param buildingId
	* @param @param userId 
	* @return void 
	* @throws 
	*/ 
	public void clearFavourite(Integer restId, Integer buildingId, Integer userId) throws SQLException {
		
		String sql = "delete from mf_user_favourite where user_id = ? and rest_id = ? and building_id = ?";
		this.executeUpdate(sql, new Integer[]{userId,restId,buildingId});
	}

	/**
	 * @throws SQLException 
	* @Title: addFavourite
	* @Description: 
	* @param @param restId
	* @param @param buildingId
	* @param @param userId 
	* @return void 
	* @throws 
	*/ 
	public void addFavourite(Integer restId, Integer buildingId, Integer userId) throws SQLException {
		
		String sql = "insert into mf_user_favourite values(null,?,?,?,?)";
		this.executeUpdate(sql, new Object[]{userId,restId,DataStatus.NONE_STRING,buildingId});
	}

	/**
	 * @throws SQLException 
	* @Title: listLatestBroadcasts
	* @Description: 
	* @param @param id
	* @param @return 
	* @return List<BroadCastEntity> 
	* @throws 
	*/ 
	public List<BroadCastEntity> listLatestBroadcasts(Integer buildingId) throws SQLException {
		String sql = "select name,alt,target from mf_broadcast where status = ? and buildingId = ? order by id desc limit 6";
		return this.selectAll(sql, new Integer[]{DataStatus.ENABLED_GENERAL,buildingId},BroadCastEntity.class);
	}

	/**
	* @Title: listDefaultBroadcasts
	* @Description: 
	* @param @return
	* @param @throws SQLException 
	* @return List<BroadCastEntity> 
	* @throws 
	*/ 
	public List<BroadCastEntity> listDefaultBroadcasts() throws SQLException {
		
		String sql = "select name,alt,target from mf_broadcast where status = ? order by id desc limit 6";
		return this.selectAll(sql, new Integer[]{DataStatus.DEFAULT_GENERAL}, BroadCastEntity.class);
	}
}
