/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl.android;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: AddressDao_android
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-28 下午11:07:12
 */
@Repository
public class AddressDao_ extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listRegions
	* @Description: 
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listRegions() throws SQLException {
		
		String sql = "select id,name from mf_region where status = ?";
		return this.executeQuery(sql, new Integer[]{DataStatus.ENABLED_GENERAL});
	}

	/**
	 * @throws SQLException 
	* @Title: listBuildings
	* @Description: 
	* @param @param regionId
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listBuildings(Integer regionId) throws SQLException {
		
		String sql = "select id,name from mf_building where status = ? and region_id = ?";
		
		return this.executeQuery(sql, new Integer[]{DataStatus.ENABLED_GENERAL,regionId});
	}
}
