/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.constant.DataStatus;
import com.cta.entity.SEOEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @ClassName: CommonDao
 * @Description: 
 * @author chenwenpeng
 * @date 2013-8-9 上午7:54:47
 */
@Repository
public class CommonDao extends BaseDaoImpl{

	/**
	 * @throws SQLException 
	* @Title: listSearchResult
	* @Description: 
	* @param @param menu_name
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> listSearchResult(String menu_name,Integer building_id) throws SQLException {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select mq.id,mq.name,mq.price,r.id rest_id,r.name rest_name,r.alias,rd.speed,rd.taste,rd.attitude from mf_menu_qd mq");
		sql.append(" left join mf_restaurant r on mq.rest_id = r.id");
		sql.append(" left join mf_rest_detail rd on r.id = rd.rest_id");
		sql.append(" where mq.name like '%").append(menu_name).append("%'");
		sql.append(" and r.id in (select rest_id from mf_build_restaurant where build_id = ?)");
		sql.append(" and mq.status = ?");
		
		return this.executeQuery(sql.toString(),new Integer []{building_id,DataStatus.ENABLED_GENERAL});
	}
	
	
	/**
	 * @throws SQLException 
	* @Title: getCommentCount
	* @Description: 
	* @param @param id
	* @param @param status
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getCommentCount(Integer user_id, int status) throws SQLException {
		String sql = "select count(id) count from mf_reply_comment where commenter_id = ? and replier_id <> ? and status = ?";
		return this.executeQueryOne(sql, new Integer[]{user_id,user_id,status});
	}

	/**
	 * @throws SQLException 
	* @Title: getFeedbackCount
	* @Description: 
	* @param @param user_id
	* @param @param status
	* @param @return 
	* @return Map<String,Object> 
	* @throws 
	*/ 
	public Map<String, Object> getFeedbackCount(Integer user_id, int status) throws SQLException {
		String sql = "select count(id) count from mf_reply_feedback where responder_id = ? and replier_id <> ? and status = ?";
		return this.executeQueryOne(sql, new Integer[]{user_id,user_id,status});
	}
	/**
	 * @throws SQLException 
	* @Title: getFavoriteRest
	* @Description: 
	* @param @param id
	* @param @return 
	* @return List<Map<String,Object>> 
	* @throws 
	*/ 
	public List<Map<String, Object>> getFavoriteRest(Integer user_id,Integer building_id) throws SQLException {
		
		String sql = "select r.name,r.alias from mf_user_favourite uf left join mf_restaurant r on uf.rest_id = r.id where uf.user_id = ? and uf.building_id = ?";
		return this.executeQuery(sql, new Integer[]{user_id,building_id});
	}


	/**
	 * @throws SQLException 
	* @Title: getSEOInfo
	* @Description: 
	* @param @param type
	* @param @return 
	* @return SEOEntity 
	* @throws 
	*/ 
	public SEOEntity getSEOInfo(int type) throws SQLException {
		
		String sql = "select title, keywords, description from mf_seo where type = ? limit 1";
		return this.selectOne(sql, new Integer[]{type}, SEOEntity.class);
	}
}
