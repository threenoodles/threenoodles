<%@page language="java" contentType="text/html; charset=utf-8"%>

<div id="feedback_btn"></div>
	<div class="pop" id="feedback_pop" style="display: blcok;">
		<div class="c" style="top: 273.5px;">
			<div class="t">问题反馈</div>
			<div name="step_1" style="display: block;">
				<div>    
					<textarea tips="请输入您的意见或建议，我们会在24小时内为您处理" class="content" name="content">请输入您的意见或建议，我们会在24小时内为您处理</textarea>
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
			</div>
			<div name="step_2" style="display: none;">
				<div class="notify">为了您能够及时的得到反馈，请先</div>
				<div class="login">
					<div class="sub">登录：</div>
					<div style="color: rgb(255, 0, 0); display: none;" class="tip">账号或密码输入错误，请重新输入</div>
					账号：<input type="text" class="input_1 inputTips" name="uname"><br> 
					密码：<input type="password" class="input_1" name="upassword">
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
				<div class="clear"></div>
			</div>
			<div name="step_3" style="display: none;">
				<div class="alertText">正在提交中...</div>
			</div>
			<div name="step_4" style="display: none;">
				<div class="alertText"><span style="color:#00CC00;">问题提交成功！</span></div>
			</div>
			<div name="step_5" style="color:#FF0000;">
				<div class="alertText"><span style="color: red;">提交失败，系统正忙，请稍后重试。</span></div>
			</div>
			<div class="tel">免费客服热线：${service_tel}</div>
			<div class="close">×</div>
		</div>
		<div class="mask"></div>
	</div>