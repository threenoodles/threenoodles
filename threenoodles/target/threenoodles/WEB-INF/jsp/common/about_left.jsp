<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
$(function(){
		leftMenu_about.flag = '${flag}';
		leftMenu_about.setTitle(leftMenu_about.flag);
});

//
var leftMenu_about = {
		setTitle : function(flag){
			$("#leftMenu li").attr("class","");
			if(!flag || '1' == flag){
				$(".partl .title").html("关于我们");
				$("#leftMenu li[val=aboutUs]").attr("class","active");
			}else if('4' == flag){
				$(".partl .title").html("餐厅加盟");
				$("#leftMenu li[val=joinUs]").attr("class","active");
			}else if('5' == flag){
				$(".partl .title").html("服务协议");
				$("#leftMenu li[val=agreement]").attr("class","active");
			}else if('2' == flag){
				$(".partl .title").html("用户指南");
				$("#leftMenu li[val=guide]").attr("class","active");
			}else if('3' == flag){
				$(".partl .title").html("诚聘英才");
				$("#leftMenu li[val=employee]").attr("class","active");
			}
		}
};

</script>
<div class="partl">
		<div class="title">关于我们</div>
		<div class="leftnav">
			<div class="t"></div>
			<div class="body">
				<ul id="leftMenu">
					<li val="aboutUs" class="active"><a href="http://demo.cookwaimai.com/about/toAboutUs">关于我们</a></li>
					<li val="agreement"><a href="http://demo.cookwaimai.com/about/toAreement">服务协议</a></li>
					<li val="joinUs"><a href="http://demo.cookwaimai.com/about/toJoinUs">餐厅加盟</a></li>
					<li val="employee"><a href="http://demo.cookwaimai.com/about/toEmployee">诚聘英才</a></li>
					<li val="guide"><a href="http://demo.cookwaimai.com/about/toHelp">帮助中心</a></li>
				</ul>
			</div>
			<div class="b"></div>
		</div>
</div>	