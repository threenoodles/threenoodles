<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/boxy/boxy.css" media=all>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_lib/jquery.boxy.js"></script>

<script type="text/javascript">
	$(function() {
		var buildingId = "${sessionScope.building.id}";
		var buildingAlias = '${sessionScope.building.alias}';
		var naviFlag = "${naviFlag}";
		topNavi.initListener(buildingId,buildingAlias);
		topNavi.activeNavi(naviFlag);
		
	});
</script>

<div id="header">
	<div class="siteNav">
		<div class="c">
			<div class=l>
				<c:choose>
					<c:when test="${null == sessionScope.user.nick_name}">
					您好，欢迎光临 酷客外卖网！
					<a href='http://demo.cookwaimai.com/login/toLogin?login_flag=to_restaurants&building_alias=${null==sessionScope.building.alias?"none":sessionScope.building.alias}'>登录 </a>| 
					<a href="http://demo.cookwaimai.com/regist/toRegist">免费注册</a>
					</c:when>
					<c:otherwise>
					亲爱的，<a>${sessionScope.user.nick_name}</a>&nbsp;欢迎回来 ! | <a href="/login/logout.action">注销</a>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="r">
				<a id="message">我的信息(0)</a> | <a id="mycenter" class="blink"
					href="#">个人中心</a> | <a id="favorite" class="blink favorite aw"
					href="#">收藏夹
					<div class="aw"></div>
				</a> | <a class="blink" href="http://demo.cookwaimai.com/about/toJoinUs">我要开店</a>
			</div>

			<div id="myMessages" style="display: none;" class="fe favorite">
				<div style="left: 60px" class="fe-arrow"></div>
				<div class="fe-title">信息</div>
				<div class="fe-list">
					<div class="fe-li" val="comment">
						<EM><a>0</a>&nbsp条评论回复<span style="color: red">(新)</span></EM>
					</div>
					<div class="fe-li" val="feedback">
						<EM><a>0</a>&nbsp条反馈回复<span style="color: red">(新)</span></EM>
					</div>
				</div>
			</div>
			<div id="myFavourite" style="display: none;" class="fe favorite">
				<div class="fe-arrow"></div>
				<div class="fe-title">餐馆</div>
				<div class="fe-list">
					<div class="fe-li">
						<EM>店铺加载中...</EM>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="topNav">
		<div class="c">
			<a class="logo" href='http://demo.cookwaimai.com/restaurant/toRestaurants/${null==sessionScope.building.alias?"none":sessionScope.building.alias}'></a>
			<div class="location">
				<div name="def">
					<span id="buildingId" val="" class="large">${sessionScope.building.name}</span>
				</div>

				<a id="changeLocation" href="javascript:void(0);">[&nbsp点击切换新地址&nbsp]</a>
				<div id="locationPop" tabIndex="-1">
					<UL>
						<LI class=last><a href="http://demo.cookwaimai.com"><EM></EM>搜索新地址</a></LI>
					</UL>
					<div class=arrow></div>
				</div>
			</div>
			<div class=navBar>
				<c:if test="${null == sessionScope.building.alias}">
					<a class="navItem nnormal nfirst" name="topNavi_item" href="http://demo.cookwaimai.com" val="order">我要订餐</a> 
				</c:if>
				<c:if test="${null != sessionScope.building.alias}">
					<a class="navItem nnormal nfirst" name="topNavi_item" href="http://demo.cookwaimai.com/restaurant/toRestaurants/${sessionScope.building.alias}" val="order">我要订餐</a> 
				</c:if>
				<a class="navItem nnormal nfirst" name="topNavi_item" href="http://demo.cookwaimai.com/gift/toGifts" val="gift">礼品中心</a> 
				<a id="navi_mycenter" class="navItem nnormal nfirst" name="topNavi_item" href="javascript:;" val="mycenter">我要催餐</a> 
				<a class="navItem nnormal nfirst" name="topNavi_item" href="http://demo.cookwaimai.com/comment/toBuildingComments" val="comments">美食点评</a>
			</div>
		</div>
	</div>
	<div class="searchBar">
		<div class="c">
			<div class="tips">
				<div class="tl">
					<B>找外卖美食：</B>囊括千家餐厅、万道美食
				</div>
			</div>
			<FORM id="search_menu_form" method="post" action="/common/toSearchResult">
				<div class="inputBar">
					<div class="bl"></div>
					<div class="body">
						<div class="icon icon_restaurant"></div>
						<input class="restaurant inputTips" name="menu_name" type="text" tips="输入您要查找的美食，例如:鱼香肉丝" value="">
					</div>
					<div class="br"></div>
				</div>
				<div id="search_menu" class="submit"></div>
			</FORM>
		</div>
	</div>
</div>