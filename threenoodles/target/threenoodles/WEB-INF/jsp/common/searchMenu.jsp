<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${sessionScope.building.name}外卖|酷客外卖网|青岛网上订餐</TITLE>
<META content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="<s:property value="#session.buildingName" escape="false"/>外卖，外卖电话，网上订餐，在线点餐，订外卖，订餐，菜单，促销，点评" />
<meta name="description" content="酷客提供<s:property value="#session.buildingName" escape="false"/>最专业在线免费订餐服务，查看<s:property value="#session.buildingName" escape="false"/>附近的餐厅、美食信息，从您附近喜欢的餐厅订购美食，网上下单，送餐上门。订外卖，找酷客" />
<meta name="application-name" content="酷客 - 免费外卖服务的缔造者" />
<meta content="酷客外卖网版权所有" name="Copyright" />
	
<%@include file="/assets/jsp/common.jsp"%>

<link rel="stylesheet" type="text/css" href="http://demo.cookwaimai.com/assets/css/restaurant/restaurants.css" media="all">
<link rel="stylesheet" type="text/css" href="http://demo.cookwaimai.com/assets/css/common/common_top_searchMenu.css" media="all">

<!--[if IE 6]>
<script type="text/javascript" src="/js/common/DD.belatedPNG.0.0.8a.min.js"></script>
<script type="text/javascript">
	DD_belatedPNG.fix('div,a,li,img');
	$("#restCategory").hide();
</script>
<![endif]-->

<script type="text/javascript">
	
	$(function() {
		var userId = "<s:property value='#session.id'/>";
		topNavi.initListener();
		feedback.init(userId);
	});
</script>
</HEAD>
<BODY>
<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

<div id="pushCustomerMsg"></div>

<div id="container">
		<a class="backLink blink" href="http://demo.cookwaimai.com/restaurant/toRestaurants/${sessionScope.building.alias}">返回首页</a>
		<div class="partm">
			<div class="title">您搜索<em>“${menu_name}”</em>的结果如下：(点击菜单可以购买一份)</div>
			 
			 <c:forEach items="${restaurants}" var="rest">
			 
				 <c:forEach items="${menus[rest]}" var="rest_" varStatus="sta">
					 <c:if test="${0 == sta.index}">
					 	<div class="resTitle">
							<div class="icon"></div>
							<a class="name" href="http://demo.cookwaimai.com/restaurant/toRestaurantDetail/${rest_.alias}">
								${rest_.rest_name}
							</a>
							<div class="star star_${rest_.score}"></div>
							<div class="count">以下是本店铺的查询结果</div>
						</div>
					</c:if>
				 </c:forEach>
				<div class="resultRes">
				 	
				 	<c:forEach items="${menus[rest]}" var="menu">
				 		<div class="section9 item">
							<div class="t"></div>
							<div class="body">
								<a class="name" href="http://demo.cookwaimai.com/restaurant/toRestaurantDetail/${menu.alias}?menu_id=${menu.id}"><em>${menu.name}</em></a>
								<div class="price">￥${menu.price}</div>
							</div>
							<div class="b"></div>
						</div>
				 	</c:forEach>
			 	</div>
			 </c:forEach>
			 
			 
			<div class="searchArea">
				<div class="text">没找到你想要的结果？可以换一个关键字：</div>
				<form id="search_menu_form_bottom" method="post" action="http://demo.cookwaimai.com/common/toSearchResult">
				<div class="input_spe">
					<div class="il"></div>
					<div class="im"><input type="text" value="" name="menu_name"></div>
					<div class="ir"></div>
					<div class="icon"></div>
				</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</BODY>
</HTML>
