<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>

<%@include file="/assets/jsp/common.jsp"%>
<LINK rel=stylesheet type=text/css href="/assets/css/regist/regist.css" media=all>

<script type="text/javascript">
	$(function() {
		registObj.init();
		registObj.initListener();
	});
	var registObj = {
	 	init : function(){
	 		registObj.imageCode = "";
	 		registObj.nameFlag = false;
			registObj.phoneFlag = false;
			registObj.passwordFlag = false;
			registObj.rePasswordFlag = false;
			registObj.imgFlag = false;
	 	},
		initListener : function() {
			
			$("#change_captha_img").click(
				function() {
					$("#imagecode_display").attr("src","http://demo.cookwaimai.com/admin/imagecode/getImageCode?ran="+ Math.random());
			});
			
			//同意服务协议的监听
			$("#form_signup_agree").click(function(){
				if("checked" == $(this).attr("class")){
					$(this).attr("class","");
				}else {
					$(this).attr("class","checked");
				}
			});
			
			$("#username").blur(function() {
				registObj.nameFlag = false;
				var username_ = $(this).val();
				if(!username_ || username_.length<3 || username_.length>30){
					$("#checker_name").removeClass("checker_ok").addClass("checker_wrong").show();
					$("#username_span").html("请填写(3-30)位用户名");
					
				}else if(/^[\u4e00-\u9fa5]+$/.test(username_)){
					$("#checker_name").removeClass("checker_ok").addClass("checker_wrong").show();
					$("#username_span").html("用户名不能包含中文");
					
				}else {
					registObj.checkName(username_);
				}
			});
			
			$("#phone").blur(function() {
				registObj.phoneFlag = false;
				var phone_ = $(this).val();
				if(/^1[3|4|5|8][0-9]\d{8}$/.test(phone_)){
					registObj.checkPhone(phone_);
				}else{
					$("#checker_phone").removeClass("checker_ok").addClass("checker_wrong").show();
					$("#phone_span").html("请正确填写用于收餐或找回密码的手机号");
				}
			});
			
			$("#password").blur(function() {
				registObj.passwordFlag = false;
				var password_ = $(this).val();
			   if(password_ && password_.length >=6 && password_.length<= 20){
				   registObj.passwordFlag = true;
				   	$("#checker_password").removeClass("checker_wrong").addClass("checker_ok").show();
				   	$("#password_span").html("");
			   }else {
					$("#checker_password").removeClass("checker_ok").addClass("checker_wrong").show();
				   	$("#password_span").html("请填写(6-20)位的密码");
			   }
			});
			
			$("#repassword").blur(function() {
				registObj.rePasswordFlag = false;
				var password_ = $("#password").val();
				var repassword_ = $(this).val();
				if(password_ && password_ == repassword_){
					registObj.rePasswordFlag = true;
				    $("#checker_repassword").removeClass("checker_wrong").addClass("checker_ok").show();
				    $("#repassword_span").html("");
				}else {
					 $("#checker_repassword").removeClass("checker_ok").addClass("checker_wrong").show();
					 $("#repassword_span").html("两次密码不一致");
				}
			});
			
			$("#imagecode").blur(function() {
				registObj.imgFlag = false;
				var imagecode_ = $(this).val();
				if(imagecode_ && imagecode_.length==4){
					registObj.checkImagecode(imagecode_);
				}else{
					$("#checker_imagecode").removeClass("checker_ok").addClass("checker_wrong").show();
					$("#imagecode_span").html("请正确填写验证码");					
				}
			});
			//提交，检验正确性
			$("#submit").click(function() {
				var agreement = $("#form_signup_agree").attr("class");
				if("checked"!=agreement){
					Boxy.alert("<span style='color:red;font-size:16px'>请您先同意服务协议！</span>");
					return;
				}
				
				if(registObj.nameFlag && registObj.phoneFlag && registObj.passwordFlag && registObj.rePasswordFlag){
					var mask = null;
					var username = $("#username").val();
					var phone = $("#phone").val();
					var password = $("#password").val();
					$.ajax({
						type : "POST",
						contentType: "application/x-www-form-urlencoded; charset=utf-8",
						url : "http://demo.cookwaimai.com/regist/registUser",
						data : {
							phone : phone,
							username : username,
							password : password
						},
						cache: !1,
						async: !0,
						dataType: "json",
						beforeSend:function(){
							 mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
						 },
						success : function(response){
							if(mask){
								mask.hide();
							}
							if(response && response.success){
								Boxy.alert("<span style='color:#00CC00;font-size:16px'>恭喜您，注册成功！</span>",function(){
									
									window.location.href="http://demo.cookwaimai.com/login/toLogin";
								});
								
							}else if(response && response.messages && response.messages.length >0){
								var html = "<span style='color:red;font-size:15px'>";
									$.each(response.messages,function(index,item){
										html+=item+"<br/>";
									});
									html+="</span>";
									Boxy.alert(html);
								}else {
									Boxy.alert("<span style='color:red;font-size:16px'>请您先同意服务协议！</span>");
								}
						},
						error : function(){
							if(mask){
								mask.hide();
							}
							alert("服务器正忙，请刷新后重试~");
						}
					});
				}else {
					Boxy.alert("<span style='color:red;font-size:16px'>请您按照提示填写信息!</span>");
				}
			});
		},
		//后台验证用户名
		checkName : function(username){
			$.ajax({
				type : "POST",
				contentType: "application/x-www-form-urlencoded; charset=utf-8",
				url : "http://demo.cookwaimai.com/regist/checkUsername",
				data : {
					username : username
				},
				cache: !1,
				async: !0,
				dataType: "json",
				success : function(response){
					//验证通过
					if(response && response.success){
						$("#checker_name").removeClass("checker_wrong").addClass("checker_ok").show();
						$("#username_span").html("");
						
						registObj.nameFlag = true;
					}else {
						$("#checker_name").removeClass("checker_ok").addClass("checker_wrong").show();
						$("#username_span").html("用户名已经存在");
					}
				},
				error : function(){
					alert("服务器正忙，请刷新后重试~");
				}
			});
		},
		//后台验证用户手机号
		checkPhone : function(phone){
			$.ajax({
				type : "POST",
				contentType: "application/x-www-form-urlencoded; charset=utf-8",
				url : "http://demo.cookwaimai.com/regist/checkPhone",
				data : {
					phone : phone
				},
				cache: !1,
				async: !0,
				dataType: "json",
				success : function(response){
					//验证通过
					if(response && response.success){
						registObj.phoneFlag = true;
						$("#checker_phone").removeClass("checker_wrong").addClass("checker_ok").show();
						$("#phone_span").html("");
					}else {
						$("#checker_phone").removeClass("checker_ok").addClass("checker_wrong").show();
						$("#phone_span").html("收餐电话已经存在");
					}
				},
				error : function(){
					alert("服务器正忙，请刷新后重试~");
				}
			});
		},
		//后台验证验证码
		checkImagecode : function(imagecode){
			$.ajax({
				type : "POST",
				contentType: "application/x-www-form-urlencoded; charset=utf-8",
				url : "http://demo.cookwaimai.com/regist/checkImagecode",
				data : {
					imagecode : imagecode
				},
				cache: !1,
				async: !0,
				dataType: "json",
				success : function(response){
					//验证通过
					if(response && response.success){
						registObj.imgFlag = true;
						$("#checker_imagecode").removeClass("checker_wrong").addClass("checker_ok").show();
						$("#imagecode_span").html("");
					}else {
						$("#checker_imagecode").removeClass("checker_ok").addClass("checker_wrong").show();
						$("#imagecode_span").html("请正确填写验证码");
					}
				},
				error : function(){
					alert("服务器正忙，请刷新后重试");
				}
			});
		}
	}

</script>

</HEAD>
<BODY>
	<DIV id=backtotop>
		<A href="#"></A>
	</DIV>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	
	<DIV id="pushCustomerMsg"></DIV>
	<input type="hidden" id="errorCode" value=""></input>
	<div id="container">
		<div class="top_space"></div>
		<div class="partl signup">
			<div class="title">
				<span class="l">免费注册&nbsp;&nbsp;|</span>&nbsp;&nbsp; <a
					href="/login/toLogin.action" class="r">登录</a>
			</div>
			<div id="showErrorCode" class="tips">
				注册只需<em>30秒</em>，注册成功即可获得<em>30积分</em>哦~
			</div>

			<form id="form_signup" action="javascript:;" method="post">
				<table>
					<tr>
						<td class="text">用户名</td>
						<td class="input">
							<div class="l"></div>
							<div class="m">
								<input type="text" id="username" tips="请输入您的用户名" />
							</div>
							<div class="r"></div>
							<div class="checker_wrong" id="checker_name" style="display: none">
								<div class="icon"></div>
								<span id="username_span">请填写(3-30)位用户名</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text">收餐电话</td>
						<td class="input">
							<div class="l"></div>
							<div class="m">
								<input type="text" id="phone" tips="" />
							</div>
							<div class="r"></div>
							<div class="checker_wrong" id="checker_phone" style="display: none">
								<div class="icon"></div>
								<span id="phone_span"><span style='color: red'>请正确填写用于收餐或找回密码的手机号</span></span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text">登录密码</td>
						<td class="input">
							<div class="l"></div>
							<div class="m">
								<input type="password" id="password" />
							</div>
							<div class="r"></div>
							<div class="checker_wrong" id="checker_password" style="display: none">
								<div class="icon"></div>
								<span id="password_span">请填写登录密码</span>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text">重复密码</td>
						<td class="input">
							<div class="l"></div>
							<div class="m">
								<input type="password" id="repassword" />
							</div>
							<div class="r"></div>
							<div class="checker_wrong" id="checker_repassword" style="display: none">
								<div class="icon"></div>
								<span id="repassword_span">两次密码不一致</span>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td class="captha_img"><img id="imagecode_display" src="http://demo.cookwaimai.com/admin/imagecode/getImageCode" id="captha_img" /><a
							id="change_captha_img">看不清，换一张</a></td>
					</tr>

					<tr>
						<td class="text">验证码</td>
						<td class="input">
							<div class="l"></div>
							<div class="m">
								<input type="text" id="imagecode" />
							</div>
							<div class="r"></div>
							<div class="checker_wrong" id="checker_imagecode" style="display: none">
								<div class="icon"></div>
								<span id="imagecode_span">请正确填写验证码</span>
							</div>
						</td>
					</tr>


					<tr>
						<td colspan="2" class="agree">
							<div id="form_signup_agree" id="agree" class="checked"> <div class="icon"></div>
								我已看过并同意<a href="javascript:;"><a id="agreement">《酷客网服务使用协议》</a></a>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="submit" class="btn_116_36 submit">立即注册</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div class="partr">
			<div class="section6 sidepanel">
				<div class="t"></div>
				<div class="body">
					<div class="t1">已经是酷客网的会员？</div>
					<a href="/login/toLogin.action" class="btn_182_46 signinLink">请直接登录</a>

					<a href="/login/toLogin.action"> 只需30秒！<br> ·累计积分兑换礼品<br>
						·收藏喜爱的餐馆和美食<br> ·定制自己的营养计划<br> ……
					</a>
				</div>
				<div class="b"></div>
			</div>
		</div>
	</div>

<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>	
</BODY>
</HTML>
