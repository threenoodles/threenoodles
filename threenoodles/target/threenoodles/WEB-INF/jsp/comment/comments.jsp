<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/comment/comments.js"></script>
<link rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/comment/comments.css" media=all>

<script type="text/javascript">
	$(function() {

		commentsObj.initListener();
		feedback.init();
	});
	
</script>
</head>
<body>

<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
<!-- 反馈信息 -->
<%@include file="/WEB-INF/jsp/common/common_feedback.jsp"%>
	
	<div class="pop" id="feedback_pop" style="display: none;">
		<div class="c" style="top: 273.5px;">
			<div class="t">回复</div>
			<div name="step_1" style="display: block;">
				<div>
					<textarea tips="请描述您的问题或想法" class="content" name="content">请输入您的回复信息</textarea>
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
			</div>
			<div name="step_2" style="display: none;">
				<div class="notify">为了您能够及时的得到反馈，请</div>
				<div class="login">
					<div class="sub">登录酷客：</div>
					<div style="color: rgb(255, 0, 0); display: none;" class="tip">请输入用户名和密码</div>
					账号：<input type="text" tips="请输入您的用户名" class="input_1 inputTips"
						name="uname"><br> 密码：<input type="password"
						class="input_1" name="upassword">
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
				<div class="clear"></div>
			</div>
			<div name="step_3" style="display: none;">
				<div class="alertText">正在提交中...</div>
			</div>
			<div name="step_4" style="display: none;">
				<div class="alertText">回复提交成功！</div>
			</div>
			<div name="step_5" style="display: none;">
				<div class="alertText">好像出了点小问题，请稍后重试。</div>
			</div>
			<div class="tel">免费客服热线：400-0532220</div>
			<div class="close">×</div>
		</div>
		<div class="mask"></div>
	</div>


	<div id="comments_frame"
		style="background-color: #F8F7F5 !important; width: 100%">
		<div id="container" style="">
			<div class="tab" style="display: block;" id="hotComments">
				<div class="comments">
					<div style="text-align: center; margin-top: 20px" class="title">酷友最新评论</div>

					<c:forEach items="${dataList}" var="comment">
					
						<div class="comment" index="">
							<div class="pic">
								<img src="http://demo.cookwaimai.com/assets/images/restaurant/${comment.rest_id}/${comment.rest_pic}" style="width: 100%; height: 140px" />
								<div class="menu_rest">
									烹饪方：<a target="_blank" href="http://demo.cookwaimai.com/restaurant/toRestaurantDetail/${comment.alias}">${comment.rest_name}</a>
								</div>
							</div>
							<div class="replies" val="${comment.id}">
								<div class="item_title">评论详情</div>

								<span class="user_name">${comment.user_name}</span>
								&nbsp在&nbsp
								<fmt:formatDate value="${comment.comment_time}" pattern="yyyy-MM-dd HH:mm:ss" />
								：<div class="c2">
									<b>点评：</b>
									${comment.content}
								</div>
								
								<c:forEach items="${comment.replyList_s}" var="reply">
									<div class="quote">
										<div class="nt">
											<b>${reply.nick_name}</b>
											<fmt:formatDate value="${reply.reply_time}" pattern="yyyy-MM-dd HH:mm:ss" />
											回复
										</div>
										<div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											${reply.content }
										</div>
									</div>
								</c:forEach>
							</div>

							<div class="orderDetail">
								<div class="item_title">订单详情</div>

								<c:forEach items="${comment.menus }" var="menu">
									<div class="menu_detail">
										<a name="menu_name" val="${menu.menu_id}">${menu.name}￥${menu.price}</a>
										<div class="menu_pic" val="${menu.menu_id}" loaded="false" belong="${comment.rest_id }">
											<img alt="鱼香肉丝面" src="http://demo.cookwaimai.com/assets/images/common/loading.gif">
											<div class="menu_buy">
												<a href="http://demo.cookwaimai.com/restaurant/toRestaurantDetail/${comment.alias }?menu_id=${menu.id}" target="_blank">来一份</a>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>

							<div class="scoreDetail" style="">
								<div class="item_title">评分详情</div>

								<div class="score">
									<div class="text">口味：</div>
									<div class="star star_${comment.taste}"></div>
								</div>
								<div class="score">
									<div class="text">速度：</div>
									<div class="star star_${comment.speed}"></div>
								</div>
								<div class="score">
									<div class="text">态度：</div>
									<div class="star star_${comment.attitude}"></div>
								</div>

								<div class="mix">
									<div class="text">最近评论：</div>
									<a target="_blank" href="http://demo.cookwaimai.com/restaurant/toRestaurant/${comment.alias}">${comment.recommend_count}</a>&nbsp次
								</div>
								<div class="actions">
									<a class="reply" val="${comment.id}">回复</a>
								</div>
							</div>
						</div>

					</c:forEach>
				</div>
				<div val="5" class="more" id="moreComments">显示更多热门点评</div>
			</div>
		</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
