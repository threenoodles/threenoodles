﻿<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>	

<%@include file="/assets/jsp/common.jsp"%>
<link rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/restaurant/restaurant.css" media=all>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/restaurant/restaurant.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_lib/cookie.js"></script>

<!--[if IE 6]>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_lib/DD.belatedPNG.0.0.8a.min.js"></script>
<script type="text/javascript">
	DD_belatedPNG.fix('div,a,li');
</script>
<![endif]-->
<script type="text/javascript">
	$(function() {
		var params = {
			buildingId : "${sessionScope.building.id}",
			userId : "${sessionScope.user.id}",
			restId : "${restaurant.id}",
			carriage : "${restaurant.carriage}",
			restName : "${restaurant.name}",
			restAlias : "${restaurant.alias}",
			userName : "${sessionScope.user.nick_name}",
			menuId : "${menu_id}"
		};

		rest.init(params);
		rest.initListener();
		chart.init();
		cart.init(params);
		foodListNav.init();
		subNav.initListener();
		cookie.init();
		hotComments.init(params.restId);
		hotComments.initListener();
		menuSearchObj.initListener();
		
		feedback.init();
		//topNavi.init();
	});
</script>

</HEAD>
<BODY>
	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	<!-- 反馈信息 -->
	<%@include file="/WEB-INF/jsp/common/common_feedback.jsp"%>
	
	<DIV id=container>
		<A class="backLink blink" href="http://demo.cookwaimai.com/restaurant/toRestaurants/${sessionScope.building.alias }">返回首页</A>
		<DIV class=progress>
			<DIV class=text>
				<DIV class=text_1>
					<A href="javascript:;">选餐馆</A>
				</DIV>
				<DIV class=text_2>
					<A href="javascript:;">选美食</A>
				</DIV>
				<DIV class=text_3>确认送餐地址</DIV>
				<DIV class=text_4>餐到付款</DIV>
			</DIV>
			<DIV class=pbar>
				<DIV class="pbaron pbaron_2"></DIV>
			</DIV>
		</DIV>
		<DIV id="notice" class=warning>${restaurant.notice}</DIV>

		<div class="section3 rest_info">
			<div class="t"></div>
			<div class="body">
				<div class="detailInfo">
					<div class="row1">
						<a id="addToFav" style="float: left"> <img
							src="http://demo.cookwaimai.com/assets/images/common/collect.gif"></a><span class="name">${restaurant.name }</span>
						<span class="status">
						<c:choose>
								<c:when test="${1 eq restaurant.status}">营业中</c:when>
								<c:when test="${3 eq restaurant.status}">可预订</c:when>
								<c:otherwise>已打烊</c:otherwise>
						</c:choose>
						</span>
					</div>
					<div class="row2">
						<b>起 送价：</b> <span class="highlight">￥ ${restaurant.deli_price}</span>起 <br> 
						<b>平均送餐时间：</b><span
							class="highlight deliveryTime">${restaurant.deli_interval}</span>分钟 <br> 
						<b>送餐费：</b>
						<span class="highlight">
						￥ ${restaurant.carriage}
						</span> <br>
					</div>
					<div class="row3">
						<b>配送状态：</b> 
						<c:choose>
							<c:when test="${1 eq restaurant.status}">营业中</c:when>
							<c:when test="${3 eq restaurant.status}">可预订</c:when>
							<c:otherwise>已打烊</c:otherwise>
						</c:choose><br>
						<b>最近订单：</b>${restaurant.order_count}份<br>
						<b>最近评论：</b>${restaurant.recommend_count}次
					</div>
					<div class="clear"></div>
					<div class="notify">
						<p>
							<b>店 铺 地 址：</b>${restaurant.address}
						</p>
					</div>
				</div>
				<div class="action">
					<div class="star_overall">
						<b>总体评价：</b>
						<div class="star star_${restaurant.score_avg}"></div>
						<em>${restaurant.score_avg}分</em> <span>(共${restaurant.recommend_count}人评价)</span>
					</div>
					<ul class="rank">
						<li>菜品质量：
							<div class="bar">
								<div id="quality_bar" class="val" style="width:${restaurant.taste*10}px;"></div>
							</div> <em id="comment_quality"> ${restaurant.taste}分 </em>
						</li>
						<li>送餐速度：
							<div class="bar">
								<div id="speed_bar" class="val" style="width:${restaurant.speed*10}px;"></div>
							</div> <em id="comment_speed"> ${restaurant.speed}分</em>
						</li>
						<li>服务态度：
							<div class="bar">
								<div id="speed_bar" class="val" style="width:${restaurant.attitude*10}px;"></div>
							</div> <em id="comment_speed"> ${restaurant.attitude}分 </em>
						</li>
					</ul>
				</div>
			</div>
			<div class="b"></div>
		</div>


		<DIV class=partr>
			<div class="section2 chart chart_pinned_t">
				<div class="t"></div>
				<div class="body">
					<div class="title">
						<div class="icon"></div>
						美食车
					</div>
					<div class="name" id="cartName">${restaurant.name}</div>
					<table id="chart">
						<tbody>
							<tr>
								<td width="113">菜名</td>
								<td width="40" align="center">数量</td>
								<td class="count">小计</td>
							</tr>
							<tr>
								<td colspan="3"><div class="divide"></div></td>
							</tr>

							<tr class="delivery_cost">
								<td colspan="2">送餐费</td>
								<td class="count">
									${restaurant.carriage}
								元</td>
							</tr>
						</tbody>
					</table>
					<div class="total" id="total">
						<div class="line">
							<div class="text">总计：</div>
							<div val='totalPrice' class="num">
									${restaurant.carriage}
							元</div>
							<div val='totalCount' class="count">0份</div>
						</div>
						<div class="line">
							<div class="text">本单获得积分：</div>
							<div val='totalPoint' class="num">0分</div>
						</div>
					</div>
					<div class="action" id="submitOrder">
						<a name="clear">清空美食车</a>
						<div id="pay" style="cursor: pointer;" class="btn_91_28 submit">买单</div>
						<div id="viewPhone" style="display: none; cursor: pointer;"
							class="btn_91_28 submit">查看电话</div>
					</div>
				</div>
				<div class="b"></div>
				<div class="handy"></div>
			</div>
		</DIV>



		<div class="partl">
			<div class="subnav">
				<a name="menuArea" class="active" href="javascript:void(0);"><div>菜单</div>
					<em></em></a> <a name="hotComments" href="javascript:void(0);"><div>热门点评</div>
					<em></em></a> <a name="favorable" href="javascript:void(0);"><div>优惠信息</div>
					<em></em></a>
				<div class="qsearch">
					<div class="text">快速搜索：</div>
					<div class="l"></div>
					<input type="text" tips="输入菜品快速搜索" name="key" id="search" class="inputTips">
					<div class="r"></div>
				</div>
			</div>

			<div id="menuArea" class="menuArea">
				<div class="floatnav floatnav_pinned_t">
					<div class="t"></div>
					<div class="body">
						<ul>
							<li class=""><a to="a_0" href="javascript:void(0);">店长推荐</a></li>
							<c:forEach items="${menus}" var="category">
								<c:if test="${0 ne category.key}">
								<li class=""><a to="a_${category.key}" href="javascript:void(0);">${category.value.name}</a></li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
					<div class="b"></div>
				</div>

				<a name="a_0"></a>
				<div class="title">店长推荐</div>
				<div class="block">
				
				<c:forEach items="${menus_recommend}" var="menu" varStatus="status">
					 <c:choose>
							<c:when test="${0 == (status.index+1)%3}">
								<div class="rec_c">
							</c:when>
							<c:otherwise>
								<div class="rec_c rlast">
							</c:otherwise>
					</c:choose>
					
						<div class="section4 rec_p">
							<div class="t"></div>
							<div class="body">
								<img src="http://demo.cookwaimai.com/assets/images/restaurant/${restaurant.id}/${menu.pic}">
							</div>
							<div class="b"></div>
						</div>
						<a class="rec_t recommendItem" href="javascript:void(0);" val="${menu.id}">
							<div class="name">
								${menu.name}
							</div>
							<div class="price">
								￥
								${menu.price}
							</div>
							<div class="addone"></div>
							<input name="menu_id" style="display: none;" value="${menu.id}">
						</a>
				</div>
				</c:forEach>		
			</div>
			
			
			
			<c:forEach items="${menus}" var="category">
				<a name="a_${category.value.id}"></a>
				<div class="title">
					${category.value.name}
				</div>
				<div class="block">
						
						<c:forEach items="${category.value.menu_list}" var="menu" varStatus="status">
							
							<c:choose>
								<c:when test="${1 == (status.index+1)%4}">
									<a class="item" href="javascript:void(0);" val="${menu.id}">
								</c:when>
								<c:when test="${2 == (status.index+1)%4}">
									<a class="item ilast" href="javascript:void(0);" val="${menu.id}">
								</c:when>
								<c:when test="${3 == (status.index+1)%4}">
									<a class="item nth" href="javascript:void(0);" val="${menu.id}">
								</c:when>
								<c:when test="${0 == (status.index+1)%4}">
									<a class="item nth ilast" href="javascript:void(0);" val="${menu.id}">
								</c:when>
							</c:choose>
								<div class="name">
									${menu.name}
								</div>
								<div class="price">
									￥${menu.price}
								</div>
								<div class="addone"></div> 
								<input name="menu_id" style="display: none;" value="${menu.id }">
								<input name="menu_name" style="display: none;" value="${menu.name }">
								<input name="menu_price" style="display: none;" value="${menu.price }">
								
							</a>
						</c:forEach>
						
				</div>
				
				</c:forEach>
		</div>


		<div id="hotComments" style="display: block;" name="tab" class="tab">
			<div class="comments">
				<div class="title" style="text-align: center; margin-top: 50px">最新评论</div>

				<div id="comments" class="section1 com">
				</div>
				<div id="moreComments" class="more" val="5">显示更多热门点评</div>

				<div class="post">
					<div class="title">
						发表留言<span style="color: red;"></span>
					</div>
					<form class="submitComment" method="post">
						<a name="reply"></a>
						<textarea name="content"></textarea>
						<br>
						<!-- 
						<div class="captcha">
							<img src="http://demo.cookwaimai.com/admin/imagecode/getImageCode" id="captha_img"><a id="change_captha_img">看不清，换一个</a>
						</div>
						<div class="e">
							验证码：<input type="text" name="captcha" class="captcha" maxlength="5">请输入验证码！
						</div>
						 -->
						<div style="cursor: pointer" class="submit" pointsadded="1"></div>
					</form>
				</div>
			</div>
		</div>

		<!-- 优惠信息 -->
		<div id="favorable" name="tab" val="4" class="tab">
			<div class="sale">
				<!--
				本餐馆暂时无优惠信息、优惠活动，敬请期待……
				-->
				<div class="section1">
					<div class="t"></div>
					<div class="body">


						<div class="title">本餐馆，暂无优惠信息，敬请您的期待...</div>

					</div>
					<div class="b"></div>
				</div>
			</div>
		</div>

	</div>
	</div>
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>

</BODY>
</HTML>
