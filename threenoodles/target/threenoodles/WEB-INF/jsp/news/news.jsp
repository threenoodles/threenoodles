<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>	
<%@include file="/assets/jsp/common.jsp"%>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/common/about.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript">

	$(function(){
		var newsId = "${news.id}";
		news.init(newsId);
	});
	var news = {
			init : function(newsId){
				var newsList = $(".nleftnav .body").find("li");
				if(newsId){
					news.setTitleActive(newsId);
				}
			},
			
			setTitleActive : function(newsId){
				var newsList = $(".nleftnav .body").find("li");
				for(var i = 0;i<newsList.length;i++){
					var newsObj = $(newsList[i]);
					if(newsId == newsObj.attr("val")){
						newsObj.attr("class","active");
						return;
					}
				}
			}
	};
</script>
</head>
<body>

<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	
<div id="pushCustomerMsg"></div>

	<div id="container">
		<input type="hidden" id="newsid" value="20"/>
		<div class="top_space"></div>

		<div class="npartl">
			<div class="title">新闻中心</div>
			<div class="nleftnav">
				<div class="t"></div>
				<div class="body">
					<ul>
						<c:forEach items="${newsList}" var="news_">
							<li class="news_20" val="${news_.id}">
								<a href="/news/listNewsDetail/${news_.id}">
						 			<c:if test="${1 == news_.type }">[新闻] </c:if><c:if test="${2 == news_.type }">[打折] </c:if><c:if test="${3 == news_.type }">[公告] </c:if>
						 			${news_.title}
						 		</a>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="b"></div>
			</div>
		</div>
			
			<div class="npartr">
				<div id="ncontent">
			<div class="npartm">
				<div class="title" style="text-align:center">
		 			<c:if test="${1 == news.type }">[新闻] </c:if><c:if test="${2 == news.type }">[打折] </c:if><c:if test="${3 == news.type }">[公告] </c:if>
			 		${news.title}
				</div>
				<p style="text-align:center">
					
					<fmt:formatDate value="${news.send_time}" pattern="yyyy年MM月dd日 HH:mm:ss" />
				</p>
				<div class="hr1"></div>
				<div class="block">
					${news.content }
				</div>
			</div>
			</div>
		</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>