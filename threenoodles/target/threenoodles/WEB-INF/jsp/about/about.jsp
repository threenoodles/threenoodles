<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/common/about.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>

<div id="backtotop"><a href="#"></a></div>

<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

<div id="pushCustomerMsg"></div>

<div id="container">

	<div class="top_space"></div>
	
	
	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/about_left.jsp"%>
	
	<div class="partr">
		<div id="content">
			${content}
		</div>
	</div>
</div>
	
<!-- 底部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>