<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>

<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all" />
<LINK rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/mycenter/changePassword.css" media=all />


<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		resetObj.initListener();
		mycenterObj.initListener(flag);
	});

	
	var resetObj = {
			flag : false,
			initListener : function(){
				$("#newpassword").blur(function(){
					resetObj.flag = false;
					var password = $(this).val().replace(/\s+/g,"");
					if(password && password.length>=6 && password.length <=20){
						resetObj.password = password;
						$("#newpassword_notice").attr("class","checker_ok");
						$("#newpassword_notice").find("span").html("验证通过");
					}else {
						$("#newpassword_notice").attr("class","checker_wrong");
						$("#newpassword_notice").find("span").html("请输入6-20位密码");
					}
				});
				
				$("#reNewPassword").blur(function(){
					
					resetObj.flag = false;
					var rePassword = $(this).val().replace(/\s+/g,"");
					
 					if(rePassword && rePassword == resetObj.password){
						$("#reNewPassword_notice").attr("class","checker_ok");
						$("#reNewPassword_notice").find("span").html("验证通过");
						resetObj.flag = true;
					}else{
						$("#reNewPassword_notice").attr("class","checker_wrong");
						$("#reNewPassword_notice").find("span").html("两次密码不一致");
					}
				});
				
				$("#resetPassword").click(function(){
					if(resetObj.flag){
						$.ajax({
							type : "POST",
							url : "/mycenter/password/resetPassword",
							type : "POST",
							dataType: 'json',
							contentType: "application/x-www-form-urlencoded;charset=utf-8",
							cache: false,
							data : {
								password : resetObj.password
							},
							success : function(response){
								if(response && response.success){
									Boxy.alert("<span style='color:green'>恭喜您，密码重置成功！<span>");
									$("#resetPassword").remove();
								}else{
									Boxy.alert("<span style='color:red'>对不起，您输入的输入有误，请重新输入！<span>");
								}
							},
							error : function(){
								Boxy.alert("<span style='color:red'>对不起，系统正忙，请稍后重试！<span>");
							}
						});
					}else {
						Boxy.alert("<span style='color:red'>对不起，您输入的输入有误，按照提示填写！<span>");
					}
				});
			}
	};

</script>
</head>
<body>

	<div id="backtotop">
		<a href="#"></a>
	</div>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>

	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>
	
		<div class="partr">
			<div class="title">更改密码</div>
			<div class="tips">请输入您的新密码</div>
			<form method="post" action="#" id="form_chgPassword">
			<table>
				<tbody>
				<tr>
					<td class="text">原始密码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type="password" id="oldpassword" value="000000"></div>
						<div class="r"></div>
						<div><div class="icon"></div><span></span></div>
					</td>
				</tr>
				
				<tr>
					<td class="text">输入新密码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type="password" id="newpassword"></div>
						<div class="r"></div>
						<div id="newpassword_notice"><div class="icon"></div><span></span></div>
					</td>
				</tr>
				<tr>
					<td class="text">重复新密码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type="password" id="reNewPassword"></div>
						<div class="r"></div>
						<div id="reNewPassword_notice"><div class="icon"></div><span></span></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id="resetPassword" class="btn_116_36 submit">修改密码</div></td>
				</tr>
			</tbody></table>
			</form>
		</div>
		

	</div>

	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
