<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myreply.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/mycenter/mycenter_comment.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		var flag = "${flag}";
		mycenterObj.initListener(flag);
		commentObj.initListener();
		topNavi.init();
	});
	
</script>
</head>
<body>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

<div class="pop" id="feedback_pop" style="display: none">
		<div class="c" style="top: 273.5px;">
			<div class="t" style="text-align: center;">回复</div>
			<div name="step_1" style="display: block;">
				<div>
					<textarea tips="请描述您的问题或想法" class="content" name="content">请输入您的回复信息</textarea>
				</div>
				<div class="submit btn_82_27 btn_82_26">提交</div>
			</div>
			<div name="step_3" style="display: none;">
				<div class="alertText">正在提交中...</div>
			</div>
			<div name="step_4" style="display: none;">
				<div class="alertText">提交成功！</div>
			</div>
			<div name="step_5" style="display: none;">
				<div class="alertText">好像出了点小问题，请稍后重试。</div>
			</div>
			<div class="tel">免费客服热线：${service_tel}</div>
			<div class="close">×</div>
		</div>
		
		<div class="mask"></div>
	</div>

	<div id="pushCustomerMsg"></div>
	<div id="container">
		<div class="top_space"></div>

	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>

		<div class="partr">
			<div class="section11 tips">
				<div class="t"></div>
				<div class="body">
					欢迎您，亲爱的
					${sessionScope.user.nick_name}, 最近您在酷客网完成了 <em>${sessionScope.user_detail.order_count}</em> 个订单，已经有了 <em>${sessionScope.user_detail.gold}</em> 点积分，<a href="#">礼品中心&nbsp</a>已经上线,更多礼品等你拿~
				</div>
				<div class="b"></div>
			</div>

			<div class="section11 orderArea">
				<div class="t"></div>
				<ul id="commentList" class="body">
					<li class="th">
						<div class="listItem li_0"></div>
						<div class="listItem li_1">您的评论摘要</div>
						<div class="listItem li_2">评论的餐厅</div>
						<div class="listItem li_3">回复者</div>
						<div class="listItem li_4">评论时间</div>
						<div class="listItem li_5">是否已读</div>
						<div class="listItem li_6" style="float: right;">操作</div>
						<div class="clear"></div>
					</li>

					<c:forEach items="${dataList}" var="comment">
						<li>
							<div class="listItem li_0"></div>
							<div class="listItem li_1">
								${comment.content}
							</div>
							<div class="listItem li_2">
								${comment.rest_name }
							</div>
							<div class="listItem li_3">
								<c:forEach items="${comment.replyList_s}" var="cr">
									${cr.replier_name}&nbsp
								</c:forEach>
							</div>
							<div class="listItem li_4">
								<fmt:formatDate value="${comment.comment_time }" pattern="yyyy年MM月dd日 HH:mm:ss" />
							</div>
							<div class="listItem li_5">
									<a>未读</a>
							</div>
							<div class="listItem li_6" style="float: right;">
								<a href="javascript:void(0);" name="view_detail">查看详情</a>
							</div>
							<div class="clear"></div>

							<div class="detailArea">
								<div class="reply_my">
									<div class="reply_my_name">
										<b>我的评论详情：</b>
									</div>
									<div>
										${comment.content}
									</div>
								</div>

								<c:forEach items="${comment.replyList_s}" var="reply">
									<div class="reply_other">
										<div class="reply_name">
											<b>${reply.replier_name}</b>的回复：
										</div>
										<div class="reply_content">
											${reply.reply_content}
											<a val="${comment.id}" href="#">回复</a>
										</div>
									</div>
								</c:forEach>
								<div class="clear"></div>
							</div>
						</li>
					</c:forEach>

				</ul>
				<div class="b"></div>
			</div>

			<c:if test="${empty dataList}">
				<div id="moreComments" class="more" val="0">您没有“未读”的评论，点击显示“已读”评论</div>
			</c:if>
			<c:if test="${!empty dataList}">
				<div id="moreComments" class="more" val="0">点击显示更多我的评论信息</div>
			</c:if>
		</div>
	</div>
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
