<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_common/mycenter.js"></script>

<link href="http://demo.cookwaimai.com/assets/css/mycenter/mycenter.css" rel="stylesheet" type="text/css" media="all"/>
<link href="http://demo.cookwaimai.com/assets/css/mycenter/myorder.css" rel="stylesheet" type="text/css" media="all"/>
<LINK rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/mycenter/changePassword.css" media=all />

<script type="text/javascript">

$(function(){
	var flag = "${flag}";
	var telephone = "${data.telephone}";
	var username = "${data.nick_name}";
	resetObj.initListener(username,telephone);
	mycenterObj.initListener(flag);
	alarm.initAlarm();
});

var resetObj = {
		code_flag : false,
		password_flag : false,
		rePassword_flag : false,
		initListener : function(username,phone){
			$("#codeButton").click(function(){
				if(90==alarm.sysSecond || alarm.sysSecond<=0){
					resetObj.sendPhoneCode(phone);
				}
			});
			$("#phoneCone").blur(function (){
				resetObj.code_flag = false;
				var phoneCode = $(this).val().replace(/\s+/g,"");
				if(phoneCode){
					resetObj.checkPhoneCode(phoneCode);
				}else{
					$("#phoneCode_notice").attr("class","checker_wrong");
					$("#phoneCode_notice").find("span").html("验证码填写错误");
				}
			});
			
			$("#newpassword").blur(function(){
				resetObj.password_flag = false;
				var password = $(this).val().replace(/\s+/g,"");
				if(password.length>=6 && password.length <=20){
					resetObj.password = password;
					$("#newpassword_notice").attr("class","checker_ok");
					$("#newpassword_notice").find("span").html("验证通过");
					resetObj.password_flag = true;
				}else {
					$("#newpassword_notice").attr("class","checker_wrong");
					$("#newpassword_notice").find("span").html("请输入6-20位密码");
				}
			});
			
			$("#reNewPassword").blur(function(){
				resetObj.rePassword_flag = false;
				var rePassword = $(this).val().replace(/\s+/g,"");
				if(rePassword && rePassword == resetObj.password){
					$("#reNewPassword_notice").attr("class","checker_ok");
					$("#reNewPassword_notice").find("span").html("验证通过");
					resetObj.rePassword_flag = true;
				}else{
					$("#reNewPassword_notice").attr("class","checker_wrong");
					$("#reNewPassword_notice").find("span").html("两次密码不一致");
				}
			});	
			//修改
			$("#resetPassword").click(function(){
				
				if(resetObj.code_flag && resetObj.password_flag && resetObj.rePassword_flag){
					$.ajax({
						type : "POST",
						contentType: "application/x-www-form-urlencoded; charset=utf-8",
						url : "/mycenter/password/resetPassword_uk",
						data : {
							username : username,
							code : $("#phoneCone").val(),
							password : $("#newpassword").val()
						},
						cache: !1,
						async: !0,
						dataType: "json",
						beforeSend:function(){
							 mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
						 },
						success : function(response){
							if(mask){
								mask.hide();
							}
							if(response && response.success){
								alarm.initAlarm();
								alarm.startAlarm();
								Boxy.alert("<span style='color:#00CC00;font-size:16px'>恭喜您，密码重置成功！</span>",function(){
									
									window.location.href="/login/toLogin";
								});
							}else {
								Boxy.alert("<span style='color:red;font-size:16px'>对不起，验证码错误，重试密码失败！</span>");
							}
						},
						error : function(){
							if(mask){
								mask.hide();
							}
							Boxy.alert("<span style='color:red;font-size:16px'>对不起，服务器正忙，请稍后重试！</span>");
						}
					});
				}
			});
		},
		sendPhoneCode : function(phone){
			$.ajax({
				type : "POST",
				contentType: "application/x-www-form-urlencoded; charset=utf-8",
				url : "/phone/sendPhoneCode_resetpassword",
				data : {
					phone : phone
				},
				cache: !1,
				async: !0,
				dataType: "json",
				beforeSend:function(){
					 mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
				 },
				success : function(response){
					if(mask){
						mask.hide();
					}
					if(response && response.success){
						alarm.initAlarm();
						alarm.startAlarm();
						Boxy.alert("<span style='color:#00CC00;font-size:16px'>验证码发送成功！</span>");
					}else {
						Boxy.alert("<span style='color:red;font-size:16px'>对不起，验证码发送失败，请刷新后重试！</span>");
					}
				},
				error : function(){
					if(mask){
						mask.hide();
					}
					Boxy.alert("<span style='color:red;font-size:16px'>对不起，服务器正忙，请稍后重试！</span>");
				}
			});
		},
		checkPhoneCode : function(code){
			$.ajax({
				type : "POST",
				contentType: "application/x-www-form-urlencoded; charset=utf-8",
				url : "/phone/checkPhoneCode_resetPassword",
				data : {
					code : code
				},
				cache: !1,
				async: !0,
				dataType: "json",
				success : function(response){
					if(response && response.success){
						$("#phoneCode_notice").attr("class","checker_ok");
						$("#phoneCode_notice").find("span").html("验证通过");
						resetObj.code_flag = true;
					}else {
						$("#phoneCode_notice").attr("class","checker_wrong");
						$("#phoneCode_notice").find("span").html("验证码填写错误");
					}
				},
				error : function(){
					Boxy.alert("<span style='color:red;font-size:16px'>对不起，服务器正忙，请稍后重试！</span>");
				}
			});
		}
};

var alarm = {
		initAlarm : function(){
			alarm.sysSecond = 90;
			if(null !== alarm.interValObj){
				window.clearInterval(alarm.interValObj);
				alarm.interValObj = null;
			}
			$("#codeButton").val("点击发送验证码");
		},
		startAlarm : function(){
			alarm.interValObj=window.setInterval(function(){
				if(alarm.sysSecond > 0) {
					alarm.sysSecond = alarm.sysSecond-1;
					$("#codeButton").val(alarm.sysSecond + "秒后,可以重新接收");
				}else {
					alarm.initAlarm();
				}
			},1000); //间隔函数，1秒执行 
		}
};
</script>
</head>
<body>


	<div id="backtotop">
		<a href="#"></a>
	</div>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>

	<div id="pushCustomerMsg"></div>

	<div id="container">
		<div class="top_space"></div>

	<!-- 个人中心左侧导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/mycenter_left.jsp"%>
	
		<div class="partr">
			<div class="title">更改密码</div>
			<div class="tips">您要修改密码的用户名为：<span style="color: #ff9900;font-size:20px">${data.nick_name}&nbsp&nbsp</span>手机号为：<span style="color: #ff9900;font-size:20px">${data.telephone}</span>，如果确认请“点击发送验证码”</div>
			<form method="post" action="#" id="form_chgPassword">
			<table>
				<tbody>
				
				<tr>
					<td class="text">发送验证码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type='button' id='codeButton' value='点击发送验证码' style='width:310px'></div>
						<div class="r"></div>
						<div name="checker_oldpassword" class="checker_notice"></div>
						
					</td>
				</tr>
				
				<tr>
					<td class="text">短信验证码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input id="phoneCone" type="text" name="oldpassword"></div>
						<div class="r"></div>
						<div id="phoneCode_notice" class="checker_notice"><div class="icon"></div><span>输入您的手机验证码</span></div>
						
					</td>
				</tr>
				
				<tr>
					<td class="text">输入新密码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type="password" id="newpassword"></div>
						<div class="r"></div>
						<div id="newpassword_notice"><div class="icon"></div><span></span></div>
					</td>
				</tr>
				<tr>
					<td class="text">重复新密码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input type="password" id="reNewPassword"></div>
						<div class="r"></div>
						<div id="reNewPassword_notice"><div class="icon"></div><span></span></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><div id="resetPassword" class="btn_116_36 submit">修改密码</div></td>
				</tr>
			</tbody></table>
			</form>
		</div>

	</div>

	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>
