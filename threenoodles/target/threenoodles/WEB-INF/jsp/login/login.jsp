<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<%@include file="/assets/jsp/common.jsp"%>

<link rel=stylesheet type=text/css href="http://demo.cookwaimai.com/assets/css/login/login.css" media=all />
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/login/login.js"></script>
<script type="text/javascript">
	$(function() {
		//用户登录的标志，指示是不是从顶部以外的地方登录；
		var loginFlag = "${login_flag}";
		var restAlias = "${rest_alias}";
		var buildingAlias = "${building_alias}";
		loginObj.initListener(loginFlag,restAlias,buildingAlias);
	});

</script>

</HEAD>
<BODY>
	<DIV id=backtotop>
		<A href="denglu.html"></A>
	</DIV>

	<!-- 顶部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
	
	
	<DIV id=pushCustomerMsg></DIV>

	<DIV id=container>
		<DIV class=top_space></DIV>
		<DIV class="partl login">
			<DIV class=title>
				<SPAN class="l">&nbsp;&nbsp&nbsp;&nbsp登录&nbsp;&nbsp|</SPAN>&nbsp;&nbsp;
				<A class=r href="/regist/toRegist.action">免费注册</A>
			</DIV>
			<FORM method=post action="">
				<TABLE>
					<TBODY>
					<tr class="login_failed">
					<td></td>
					<td name="login_failed_text"></td>
					</tr>
						<TR>
							<TD class=text>账号</TD>
							<TD class=input>
								<DIV class=l></DIV>
								<DIV class=m>
									<input id="username" class="inputTips" type="text" value="请输入您的用户名">
								</DIV>
								<DIV class=r></DIV>
							</TD>
						</TR>
						<TR>
							<TD class=text>密码</TD>
							<TD class=input>
								<DIV class=l></DIV>
								<DIV class=m>
									<INPUT type="password" id="password">
								</DIV>
								<DIV class=r></DIV>
							</TD>
						</TR>
						<TR>
							<TD class=forget_password colSpan=2><A class=blink href="http://demo.cookwaimai.com/mycenter/password/toQuery">忘记密码?</A></TD>
						</TR>
						<TR>
							<TD colSpan=2><INPUT id="submit" class="btn_116_36 submit" value=登录酷客
								type="button"></TD>
						</TR>
					</TBODY>
				</TABLE>
			</FORM>
			<DIV class=connect_login></DIV>
		</DIV>
		<DIV class=partr>
			<DIV class="section6 sidepanel">
				<DIV class=t></DIV>
				<DIV class=body>
					<DIV class=t1>还不是酷客会员？</DIV>
					<A class="btn_182_46 signinLink" href="http://demo.cookwaimai.com/regist/toRegist">免费注册酷客</A>
					<DIV class=t2>
						只需<EM>30秒！</EM>
					</DIV>
					<DIV class=t3>
						·累计积分兑换礼品<BR>·收藏喜爱的餐馆和美食<BR>·定制自己的营养计划<BR> ……
					</DIV>
				</DIV>
				<DIV class=b></DIV>
			</DIV>
		</DIV>
	</DIV>

	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</BODY>
</HTML>
