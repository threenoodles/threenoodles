<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<%@include file="/assets/jsp/common.jsp" %>
<link rel="stylesheet" type="text/css" href="http://demo.cookwaimai.com/assets/css/index/index.css" media="all">
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/index/index.js"></script>

<!--[if lt IE 6]>
<script type="text/javascript">
window.onload=function(){
	alert("您的浏览器版本过低，为了您更好的体验，请使用高版本的浏览器！");
}
</script>
<![endif]-->
<SCRIPT type="text/javascript">
	$(function() {
		addressObj.init();
		addressObj.initListener();
		searchObj.init();
		searchObj.initListener();
	});
</SCRIPT>
</HEAD>
<BODY>
	<div id="bgimg">
		<IMG name="img_0" src="http://demo.cookwaimai.com/assets/images/index/bg.jpg" alt="酷客外卖网-青岛外卖"
			style="width: 100%; height: 115%">
	</div>
	<div id="header">
	
			<c:choose>
				<c:when test="${null != sessionScope.user}">
					<a href="javascript:;">欢迎您，${sessionScope.user.nick_name}</a>| <a
						href="http://demo.cookwaimai.com/login/logout">注销 | </a>
					<a href="http://demo.cookwaimai.com/mycenter/order?flag=today">个人中心</a>
				</c:when>
				<c:otherwise>
					<a href="http://demo.cookwaimai.com/login/toLogin">登录</a>&nbsp;&nbsp; 
					<a href="http://demo.cookwaimai.com/regist/toRegist">免费注册</a>
				</c:otherwise>
			</c:choose>
	</div>
	<div id="container">
		<div class="logo"></div>
		<div class="bg"></div>
		<div class="searchArea">
			<FORM method="post" action="">
				<input class="input inputTips" name="key" type="text"
					tips="请输入您所在的地址的关键字，例如 ：大学" autocomplete="off">
				<div class="icon"></div>
				<ul id="autoComplete" style="display: none;">
				</ul>
				<img id="submit" class="btn" name="submit"
					src="http://demo.cookwaimai.com/assets/images/index/loadingSearch.png"> <input
					style="display: none;" type="text" />
			</FORM>
		</div>
		<div class="hotArea">
			<div>
				<a id="allAreasTitle" class="t2 t_c fl">青岛区域：</a>
				<div class="t4">
					<c:forEach items="${regions}" var="region">
						<c:if test=""></c:if>
 					    <a class="t4_l active" href="#" val="${region.id}">${region.name}</a>
					</c:forEach>
				</div>
				
			</div>
			<div class="clear"></div>

			<div id="hotAreas">
				<a name="region_name" class="t2 fl">热门地址：</a>
				<div class="t3">
					<c:forEach items="${buildings_hot}" var="building">
						<a href="http://demo.cookwaimai.com/restaurant/toRestaurants/${building.alias}">${building.name}</a>
					</c:forEach>
				</div>
			</div>

			<c:forEach items="${buildings_all}" var="region">
				<div name="region_building" val="${region.key}" style="display: none">
					<a name="region_name" class="t2 fl"></a>
					<div class="t3">
						<c:forEach items="${region.value}" var="building">
							<c:choose>
								<c:when test="${14 eq region.key}">
					   				<a href="${building.id}">${building.name}</a>
					   			</c:when>
					   			<c:otherwise>
					   				<a href="http://demo.cookwaimai.com/restaurant/toRestaurants/${building.alias}">${building.name}</a>
					   			</c:otherwise>
					   		</c:choose>
					   	</c:forEach>
					</div>
				</div>
			</c:forEach>
			
		</div>
		<div class="b">
			<SPAN class="copy">©</SPAN> 2012-2014 青岛外卖
		</div>
	</div>
</BODY>
</HTML>
