﻿<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${seo.title}</TITLE>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="${seo.keywords}" />
<meta name="description" content="${seo.description}" />

<meta content="text/html; charset=utf-8" http-equiv=Content-Type>

<%@include file="/assets/jsp/common.jsp"%>

<LINK rel="stylesheet" type="text/css" href="http://demo.cookwaimai.com/assets/css/order/order_confirm.css" media="all">
<script type="text/javascript" src="http://demo.cookwaimai.com/assets/js/js_app/order/order_general_confirm.js"></script>

<script type="text/javascript">
$(function(){
	var params = {
		    building_name : "${sessionScope.building.name}"
	};
	order.init(params);
	order.initListener();
});
</script>
</head>
<body>

<!-- 顶部导航代码块 -->
<%@include file="/WEB-INF/jsp/common/common_top.jsp"%>
<div id="pushCustomerMsg"></div>
	<div id="container">
	<a onclick="history.go(-1)" class="backLink blink">返回选菜</a>
	<div class="progress">
		<div class="text">
			<div class="text_1"><a href="javascript:;" >选餐馆</a></div>
			<div class="text_2"><a href="javascript:history.go(-1)">选美食</a></div>
			<div class="text_3"><a href="#">确认送餐地址</a></div>
			<div class="text_4">餐到付款</div>
		</div>
		<div class="pbar"><div class="pbaron pbaron_3"></div></div>
	</div>
	<div class="partl">
		<div class="confirm">
			<form id="form_order" action="#" method="post">
			<table class="detailArea">
				<tr>
					<td class="text">详细地址</td>
					<td class="input">
						<div class="l"></div>
						<div class="m" style="z-index:100;position: relative;">
							<div style="height: 39px">
								<span id="address" class="name">${sessionScope.building.name}</span>
								<input id="address_remark" class="address gray" maxlength="100" value="请输入或选择详细地址" />
							</div>
							<div id="address_selector" class="selector" style="position: relative; z-index: 1999;display:none;">
							</div>
						</div>
						<div class="r"></div>
						<div id="address_remark_tips" class="checker_wrong">
							<div class="icon"></div>
							<span>请填写详细地址</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="text">联系电话</td>
					<td class="input">
						<div class="l"></div>
						<div class="m" style="z-index:99;position: relative;">
							<input id="tel" maxlength="11" type="text" name="tel" value=""/>
							<div id="phone_selector" class="selector" style="position: relative; z-index: 999;display:none">
							</div>
							</div>
						<div class="r"></div>
						<div id="phone_tips" class="checker_code">
							<div class="icon"></div>
							<span><input value="10" type="button" class="code"/></span>
						</div>
					</td>
				</tr>
				
				<tr id="phone_code_continer" style="display: none">
					<td class="text">验证码</td>
					<td class="input">
						<div class="l"></div>
						<div class="m" style="z-index:98;position: relative;">
							<input id="phone_code" type="text" name="tel"/>
						</div>
						<div class="r"></div>
						<div id="phonecode_tips" class="checker_ok">
							<div class="icon"></div>
							<span></span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="text">备注</td>
					<td class="input">
						<div class="l"></div>
						<div class="m"><input id="remarks" type="text" name="remarks"  maxlength="24"/></div>
						<div class="r"></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div class="handy">
							<div class="item">不要辣</div>
							<div class="item">多加米</div>
							<div class="item">不要蒜</div>
							<div class="item">不要香菜</div>
							<a id="clearRemarks" class="item">清除备注</a>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="confirmOrder"></div>
						<div class="confirming">提交中。。。</div>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
	<div class="partr">
		<input type="hidden" id="lockToken" name="lockToken" value="94450ea2-35ab-4363-a51a-9eceddc99635"></input>
		
		<div id="order_display" style="display: none;">
			<table border="0" style="font-size:17px;width:550px;background-color: #ccc;" cellspacing="1">
			<tbody>
			<tr>	
				<td style="background-color: #fff;color:#FF0000;padding:5px">烹饪店铺</td>	
				<td style="background-color: #fff;padding:5px">${sessionScope.restaurant.name}</td>
			</tr>
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">配送地址</td>	
				<td id="address_display" style="background-color: #fff;padding:5px"></td>	
			</tr>	
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">收餐电话</td>	
				<td id="phone_display" style="background-color: #fff;padding:5px"></td>	
			</tr>	
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">菜单详情</td>
				<td style="background-color: #fff;;padding:5px">
				<table>	
				<tbody>			
					<c:forEach items="${sessionScope.order_menuList}" var="menu">
					<tr>		
					<td style="background-color: #fff;">${menu.name}</td>			
					<td style="background-color: #fff;">*</td>			
					<td style="background-color: #fff;">${menu.num}</td>			
					<td style="background-color: #fff;">=</td>		
					<td style="background-color: #fff;">￥${menu.price*menu.num}</td>	
					</tr>	
					</c:forEach>		
				</tbody>
				</table>
				</td>
			</tr>
			<tr>
			<td style="background-color: #fff;color:#FF0000;padding:5px">送餐费</td>
			<td style="background-color: #fff;padding:5px">￥${sessionScope.order_carriage}</td>
			</tr>
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">价格总计</td>
				<td style="background-color: #fff;padding:5px">${sessionScope.order_totalcount}+${sessionScope.order_carriage}=￥${sessionScope.order_totalcount+sessionScope.order_carriage}
			</td>
			</tr>
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">订单备注</td>
				<td id="remarks_display" style="background-color: #fff;padding:5px"></td>
			</tr>
			<tr>
				<td style="background-color: #fff;color:#FF0000;padding:5px">配送时间</td>
				<td style="background-color: #fff;padding:5px">马上配送</td></tr>
			</tbody>
			</table>
		</div>
		
	</div>
	</div>
	
	<!-- 底部导航代码块 -->
	<%@include file="/WEB-INF/jsp/common/common_bottom.jsp"%>
</body>
</html>