var commentsObj = {
    initListener: function() {
        $(".comments .actions .reply").live("click",function(){
            // 初始化反馈组件
            $("#feedback_pop .content").val("请输入您的回复信息");
            $("#feedback_pop [name^=step_]").hide();
            $("#feedback_pop [name=step_1]").show();

            commentsObj.reyply_id = $(this).attr("val");
            var replier_name = $(this).parent().parent().parent().find(".user_name").html();
            $("#feedback_pop .t").html("回复 " + replier_name);
            $("#feedback_pop").show();
            //计算偏移
            var offset = $(this).offset();
            $("#feedback_pop .c").css({
                "top": offset.top + "px"
            });
        });
        
		// 点击关闭按钮
		$(".pop .close").click(function() {
			$("#feedback_pop").hide();
		});
		// 点击输入框以后
		$("#feedback_pop .content").focus(function() {
			if ("" == $.trim($(this).val()) || "请输入您的回复信息" == $(this).val()) {
				$(this).val("");
			}

		});
		$("#feedback_pop .content").focusout(function() {
			if (!$.trim($(this).val())) {
				$(this).val("请输入您的回复信息");
			}
		});
        
        // 用户点击了提交按钮以后
        $("#feedback_pop .submit").click(function() {
            var content = $.trim($("#feedback_pop .content").val());
            if (!content || "请输入您的回复信息" == content || !commentsObj.reyply_id) {
                $(this).val("请输入您的回复信息");
                return;
            }
            commentsObj.sendReply(commentsObj.reyply_id, content);
        });

        $(".comments .comment .menu_detail a[name='menu_name']").live("mouseover",function(){
            var menuId = $(this).attr("val");
            var menuPic = ".comments .comment .menu_detail .menu_pic[val='" + menuId + "']";
            var rest_id = $(this).next().attr("belong");
            var loadedFlag = $(this).next().attr("loaded");
            $(this).next().show();
            
            if(loadedFlag && "true" == loadedFlag) {
                $(this).next().show();
                //如果加载过了就不再请求
            }else {
                $(this).next().show();
                $.ajax({
                	type : "POST",
					url : "/comment/getMenuPic",
					dataType: 'json',
					contentType: "application/x-www-form-urlencoded;charset=utf-8",
					cache: false,
                    data: {
                        menu_id: menuId
                    },
                    success: function(response) {
                    	
                    	if(response && "none.png" == response.data){
                    		$(menuPic).find("img").attr("src", "http://demo.cookwaimai.com/assets/images/common/none.png");
                    	}else {
                    		$(menuPic).find("img").attr("src", "http://demo.cookwaimai.com/assets/images/restaurant/"+rest_id+"/"+response.data);
                    	}
                    },
                    error: function() {
                        $(menuPic).find("img").attr("src", "http://demo.cookwaimai.com/assets/images/common/none.png");
                    }
                });
            }
        });

        $(".comments .comment .menu_detail a[name='menu_name']").live("mouseout",function() {
            var menuId = $(this).attr("val");
            var menuPic = ".comments .comment .menu_detail .menu_pic[val='" + menuId + "']";
            $(menuPic).hide();
        });

        $(".comments .comment .menu_detail .menu_pic").live("mouseover",
        function() {
            $(this).show();
        });
        $(".comments .comment .menu_detail .menu_pic").live("mouseout",
        function() {
            $(this).hide();
        });

        $("#moreComments").click(function() {
            $(this).html("正在努力的为您加载...");
            var count = $(this).attr("val");
            $(this).attr("val", parseInt(count) + 5);
            $.ajax({
            	type : "POST",
				url : "/comment/listBuildingComments",
				dataType: 'json',
				contentType: "application/x-www-form-urlencoded;charset=utf-8",
				cache: false,
                data: {
                	start: count
                },
                success: function(response) {
                    if (!response && response.dataList < 5) {
                        $("#moreComments").html("没有更多评论了");
                    } else {
                        $("#moreComments").html("显示更多我的评论");
                    }

                    var commentList = response.dataList;
                    var commentsHtml = "";
                    for (var i = 0; i < commentList.length; i++) {
                        var totalCommentCount = $(".comments .comment").length;
                        totalCommentCount += i;
                        commentsHtml += commentsObj.getCommentsHtml(commentList[i], totalCommentCount);
                    }
                    $(".comments").append(commentsHtml);
                }
            });
        });
    },
    getCommentsHtml: function(comment, totalCommentCount) {

        var commentHtml = "";
        commentHtml += '<div class="comment">';
        commentHtml += '<div class="pic">';
        commentHtml += '<img style="width: 100%; height: 140px" src="/assets/images/restaurant/' + comment.rest_id + '/' + comment.rest_pic + '">';
        commentHtml += '<div class="menu_rest">' + comment.rest_name + '：<a href="/restaurant/toRestaurantDetail/'+ comment.alias +'" target="_blank">' + comment.rest_name + '</a></div></div>';
        commentHtml += '<div val="' + comment.id + '" class="replies">';
        commentHtml += '<div class="item_title">评论详情</div>';
        commentHtml += '<span class="user_name">' + comment.user_name + '</span>&nbsp;在&nbsp;' + new Date(parseFloat(comment.comment_time)).toLocaleString() + '：';
        commentHtml += '<div class="c2">';
        commentHtml += '<b>点评：</b>' + comment.content + '</div>';
        for (var i = 0; i < comment.replyList_s.length; i++) {
            var reply = comment.replyList_s[i];
            commentHtml += '<div class="quote">';
            commentHtml += '<div class="nt"><b>' + reply.nick_name + '</b>' + reply.reply_time + '回复</div><div>&nbsp;&nbsp;&nbsp;&nbsp;' + reply.content + '</div></div>';
        }
        commentHtml += '</div>';

        commentHtml += '<div class="orderDetail">';
        commentHtml += '<div class="item_title">订单详情</div>';
        for (var j = 0; j < comment.menus.length; j++) {
            var menu = comment.menus[j];
            commentHtml += '<div class="menu_detail">';
            commentHtml += '<a val="' + menu.menu_id + '" name="menu_name">' + menu.name + ' ￥' + menu.price + '</a>';
            commentHtml += '<div belong="' + comment.rest_id + '" loaded="false" val="' + menu.menu_id + '" class="menu_pic" style="display: none;">';
            commentHtml += '<img src="/assets/images/common/loading.gif" alt="' + menu.name + '">';
            commentHtml += '<div class="menu_buy">';
            commentHtml += '<a target="_blank" href="/restaurant/toRestaurantDetail/'+comment.alias+'?menu_id=' + menu.menu_id + '">来一份</a></div></div></div>';

        }
        commentHtml += '</div>';

        commentHtml += '<div style="" class="scoreDetail">';
        commentHtml += '<div class="item_title">评分详情</div>';
        commentHtml += '<div class="score">';
        commentHtml += '<div class="text">口味：</div>';
        commentHtml += '<div class="star star_' + comment.taste + '"></div></div>';
        commentHtml += '<div class="score">';
        commentHtml += '<div class="text">速度：</div>';
        commentHtml += '<div class="star star_' + comment.speed + '"></div></div>';
        commentHtml += '<div class="score">';
        commentHtml += '<div class="text">态度：</div>';
        commentHtml += '<div class="star star_' + comment.attitude + '"></div></div>';

        commentHtml += '<div class="mix">';
        commentHtml += '<div class="text">最近评论：</div>';
        commentHtml += '<a href="/restaurant/toRestaurant.action?restId=' + comment.rest_id + '" target="_blank">' + comment.recommend_count + '</a>&nbsp;次</div>';
        commentHtml += '<div class="actions">';
        commentHtml += '<a val="' + comment.id + '" class="reply">回复</a>';
        commentHtml += '</div></div></div>';

        return commentHtml;
    },
    sendReply: function(commentId, content) {

        var username = $("#feedback_pop .input_1[name='uname']").val();
        username = encodeURI(username);
        var password = $("#feedback_pop .input_1[name='upassword']").val();
        $.ajax({
        	type : "POST",
			url : "/comment/submitReply",
			dataType: 'json',
			contentType: "application/x-www-form-urlencoded;charset=utf-8",
			cache: false,
            data: {
                username: username,
                password: password,
                commentId: commentId,
                content: content
            },
            beforeSend: function() {
                $("#feedback_pop [name^=step_]").hide();
                $("#feedback_pop [name=step_3]").show();
            },
            success: function(response) {
            	
                $("#feedback_pop [name^=step_]").hide();
                if (response && response.data && "success" == response.data) {
                    $("#feedback_pop [name='step_4']").show();
                    var reply = {
                        replyTime: new Date().toLocaleTimeString(),
                        replierName: '我的回复 ',
                        content: content
                    };
                    var replyHtml = commentsObj.getReplyHtml(reply);
                    $(".comments .comment .replies[val='" + commentId + "']").append(replyHtml);
                } else {
                    $("#feedback_pop [name=step_2]").find(".tip").show();
                    $("#feedback_pop [name=step_2]").show();
                }
            },
            error: function() {
                $("#feedback_pop [name^=step_]").hide();
                $("#feedback_pop [name=step_5]").show();
            }
        });
    },
    getReplyHtml: function(reply) {

        var replyHtml = "";
        replyHtml += '<div class="quote">';
        replyHtml += '<div class="nt">';
        replyHtml += '<b>' + reply.replierName + '</b>';
        replyHtml += reply.replyTime;
        replyHtml += '回复';
        replyHtml += '</div>';
        replyHtml += '<div>';
        replyHtml += '&nbsp;&nbsp;&nbsp;&nbsp;';
        replyHtml += reply.content;
        replyHtml += '</div>';
        replyHtml += '</div>';

        return replyHtml;
    }
};