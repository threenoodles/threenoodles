var aniSpeed = {slow: 2000, med: 1000, fast: 500, flash: 200};
$(document).ready(function() {
	bannerSearch.init();
	topBanner.init();
});

var topBanner = {
	pos: 0,
	max: null,
	timeout: 7000,
	width: 719,
	pNode: null,
	cNode: null,
	nNode: null,
	timer: null,
	init: function() {
		this.max = $('.rollerBox .roller_b').children().length-1;
		for(var i = 1;i< this.max+1;i++){
			$('.rollerBox .roller_nav').append('<div name="nav_'+i+'"></div>');
		}
		
		this.pNode = $('.rollerBox');
		this.cNode = this.pNode.find('.roller_b');
		this.nNode = this.pNode.find('.roller_nav');
		this.nNode.children('div').click(function(){topBanner.move($(this).attr('name').substr(4));});
		this.pNode.find('.roller_c').bind({
			'mouseenter': function(){clearTimeout(topBanner.timer);},
			'mouseleave': function(){clearTimeout(topBanner.timer);topBanner.timer = setTimeout(topBanner.move, topBanner.timeout);}
		});
		topBanner.timer = setTimeout(topBanner.move, topBanner.timeout);
	},
	move: function(pos) {
		clearTimeout(topBanner.timer);
		if(!pos) topBanner.pos++;
		else topBanner.pos = pos;
		if(topBanner.pos < 0) topBanner.pos = topBanner.max;
		else if(topBanner.pos > topBanner.max) topBanner.pos = 0;
		var left = 0 - topBanner.pos * topBanner.width;
		topBanner.nNode.children('div').removeClass('roller_active').css('zoom', '1');
		topBanner.nNode.children('[name=nav_'+topBanner.pos+']').addClass('roller_active');
		topBanner.cNode.stop().animate({left: left+'px'}, aniSpeed.med, 'easeOutExpo');
		topBanner.timer = setTimeout('topBanner.move()', topBanner.timeout);
	}
};

var bannerSearch = {
	init: function() {
		$('#search_submit').click(bannerSearch.submit);
	},
	submit: function() {
		var product = $(this).parent().find(".place");
		var zone = $(this).parent().find(".restaurant");
		if(product.val() == product.attr('tips')) {
			product.val('');
		}
		if(zone.val() == zone.attr('tips')) {
			zone.val('');
		}
		$(this).parent().submit();
	}
};
