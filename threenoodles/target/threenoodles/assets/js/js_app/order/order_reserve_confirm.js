var order = {
    init: function(params_) {
        order.params = {
            building_name: "",
            phonecode_thread: null,
            reserveTime : "",
            flag: {
                address: false,
                phone: false,
                reserve : false
            }
        };
        order.params = $.extend({},order.params, params_);
    },
    initListener: function() {

        $(window).keydown(function() {
            $("#address_selector").hide();
            $("#phone_selector").hide();
        });

        $("#reserve_time_set").click(function() {
            var html = order.getTimeSelectorHtml();
            order.params.time_popup = new Boxy(html,{
            	unloadOnHide : true,
            	title : '设置下单时间'
            });
            
        });
        //小时
        $("#time_hour").live("change",function(){
        	var hour = $(this).val();
        	order.checkReserveTime();
        });
        //分钟
        $("#time_minutes").live("change",function(){
        	var minutes = $(this).val();
        	order.checkReserveTime();
        });
        
        $("#time_select_ok").live("click",function(){
        	if(order.params.time_popup && order.params.flag.reserve){
        		order.params.time_popup.hide();
        		$("#reserve_time").text(order.params.reserveTime);
        		
        		$("#reserve_tips").attr("class", "checker_ok").find("span").html("");
        		$("#reserve_tips").show();
        	}
        });
        
        $("#time_select_cancel").live("click",function(){
        	if(order.params.time_popup){
        		order.params.time_popup.hide();
        	}
        });

        $("#address_remark").focus(function() {
            if ("请输入或选择详细地址" == $(this).val()) {
                $(this).val("");
            }
            $(this).css({
                "color": "#000000"
            });
            if ($("#address_selector").find("div") && $("#address_selector").find("div").length) {
                $("#address_selector").show();
            } else {
                order.loadAddress();
            }
        });
        
        
        $("#address_remark").focusout(function() {
            order.params.flag.address = false;
            var address = $("#address_remark").val();
            if (address) {
                $("#address_remark_tips").attr("class", "checker_ok").find("span").html("");
                $("#address_remark_tips").show();
                order.params.flag.address = true;
            } else {
                $("#address_remark_tips").attr("class", "checker_wrong").find("span").html("请填写详细地址").show();
                $("#address_remark_tips").show();
            }
            $("#address_selector").hide();
        });

        $("#address_selector a").live("hover",
        function() {
            var address = $(this).text();
            $("#address_remark").val(address);
        });

        $("#tel").focus(function() {
            if ($("#phone_selector").find("div") && $("#phone_selector").find("div").length) {
                $("#phone_selector").show();
            } else {
                order.loadPhone();
            }
        });

        $("#phone_selector a").live("hover",
        function() {
            var phone = $(this).text();
            $("#tel").val(phone);
        });

        $("#tel").focusout(function() {
            order.params.flag.phone = false;
            var phone = $("#tel").val();
            if (phone && /^1[3-9][0-9]\d{8}$/.test(phone)) {
                order.checkPhone(phone);
            } else {
                $("#phone_tips").attr("class", "checker_wrong").find("span").html("请输入正确电话").show();
                $("#phone_tips").show();
            }
            $("#phone_selector").hide();
        });

        $("table .checker_code .code").live("click",
        function() {
            if (null === order.phonecode_thread) {
                var phone = $("#tel").val();
                order.checkPhone(phone);
            }
        });

        $("#phone_code").focusout(function() {
            var code = $(this).val();
            var phone = $("#tel").val();
            if (code) {
                order.checkPhoneCode(code, phone);
            } else {
                $("#phonecode_tips").attr("class", "checker_wrong").find("span").html("验证码错误");
                $("#phonecode_tips").show();
            }
        });

        $(".detailArea .handy div").click(function() {
            if ($("#remarks").val() && $("#remarks").val().length >= 25) {
                return;
            }
            $("#remarks").val() ? $("#remarks").val($("#remarks").val() + "," + $(this).text()) : $("#remarks").val($(this).text());
        });

        $("#clearRemarks").click(function() {
            $("#remarks").val("");
        });

        $(".detailArea .confirmOrder").click(function() {
            if (order.params.flag.address && order.params.flag.phone && order.params.flag.reserve) {
                var orderHtml = order.getOrderHtml();
                var popup = Boxy.ask(orderHtml, ["确定", "取消"],
                function(val) {
                    if ("确定" == val) {
                        var mask = null;
                        $.ajax({
                        	type: 'POST',
                        	dataType: 'json',
                        	contentType: "application/x-www-form-urlencoded;charset=utf-8",
                        	cache: false,
                            url: "http://demo.cookwaimai.com/order/submitOrder",
                            data: {
                                address: $("#address_remark").val(),
                                phone: $("#tel").val(),
                                remarks: $("#remarks").val(),
                                reserveTime: order.params.reserveTime,
                                orderType: 2
                            },
                            beforeSend: function() {
                                mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
                            },
                            success: function(response) {
                                if (mask) {
                                    mask.hide();
                                }
                                if (response && response.success) {
                                    window.location.href = "http://demo.cookwaimai.com/order/toSuccess";
                                } else {
                                    Boxy.ask("<span style='color:red'>对不起，订单提交异常，请到 “个人中心” 查看订单是否提交成功！</span>", ["确定"],
                                    function(val) {
                                    	
					            		 window.location.href="http://demo.cookwaimai.com/mycenter/order?naviFlag=mycenter&flag=today";
                                        popup.hide();
                                    },
                                    {
                                        title: "异常提示"
                                    });
                                }
                            },
                            error: function() {
                                if (mask) {
                                    mask.hide();
                                }
                                alert("系统繁忙，请退出后重试!");
                            }
                        });
                    } else {
                        popup.hide();
                    }
                },
                {
                    title: "订单详情",
                    hideShrink: '',
                    draggable: true,
                    modal: true,
                    width: 500,
                    height: 800
                });

            }else if(!order.params.flag.reserve){
            	Boxy.alert("<span style='color:red'>请正确选择预定时间!</span>");
            }else if(!order.params.flag.address) {
                Boxy.alert("<span style='color:red'>请填写详细地址!</span>");
            }else {
            	Boxy.alert("<span style='color:red'>请正确填写收餐电话!</span>");
            }
        });
    },
    //验证预定时间
    checkReserveTime : function(){
    	var hour = $("#time_hour").find("option:selected").val();
    	var minutes = $("#time_minutes").find("option:selected").val();
    	
    	if(hour && minutes){
    		var reserveTime = hour+":"+minutes+":00";
    		$.ajax({
                url: "http://demo.cookwaimai.com/order/checkReserveTime",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                type: 'POST',
                dataType: 'json',
                data : {
                	reserveTime : reserveTime
                },
                cache: false,
                beforeSend: function() {
                	$("table[name='time_selector_tips']").each(function(i,item){
                		$(item).hide();
                	});
                	$("#time_tips_info").css({color:"#ccc"}).text("正在验证...").show();
                },
                success: function(response) {
                	order.params.flag.reserve = false;
                	$("table[name='time_selector_tips']").each(function(i,item){
            			$(item).hide();
                	});
            	    $("#reserve_tips").attr("class", "checker_wrong").find("span").html("请选择正确时间");
                    $("#reserve_time").text(order.params.reserveTime);
                    $("#reserve_tips").show();
            	
                	if(response && response.success){
                		order.params.flag.reserve = true;
                		order.params.reserveTime = reserveTime;
                		$("#time_tips_ok").show();
                		$("#reserve_tips").attr("class", "checker_ok").find("span").html("");
                		$("#reserve_time").text(order.params.reserveTime);
                		
                	}else if(response && "failed_past"==response.message){
                		$("#time_tips_wrong_past").show();
                	}else {
                		$("#time_tips_wrong_between").show();
                	}
                },
                error: function(response) {
                	order.params.flag.reserve = false;
                    alert("发送请求失败，请退出浏览器后重试!");
                }
            });
    	}
    },
    //加载地址
    loadAddress: function() {
        $.ajax({
            url: "http://demo.cookwaimai.com/address/listUserAddress",
            type: 'GET',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            cache: false,
            beforeSend: function() {
                $("#address_selector").html("<div style='font-size:14px'>努力加载中。。。</div>");
                $("#address_selector").show();
            },
            success: function(response) {
                if (response && response.dataList && response.dataList.length > 0) {
                    var html = "";
                    $.each(response.dataList,
                    function(index, value) {
                        html += '<div><a href="javascript:;">' + value.address + '</a></div>';
                    });
                    $("#address_selector").html(html);
                    $("#address_selector").show();

                } else {
                    $("#address_selector").html("<div style='font-size:14px'>未找到地址,提交订单后自动为您保存。</div>");
                }
            },
            error: function() {
                $("#address_selector").html("<div style='font-size:14px'>加载地址失败!</div>");
            }
        });
    },
    // 加载电话
    loadPhone: function() {
        $.ajax({
            url: "http://demo.cookwaimai.com/phone/listUserPhone",
            type: 'GET',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            cache: false,
            beforeSend: function() {
                $("#phone_selector").html("<div style='font-size:14px'>努力加载中。。。</div>");
                $("#phone_selector").show();
            },
            success: function(response) {
                if (response && response.dataList && response.dataList.length > 0) {
                    var html = "";
                    $.each(response.dataList,
                    function(index, value) {
                        html += '<div><a href="javascript:;">' + value.telephone + '</a></div>';
                    });
                    $("#phone_selector").html(html);
                    $("#phone_selector").show();

                } else {
                    $("#phone_selector").html("<div style='font-size:14px'>未找到电话,提交订单后自动为您保存。</div>");
                }
            },
            error: function() {
                alert("系统繁忙，请退出后重试!");
            }
        });
    },
    // 后台验证电话
    checkPhone: function(phone) {
        $.ajax({
            url: "http://demo.cookwaimai.com/phone/checkPhone",
            data: {
                phone: phone
            },
            type: 'POST',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            cache: false,
            success: function(response) {
                if (response && response.data) {
                    // 清除更新时间的线程
                    order.clearTimer();
                    // 电话验证通过,隐藏电话验证码
                    $("#phone_code_continer").hide();
                    $("#phone_tips").attr("class", "checker_ok").find("span").html("");
                    $("#phone_tips").show();
                    order.params.flag.phone = true;
                } else {

                    $("#phone_code_continer").show();
                    $("#phone_tips").attr("class", "checker_wrong").find("span").html('电话未验证');
                    $("#phone_tips").show();
                    var popup = Boxy.ask("本收餐电话第一次订餐，需要接收一次短信验证，是否继续？", ["接收", "取消"],
                    function(val) {
                        if ("接收" == val) {
                            // 发送验证码
                            order.sendPhoneCode(phone);
                        }
                        popup.hide();
                    },
                    {
                        title: "温馨提示"
                    });

                }
            },
            error: function() {
                alert("系统繁忙，请退出后重试!");
            }
        });
    },
    sendPhoneCode: function(phone) {
        var mask = null;
        $.ajax({
            url: "http://demo.cookwaimai.com/phone/sendPhoneCode",
            data: {
                phone: phone
            },
            type: 'GET',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            cache: false,
            beforeSend: function() {
                mask = new Boxy("<img class='boxy-image' src='http://demo.cookwaimai.com/assets/images/common/loading.gif'/>");
            },
            success: function(response) {
                if (mask) {
                    mask.hide();
                }
                if (response && response.data) {
                    Boxy.alert("恭喜您，验证码已成功发送到您的手机，请注意查收！");
                    // 清除更新时间的线程
                    order.clearTimer();
                    order.startTimer();
                } else {
                    $("#phone_tips").attr("class", "checker_wrong").find("span").html('电话未验证');
                    $("#phone_tips").show();
                    var popup = Boxy.ask("发送验证码失败，是否重新发送？", ["发送", "取消"],
                    function(val) {
                        if ("发送" == val) {
                            // 发送验证码
                            order.sendPhoneCode(phone);
                        } else {
                            popup.hide();
                        }
                    },
                    {
                        title: "失败提示"
                    });
                }
            },
            error: function() {
                if (mask) {
                    mask.hide();
                }
                alert("系统繁忙，请退出后重试!");
            }
        });
    },
    
    startTimer: function() {
        $("#phone_tips").find("span").html('<input value="90" type="button" class="code"/>');
        order.phonecode_thread = window.setInterval(function() {
            if (!$("#phone_tips").find("input").get(0)) {
                $("#phone_tips").find("span").html('<input value="90" type="button" class="code"/>');
            }
            var time = $("#phone_tips").find("input").val();
            if (time && parseInt(time) - 1 > 0) {
                time = time - 1;
            } else {
                order.clearTimer();
                time = "发送";
            }
            $("#phone_tips").attr("class", "checker_code").find("span").html('<input value="' + time + '" type="button" class="code"/>');
            $("#phone_tips").show();
        },
        1000);
    },
    clearTimer: function() {
        if (null !== order.phonecode_thread) {
            window.clearInterval(order.phonecode_thread);
            order.phonecode_thread = null;
        }
    },
    checkPhoneCode: function(code, phone) {
        $.ajax({
            url: "http://demo.cookwaimai.com/phone/checkPhoneCode",
            data: {
                code: code,
                phone: phone
            },
            type: 'GET',
            dataType: 'json',
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            cache: false,
            beforeSend: function() {
                $("#phonecode_tips").attr("class", "checker_wrong").find("span").html("正在验证");
                $("#phonecode_tips").show();
            },
            success: function(response) {
                if (response && response.success) {
                    // 验证成功验证码以后，触发验证电话事件，改变状态
                    var phone = $("#tel").val();
                    order.checkPhone(phone);
                    $("#phonecode_tips").attr("class", "checker_ok").find("span").html("");
                    $("#phonecode_tips").show();
                } else {
                    $("#phonecode_tips").attr("class", "checker_wrong").find("span").html("验证码错误");
                    $("#phonecode_tips").show();
                }
            },
            error: function() {
                alert("系统繁忙，请退出后重试!");
            }
        });
    },
    getOrderHtml: function() {
        $("#address_display").html(order.params.building_name + $("#address_remark").val());
        $("#phone_display").html($("#tel").val());
        $("#remarks_display").html($("#remarks").val());
        $("#deli_time").html(order.params.reserveTime);
        return $("#order_display").html();
    },
    getTimeSelectorHtml: function() {
        var html = "";
        html += '<div style="width:350px; min-height:20px; height:auto !important; padding:10px; border:double 4px #eee;">';
        html += '<table cellpadding="0" cellspacing="0" border="0" style="font-size:14px; font-weight:bold; color:#333;">';
        html += '<tr>';
        html += '<td width="35"><img src="/assets/images/reserve/icontime.gif" width="25" height="24" /></td>';
        html += ' <td>选择时间：';
        html += '<select id="time_hour" style="width: 80px">';
        html += '<option value="">请选择</option>';
        for(var i = 10;i<21;i++){
        	html += '<option val="'+i+'">'+i+'</option>';
        }
        html += '</select> 时';
        html += '<select id="time_minutes" style="width: 80px;margin-left:10px">';
        html += ' <option value="">请选择</option>';
        for(var j = 1;j<60;j++){
        	if(j<10){
        		html += '<option value="0'+j+'">'+j+'</option>';
        	}else {
        		html += '<option value="'+j+'">'+j+'</option>';
        	}
        }
        
        html += '</select> 分</td>';
        html += '</tr>';
        html += '</table>';
        html += '<div style="padding-top:10px; margin-top:10px; border-top:solid 1px #ccc; margin-bottom:10px; width:100%;">';
        
        html += '<table id="time_tips_ok" name="time_selector_tips" cellpadding="0" cellspacing="0" border="0" style="margin:10px 25px 25px 25px;display:none">';
        html += '<tr><td width="25" valign="top"><div style="background:url(/assets/images/common/elements.png) -334px -238px no-repeat; height:16px; width:16px; float:left; line-height:200%;"></div></td><td style="font-size:12px; float:left;color: green;">可以预定</td></tr>';
        html += '</table>';
        
        html += '<table id="time_tips_wrong_past" name="time_selector_tips" cellpadding="0" cellspacing="0" border="0" style="margin:10px 25px 25px 25px;display: none;color:#FF6200">';
        html += '<tr><td width="25" valign="top"><div style="background:url(/assets/images/common/elements.png) -366px -238px no-repeat; height:16px; width:16px; float:left; line-height:200%;"></div></td><td style="font-size:12px; float:left;">配送时间已过，请您重新选择!</td></tr>';
        html += '</table>';
        
        html += '<table id="time_tips_wrong_between" name="time_selector_tips" cellpadding="0" cellspacing="0" border="0" style="margin:10px 25px 25px 25px;display: none;color:#FF6200">';
        html += '<tr><td width="25" valign="top"><div style="background:url(/assets/images/common/elements.png) -366px -238px no-repeat; height:16px; width:16px; float:left; line-height:200%;"></div></td><td style="font-size:12px; float:left;">本店铺的配送时间为，请在配送时间段内选择<br/>上午 '+ order.params.begin_am + '-' + order.params.end_am +'<br/>下午 '+ order.params.begin_pm + '-' + order.params.end_pm+'</td></tr>';
        html += '</table>';
        
        html += '<table id="time_tips_info" cellpadding="0" name="time_selector_tips" cellspacing="0" border="0" style="margin:10px 25px 25px 25px;display: block;color:#FF6200">';
        html += '<tr><td width="25" valign="top"><div style="background:url(/assets/images/common/elements.png) -350px -238px no-repeat; height:16px; width:16px; float:left; line-height:200%;"></div></td><td style="font-size:12px; float:left;">本店铺的配送时间为，请在配送时间段内选择<br/>上午 '+ order.params.begin_am + '-' + order.params.end_am +'<br/>下午 '+ order.params.begin_pm + '-' + order.params.end_pm+'</td></tr>';
        html += '</table>';
        
        html += '<div style="text-align:center; width:100%;">';
        html += '<input id="time_select_ok" type="submit" style="background:url(/assets/images/common/elements.png) -404px -380px no-repeat; font-size:14px; border:medium none; cursor:pointer; font-weight:bold; color:#fff; height:26px; width:82px;margin-right:15px" value="确定">';
        html += '<input id="time_select_cancel" type="submit" style="background:url(/assets/images/common/elements.png) -404px -380px no-repeat; font-size:14px; border:medium none; cursor:pointer; font-weight:bold; color:#fff; height:26px; width:82px;" value="取消">';
        html += '</div>';
        html += '</div></div>';
        
        return html;
    }
};