//左侧的导航
var mycenterObj = {
	initListener : function(flag){
		if(flag){
			mycenterObj.clearActive();
			var li_indentification = "li[val='"+flag+"']";
			$("ul[name='mycenter_leftNavi']").find(li_indentification).attr("class","active");
		}
	},
	clearActive : function(){
		var leftNavis = $("ul[name='mycenter_leftNavi']").find("li");
		for(var i = 0;i<leftNavis.length;i++){
			$(leftNavis[i]).attr("class","");
		}
	}
};

